<?php
@ini_set('max_execution_time',0);
echo '<style>body{background-color:#444;color:#e1e1e1;}</style>';
if (!isset($_POST['evalcode'])) {
?>
Code to Eval: <br />
<form method=post>
<textarea rows="45" cols="50" name=evalcode></textarea><br />
<select name='encType'>
<option value='singleB64'>Single-Pass(1) B64</option>
<option value='pentaB64'>Penta-Pass(5) B64</option>
<option value='tenB64'>Deca-Pass(10) B64</option>
<option value='tfB64'>25-Pass B64</option>
<option value='brokenB64Ob'>Broken-up B64 Ob</option>
<option value='brokenB64Ob25'>25-Pass Broken-up B64 Ob</option>
<option value='pentabrokenB64Ob25'>Penta-Pass B64(25-Pass BB64 Ob)</option>
<option value='filebasedOb'>File based Obfuscation</option>
<option value='pentabrokenB64Ob25filebasedOb'>File Ob(Penta B64(25-BB64 Ob))</option>
<option value='BO25-pentabrokenB64Ob25filebasedOb'>File Ob(B64(25-BB64 Ob(Penta B64(25-BB64 Ob))))</option>
<option value='hexEncode'>HexString encode</option>
<option value='hexEncode5'>Penta-Pass(5) HexEncode</option>
<option value='hexEncode10'>Deca-Pass(10) HexEncode</option>
<option value='File10Hex25Bob10B645Hex25Bob'>10-Hex(25-BB64 Ob(10-B64(5-Hex(25-BB64 Ob)))))</option>
<option value='Rot13'>Rot-13</option>
<option value='RotBO25RotPentaRotBB64Ob25RotFilebasedOb'>File based(Rot(25-BB64Ob(Rot(5-B64(Rot(25-BB64 Ob(Rot())))))))</option>
<option value='Rot13BrokenPentaBO25FileRead'>File based(25-BB64 Ob(5-B64(Rot())))</option>
<option value='bitShift'>Bit-Shift</option>
</select>
<br />
<input type=submit value='>>'>
</form>
<?php
} else {

switch($_POST['encType']) {
  case 'singleB64':
    $StringFileData = '<?php '.B64Gz(1, $_POST['evalcode']).' ?>';
  break;
  case 'pentaB64':
    $StringFileData = '<?php '.B64Gz(5, $_POST['evalcode']).' ?>';
  break;
  case 'tenB64':
    $StringFileData = '<?php '.B64Gz(10, $_POST['evalcode']).' ?>';
  break;
  case 'tfB64':
    $StringFileData = '<?php '.B64Gz(25, $_POST['evalcode']).' ?>';
  break;
  case 'brokenB64Ob':
    $StringFileData = '<?php '.BrokenB64Ob(1, $_POST['evalcode']).' ?>';
  break;
  case 'brokenB64Ob25':
    $StringFileData = '<?php '.BrokenB64Ob(25, $_POST['evalcode']).' ?>';
  break;
  case 'pentabrokenB64Ob25':
    $StringFileData = '<?php '.B64Gz(5, BrokenB64Ob(25, $_POST['evalcode'])).' ?>';
  break;
  case 'filebasedOb':
    $StringFileData = '<?php '.fileRead($_POST['evalcode']).' ?>';
  break;
  case 'pentabrokenB64Ob25filebasedOb':
    $StringFileData = '<?php '.fileRead(B64Gz(5, BrokenB64Ob(25, $_POST['evalcode']))).' ?>';
  break;
  case 'BO25-pentabrokenB64Ob25filebasedOb':
    $StringFileData = '<?php '.fileRead(BrokenB64Ob(25, B64Gz(5, BrokenB64Ob(25, $_POST['evalcode'])))).' ?>';
  break;
  case 'hexEncode':
    $StringFileData = '<?php '.hexEncode(1, $_POST['evalcode']).' ?>';
  break;
  case 'hexEncode5':
    $StringFileData = '<?php '.hexEncode(5, $_POST['evalcode']).' ?>';
  break;
  case 'hexEncode10':
    $StringFileData = '<?php '.hexEncode(10, $_POST['evalcode']).' ?>';
  break;
  case 'File10Hex25Bob10B645Hex25Bob':
    $StringFileData = '<?php '.hexEncode(10, BrokenB64Ob(25, B64Gz(10, hexEncode(5, BrokenB64Ob(25, $_POST['evalcode']))))).' ?>';
  break;
  case 'Rot13':
    $StringFileData = '<?php '.rotEncode($_POST['evalcode']).' ?>';
  break;
  case 'RotBO25RotPentaRotBB64Ob25RotFilebasedOb':
    $StringFileData = '<?php '.fileRead(rotEncode(BrokenB64Ob(25, rotEncode(B64Gz(5, rotEncode(BrokenB64Ob(25, rotEncode($_POST['evalcode'])))))))).' ?>';
  break;
  case 'Rot13BrokenPentaBO25FileRead':
    $StringFileData = '<?php '.fileRead(BrokenB64Ob(25, B64Gz(5, rotEncode($_POST['evalcode'])))).' ?>';
  break;
  case 'bitShift':
    $StringFileData = '<?php '.bitShift(1, $_POST['evalcode']).' ?>';
  break;

}

echo 'Final code:<br />';
echo '<textarea rows="45" cols="50">';
echo $StringFileData;
echo '</textarea>';

$fh = fopen("EvalEncodeScript.php", 'w') or die("can't open file");
$stringData = $StringFileData;
fwrite($fh, $stringData);
fclose($fh);

echo '<br /><a href=".\EvalEncodeScript.php" target="_blank">go to eval file</a><br />';
?>
<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
<input type="submit" name="Back" value="Back"/>
</form>
<?php

}

function B64Gz($times, $data) {
  if ($times == 1) {
    $sixfour = base64_encode(gzdeflate($data));
    return "preg_replace(\"/.*/e\",\"\\x65\\x76\\x61\\x6C\\x28\\x67\\x7A\\x69\\x6E\\x66\\x6C\\x61\\x74\\x65\\x28\\x62\\x61\\x73\\x65\\x36\\x34\\x5F\\x64\\x65\\x63\\x6F\\x64\\x65\\x28'".$sixfour."'\\x29\\x29\\x29\\x3B\",\".\");"; //eval(gzinflate(base64_decode('code')));
  } else {
    $data = base64_encode(gzdeflate($data)); // Very first and last to be decoded, need nothing after it.
    for ($i = 1; $i <= ($times - 1); $i++) { // Needed for correct amount
      $data = base64_encode(gzdeflate('eval(gzinflate(base64_decode(\''.$data.'\')));'));
    }
    return "preg_replace(\"/./e\",\"\\x65\\x76\\x61\\x6C\\x28\\x67\\x7A\\x69\\x6E\\x66\\x6C\\x61\\x74\\x65\\x28\\x62\\x61\\x73\\x65\\x36\\x34\\x5F\\x64\\x65\\x63\\x6F\\x64\\x65\\x28'".$data."'\\x29\\x29\\x29\\x3B\",\".\");"; //eval(gzinflate(base64_decode('code')));
  }
}

function BrokenB64Ob($times, $data) {
  $AlphaNumeric = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcedfghijklmnopqrstuvwxyz0123456789'; // 62 total characters
    $AlphaNumericArray = str_split($AlphaNumeric);

    $AlphaNumericSym = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcedfghijklmnopqrstuvwxyz0123456789!@#$%^&()_+\\\'\"[]{}<>?.*-';
    $AlphaNumericSymArray = str_split($AlphaNumericSym);
    $totalVariablesNames = array(array());
    $B64SplitNameLength = mt_rand(1, 15); // Length for variable names
    $VarName = '';
    for ($j = 0; $j <= $B64SplitNameLength; $j++) {
      $VarName = $VarName.$AlphaNumericArray[mt_rand(0, 61)];
    }
    $B64SplitName = $VarName;

    if ($times == 1) {
      $sixfour = base64_encode(gzdeflate($_POST['evalcode']));
    } else {
      $sixfour = base64_encode(gzdeflate($_POST['evalcode'])); // Very first and last to be decoded, need nothing after it.
      for ($i = 1; $i <= ($times - 1); $i++) { // Needed for correct amount
        $sixfour = base64_encode(gzdeflate('eval(gzinflate(base64_decode(\''.$sixfour.'\')));'));
      }
    }

    $whileitteration = 0;
    $buildb64String = '';
    $buildvardecString = '';
    while(strlen($sixfour) > 0) {
      $takenLength = mt_rand(1, strlen($sixfour));
      $totalVariablesNameLength = mt_rand(1, 15); // Length for variable names
      $VarName = $AlphaNumericArray[mt_rand(0, 51)]; // First CANNOT be a number, or things break
      for ($j = 0; $j <= ($totalVariablesNameLength - 1); $j++) {
        $VarName = $VarName.$AlphaNumericArray[mt_rand(0, 61)];
      }
      $totalVariablesNames[$whileitteration][0] = $VarName;
      $totalVariablesNames[$whileitteration][1] = substr($sixfour, 0, $takenLength);

      $sixfour = substr($sixfour, $takenLength, strlen($sixfour));

      $FalseCommentLength = mt_rand(1, 15);
      $FalseComment = '/*';
      for ($j = 0; $j <= $totalVariablesNameLength; $j++) {
        $FalseComment = $FalseComment.$AlphaNumericSymArray[mt_rand(0, 86)];
      }
      $FalseComment = $FalseComment.'*/';

      // Could also use an array here, but it is whatever works.
      $buildvardecString = $buildvardecString.'$'.$totalVariablesNames[$whileitteration][0].'=\''.$totalVariablesNames[$whileitteration][1].'\';'.$FalseComment.'|';
      $buildb64String = $buildb64String.'.$'.$totalVariablesNames[$whileitteration][0];
      $whileitteration = $whileitteration + 1;
    }
    $buildb64String = substr($buildb64String, 1);

    $explodevardecString = explode('|', $buildvardecString);
    array_pop($explodevardecString); // Kills off the empty array element
    shuffle($explodevardecString);
    $finalvardecString = implode('', $explodevardecString);

    return $finalvardecString." preg_replace(\"/./e\",\"\\x65\\x76\\x61\\x6C\\x28\\x67\\x7A\\x69\\x6E\\x66\\x6C\\x61\\x74\\x65\\x28\\x62\\x61\\x73\\x65\\x36\\x34\\x5F\\x64\\x65\\x63\\x6F\\x64\\x65\\x28'".$buildb64String."'\\x29\\x29\\x29\\x3B\",\".\");"; //eval(gzinflate(base64_decode('code')));
}

function fileRead($data) { // Connot be stacked on bottom. Can however, be stacked on top. Data is B64GZ'ed 3 times then fed in this.
  $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';  //64 characters
  $charactersArray = str_split($characters);
  shuffle($charactersArray);
  $charactersShuffled = implode('', $charactersArray);

  $innerEval = strtr(base64_encode('$OO00O00O0=str_replace(__FILE__,"".$OOO0O0O00."",(gzinflate(base64_decode(strtr(fread($O000O0O00,$OO00O0000),\''.$charactersShuffled.'\',\''.$characters.'\')))));fclose($O000O0O00);eval($OO00O00O0);'), $characters, $charactersShuffled);

  $outerEval = base64_encode('$O000O0O00=fopen($OOO0O0O00,\'rb\');while(--$O00O00O00)fgets($O000O0O00,1024);fgets($O000O0O00,4096);$OO00O00O0=(base64_decode(strtr(fread($O000O0O00,380),\''.$charactersShuffled.'\',\''.$characters.'\')));eval($OO00O00O0);');

  $realCode = strtr(base64_encode(gzdeflate($data)), $characters, $charactersShuffled);

  $startFile = '// This file is protected by copyright law and provided under license. Reverse engineering of this file is strictly prohibited.'.PHP_EOL.'$OOO0O0O00=__FILE__;$O00O00O00=__LINE__;$OO00O0000='.strlen($realCode).';eval((base64_decode(\''.$outerEval.'\')));return;?>'.PHP_EOL;

  $startFile .= $innerEval;
  $startFile .= $realCode;

  return $startFile;
}

function hexEncode($times, $data) {
    if ($times == 1) {
      $string = str_split(base64_encode(gzdeflate(($data))));
      foreach($string as &$char)
        $char = "\x".dechex(ord($char));
      $finalString = implode('',$string);
    } if ($times > 10) {
      return "Cannot use the Hex Encoding option with a value higher than 10. It causes issues.";
    } else {

      for ($i = 0; $i <= ($times - 1); $i++) { // Needed for correct amount
      $string = str_split(base64_encode(gzdeflate(($data))));
      foreach($string as &$char)
        $char = "\x".dechex(ord($char));
      $data = implode('',$string);
      }
      $finalString = $data;
    }
  return "preg_replace(\"/./e\",\"\\x65\\x76\\x61\\x6C\\x28\\x67\\x7A\\x69\\x6E\\x66\\x6C\\x61\\x74\\x65\\x28\\x62\\x61\\x73\\x65\\x36\\x34\\x5F\\x64\\x65\\x63\\x6F\\x64\\x65\\x28'".$finalString."'\\x29\\x29\\x29\\x3B\",\".\");"; //eval(gzinflate(base64_decode('code')));

}

function rotEncode($data) {
// Rot does an inverse of itself if called on itself. As such, chainging cannot be done in a row, and an intermediate must be used.
return 'preg_replace("/./e","\\x65\\x76\\x61\\x6C\\x28\\x67\\x7A\\x69\\x6E\\x66\\x6C\\x61\\x74\\x65\\x28\\x62\\x61\\x73\\x65\\x36\\x34\\x5F\\x64\\x65\\x63\\x6F\\x64\\x65\\x28\\x73\\x74\\x72\\x5F\\x72\\x6F\\x74\\x31\\x33\\x28\''.str_rot13(base64_encode(gzdeflate($data))).'\'\\x29\\x29\\x29\\x29\\x3B",".");'; // encapsulation is required for this! so it is done here.
}

function bitShift($times, $data) {
$string = str_split($data);
foreach($string as &$char) {
	$shift = mt_rand(0, 24);
  $char = 'chr('.(ord($char) << $shift).' >> '.$shift.').';
}
$finalString = 'eval(';
$finalString .= implode('',$string);
$finalString = substr($finalString, 0, -1);
$finalString .= ');';
$finalString = base64_encode(gzdeflate($finalString));
return "preg_replace(\"/./e\",\"\\x65\\x76\\x61\\x6C\\x28\\x67\\x7A\\x69\\x6E\\x66\\x6C\\x61\\x74\\x65\\x28\\x62\\x61\\x73\\x65\\x36\\x34\\x5F\\x64\\x65\\x63\\x6F\\x64\\x65\\x28'".$finalString."'\\x29\\x29\\x29\\x3B\",\".\");";
}


?>